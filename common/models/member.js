/*!
 * Module Dependencies.
 */
var checkAccessToken = require('../lib/checkAccessToken');
var FB = require('fb');
var bcrypt = require('bcrypt');
var nodemailer = require('nodemailer');
var sesTransport = require('nodemailer-ses-transport');
var transport = nodemailer.createTransport(sesTransport({
  accessKeyId: 'AKIAJGYOQUOBOXWCXTKA',
  secretAccessKey: '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',
  rateLimit: 5, // do not send more than 5 messages in a second
  region: 'us-west-2'
}));
var loopback = require('loopback');
var async = require('async');

// for download image
var fs = require('fs');
var request = require('request');
var path = require('path');
var moment = require('moment');

var SALT_FACTOR = 10;

//var transport = nodemailer.createTransport({
//    service: 'gmail',
//    auth: {
//        user: 'salimz.d55@gmail.com',
//        pass: '2482536sa'
//    }
//
//});


// Development Environment for reset password
var env = process.env.NODE_ENV || 'production';
var resetUrl = 'https://api.vingtv.com#/resetpassword';
var BASE_URL = 'https://api.vingtv.com';
if ('dev' === env) {
  resetUrl = 'http://localhost:3000#/resetpassword';
  BASE_URL = 'http://localhost:3000';
}


/*!
 * Module Constants.
 */
var APP_ACCESS_TOKEN_TTL = 1209600;

/**
 * Member model.
 *
 * Default `Member` ACLs.
 *
 * - DENY EVERYONE `*`
 * - ALLOW EVERYONE `create`
 * - ALLOW OWNER `deleteById`
 * - ALLOW EVERYONE `login`
 * - ALLOW EVERYONE `logout`
 * - ALLOW OWNER `findById`
 * - ALLOW OWNER `updateAttributes`
 * - ALLOW EVERYONE `loginWithFacebookAcccessToken`
 *
 * @class Member
 * @inherits {User}
 */

module.exports = function (Member) {

  Member.beforeRemote('**', function (ctx, unused, next) {
    console.log('Member.beforeRemote');
    console.log(ctx.methodString);

    var allowAll = [
      'Member.login', 'Member.logout', 'Member.signup', 'Member.upload',
      'Member.loginWithFacebookAccessToken', 'Member.reset', 'Member.find'
    ];

    var checkAccess = [
      'Member.updateProfile', 'Member.unlinkfacebook', 'Member.changePassword',
      'Member.detail', 'Member.requestCoach', 'Member.getBalanceTotal'
    ];

    console.log("hi1");
    // Allow All
    if (allowAll.indexOf(ctx.methodString) > -1) {
      console.log("hi2");
      return next();
    }

    if (checkAccess.indexOf(ctx.methodString) > -1) {
      checkAccessToken(Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
        return next(err);
      });
    } else {
      if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
        Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
          if (err) {
            var errMsg = new Error('Database error.');
            errMsg.status = 500; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
          if (memberObj.username === 'admin') { //Admin -> allows all.
            next(); //OK
          } else {
            var errMsg = new Error('Not Authorized. You don\'t have a permission.');
            errMsg.status = 401; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
        })
      } else {
        var errMsg = new Error('Not Authorized. You don\'t have a permission.');
        errMsg.status = 401; // HTTP status code
        console.log(errMsg);
        next(errMsg);
      }
    }
  });

  // inline helper function
  var createReturnObj = function (member, token) {
    return {
      id: member.id,
      userId: member.id,
      name: member.name,
      displayName: member.displayName,
      email: member.email,
      notificationFollow: member.notificationFollow,
      notificationComment: member.notificationComment,
      facebookId: member.facebookId,
      profileImage: member.profileImage(),
      coverImage: member.coverImage(),
      token: token,
      facebookstatus: true
    };
  };

  var createMemberToken = function (member, done) {
    member.createAccessToken({
      tt: APP_ACCESS_TOKEN_TTL
    }, function (err, token) {
      if (err) {
        return done({
          message: err
        });
      }
      console.log('profile' + member.profileImage());
      console.log(member.coverImage());
      Member.findById(member.id, {
        include: [{
          relation: 'profileImage',
          scope: {
            where: {
              hidden: 0
            },
          }
        }, {
          relation: 'coverImage',
          scope: {
            where: {
              hidden: 0
            },
          }
        }]
      }, function (err, memberWithImage) {
        if (err) {
          console.log(err);
          return done(err);
        }
        console.log(memberWithImage);
        var returnObj = createReturnObj(memberWithImage, token);
        return done(err, returnObj);
      })

    });
  };

  var download = function (uri, filename, callback) {
    request.head(uri, function (err, res, body) {
      console.log('content-type:', res.headers['content-type']);
      console.log('content-length:', res.headers['content-length']);

      request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
  };

  /**
   *  Reset password handle
   */
  Member.on('resetPasswordRequest', function (info) {
    console.log(info.email); // the email of the requested user
    console.log(info.accessToken.id); // the temp access token to allow password reset

    // setup e-mail data with unicode symbols
    var mailOptions = {
      from: 'phisit@playwork.co.th', // sender address
      to: info.email, // list of receivers
      subject: 'Reset Password Ving App', // Subject line
      text: 'Please reset password at: ' + resetUrl + '?user=' + info.user.id + '&access_token=' + info.accessToken.id, // plaintext body
      html: 'Please reset password at:<br> <a href="' + resetUrl + '?user=' + info.user.id + '&access_token=' + info.accessToken.id + '">Reset Password</a>',
    };


    console.log(info);
    // requires AccessToken.belongsTo(User)
    info.accessToken.user(function (err) {
      if (err) {
        console.log(err);
        return;
      }
      // send mail with defined transport object
      transport.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log('reject here');
          console.log(info);
          return console.log(error);
        }
        console.log(info);
        // console.log('Message sent: ' + info);
      });
    });
  });


  /**
   *  Return token after signup
   */
  Member.observe('after save', function (ctx, next) {
    if (ctx.isNewInstance) {
      // console.log('Saved %s#%s', ctx.Model.modelName, ctx.instance.id);
      // ctx.instance
      ctx.instance.createAccessToken({
        tt: APP_ACCESS_TOKEN_TTL
      }, function (err, token) {
        if (err) {
          console.log(err);
          next();
        }
        ctx.instance.token = token;
        next();
      });
    } else {
      next();
    }
  });

  /**
   *  Change how login return token
   */
  /*
  Member.afterRemote('login', function (ctx, model, next) {
    Member.findById(model.userId, {
      include: [{
        relation: 'profileImage',
        scope: {
          where: {
            hidden: 0
          },
        }
      }, {
        relation: 'coverImage',
        scope: {
          where: {
            hidden: 0
          },
        }
      }]
    }, function (err, member) {
      if (err) {
        console.log(err);
      }

      console.log(member);
      var token = {
        id: model.id,
        ttl: model.ttl,
        created: model.created,
        userId: model.userId,
      };
      for (var key in member) {
        if (typeof member[key] !== 'function' && typeof member[key] !== 'object' && member[key] && key !== 'password' && typeof member[key] !== 'boolean') {
          model[key] = member[key];
        }
      }
      model.id = undefined;
      model.id = member.id;
      model.profileImage = member.profileImage();
      model.coverImage = member.coverImage();
      model.token = token;
      model.userId = undefined;
      delete model.userId;
      model.ttl = undefined;
      delete model.ttl;
      model.created = undefined;
      delete model.created;
      //ctx.result = model;
      ctx.result.id = undefined;
      delete ctx.result.id;
      console.log(ctx.result);
      next();
    });
  });
  */
  Member.login = function (data, hmm, cb) {
    console.log(data);
    Member.findOne({
      where: {
        email: data.email
      }
    }, function (err, member) {
      console.log(member);
      member.hasPassword(data.password, function (err, isMatched) {
        if (isMatched) {
          createMemberToken(member, function (err, returnObj) {
            if (err) {
              console.log(err);
            }
            return cb(null, returnObj);
          })

        } else {
          var errMsg = new Error('Not Authorized.');
          errMsg.status = 401; // HTTP status code
          return cb(errMsg);
        }
      });
    });

  }

  /**
   update profile
   */
  Member.updateProfile = function (data, cb) {
    if (data.memberId && data.memberId !== '') {
      delete data.password;
      Member.findOne({
        where: {
          id: data.memberId
        }
      }, function (err, existingMember) {
        if (err) {
          console.log(err);
          var errMsg = new Error(err);
          errMsg.status = 500; // HTTP status code
          return cb(errMsg);
        }
        if (data.email && existingMember.email !== data.email) {
          Member.findOne({
            where: {
              email: data.email
            }
          }, function (err, checkingMember) {
            if (err) {
              console.log(err);
              var errMsg = new Error(err);
              errMsg.status = 500; // HTTP status code
              return cb(errMsg);
            }
            if (checkingMember) {
              var errMsg = new Error('This email is already used.');
              errMsg.status = 422; // HTTP status code
              return cb(errMsg);
            } else {
              existingMember.email = data.email;
              Member.updateAll({
                id: data.memberId
              }, existingMember, function (err, info) {
                if (err) {
                  console.log(err);
                  var errMsg = new Error(err);
                  errMsg.status = 500; // HTTP status code
                  return cb(errMsg);
                }
                return cb(null, data);
              });
            }
          })
        } else {
          data.id = existingMember.id;
          existingMember.displayName = data.displayName ? data.displayName : existingMember.displayName;
          existingMember.firstname = data.firstname ? data.firstname : existingMember.firstname;
          existingMember.lastname = data.lastname ? data.lastname : existingMember.lastname;
          existingMember.about = data.about ? data.about : existingMember.about;
          existingMember.displayName = data.displayName ? data.displayName : existingMember.displayName;
          existingMember.device_token = data.device_token ? data.device_token : existingMember.device_token;
          existingMember.version = data.version ? data.version : existingMember.version;
          existingMember.facebookId = data.facebookId ? data.facebookId : existingMember.facebookId;
          if (data.facebookId) {
            existingMember.facebookstatus = true;
          }

          Member.updateAll({
            id: data.memberId
          }, existingMember, function (err, info) {
            if (err) {
              console.log(err);
              var errMsg = new Error(err);
              errMsg.status = 500; // HTTP status code
              return cb(errMsg);
            }
            console.log(info);
            return cb(null, existingMember);
          });
        }
      });
    } else {
      var errMsg = new Error('Validate.');
      errMsg.status = 422; // HTTP status code
      return cb(errMsg);
    }
  };

  Member.unlinkfacebook = function (data, cb) {
    Member.findById(data.memberId, function (err, member) {
      member.facebookstatus = false;
      member.facebookId = '';
      member.save(function (err) {
        if (err) {
          return cb(err);
        }
        cb(null, member);
      });
    });

  };


  /**
   * Allow mobile/web client to login and register a new account using Facebook Access Token.
   * Then generate App Access Token for the logged in user.
   *
   * @param {String} Facebook Access Token
   * @callback {Function} done - The callback function
   * @return {String|Error} err - The error string or object
   * @return {String} token - App Access Token
      Changelog:
          - check if there is any user with facebookId
          - if user exists create accessToken and return member object
          - if user does not exists create a new member and return with token.
   */
  Member.loginWithFacebookAccessToken = function (data, done) {

    var Image = Member.app.models.Image;

    var accessToken = data.accessToken;




    // Set FB access token to FB connector
    FB.setAccessToken(accessToken);

    // Check if FB is valid
    FB.api('me?fields=name,first_name,last_name,picture', function (res) {
      if (res.error) {
        var err = new Error('Invalid FacebookAccessToken.');
        err.status = 422; // HTTP status code
        done(err);
      } else {
        console.log(res);

        // res.id is facebookId in our member
        var facebookId = res.id;
        var email = res.email;

        Member.findOne({
          where: {
            facebookId: facebookId
          }
        }, function (err, member) {
          if (err) {
            console.log(err);
            return done({
              message: err
            });
          }

          if (member) {
            if (!member.facebookstatus) {
              member.facebookstatus = true;
              member.save(function (err) {
                if (err) {
                  return done(err);
                }
              });
            }
            // Generate App Access Token for logged in member
            return createMemberToken(member, done);
          } else {
            Member.findOne({
              where: {
                email: email
              }
            }, function (err, member2) {
              if (err) {
                console.log(err);
              }
              if (member2 && member2.email === email) {
                member2.facebookId = facebookId;
                member2.facebookstatus = true;
                member2.save(function (err) {
                  if (err) {
                    return done(err);
                  }
                  // Generate App Access Token for logged in member
                  return createMemberToken(member2, done);
                });
              } else {

                var newMember = {};
                newMember.facebookId = res.id;
                newMember.displayName = res.name;
                newMember.email = res.email ? res.email : res.id + 'ving@facebook.com';

                // Generate salt
                bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
                  if (err) {
                    callback(err);
                  }

                  // Generate password
                  var d = Date.now;
                  bcrypt.hash(newMember.email + d.toString(), salt, function (err, hash) {
                    if (err) {
                      callback(err);
                    }
                    newMember.password = hash;

                    // Create a new member
                    Member.create(newMember, function (err, user) {
                      if (err) {
                        callback(err);
                      }

                      var imageFilename = Math.floor(Math.random() * 1000) + '-' + Date.now() + '.jpg';
                      var imagePath = path.join(__dirname, '../../client/dist/assets/profile/' + imageFilename);
                      download(res.picture.data.url, imagePath, function () {
                        console.log('done downloading');
                        Image.create({
                            profileId: user.id,
                            url: '/assets/profile/' + imageFilename,
                            hidden: 0
                          })
                          // create access token
                        return createMemberToken(user, done);
                      });


                    });
                  });
                });
              }
            });
          }
        });
      } // else
    }); // FB.api('/me')
  };

  Member.signup = function (data, callback) {
    Member.create({
      email: data.email,
      password: data.password,
      name: data.name,
      displayName: data.displayName,
      facebookId: data.facebookId,
      notificationFollow: true,
      notificationComment: true,
    }, function (err, member) {
      if (err) {
        console.log(err);
        var errMsg = new Error(err);
        errMsg.status = 422; // HTTP status code
        return callback(errMsg);
      }
      createMemberToken(member, function (err, returnMember) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        callback(null, returnMember);
      });

    });
  };

  Member.changePassword = function (data, callback) {
    Member.findById(data.memberId, function (err, member) {
      if (err) {
        console.log(err);
        var errMsg = new Error(err);
        errMsg.status = 500; // HTTP status code
        return callback(errMsg);
      }
      if (member) {
        member.hasPassword(data.oldpassword, function (err, isMatched) {
          if (err) {
            console.log(err);
            var errMsg = new Error(err);
            errMsg.status = 500; // HTTP status code
            return callback(errMsg);
          }
          if (isMatched) {
            bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
              if (err) {
                console.log(err);
                var errMsg = new Error(err);
                errMsg.status = 500; // HTTP status code
                return callback(errMsg);
              }
              bcrypt.hash(data.newpassword, salt, function (err, hash) {
                if (err) {
                  console.log(err);
                  var errMsg = new Error(err);
                  errMsg.status = 500; // HTTP status code
                  return callback(errMsg);
                }
                Member.updateAll({
                  id: member.id
                }, {
                  password: hash
                }, function (err, info) {
                  if (err) {
                    console.log(err);
                    var errMsg = new Error(err);
                    errMsg.status = 500; // HTTP status code
                    return callback(errMsg);
                  }
                  console.log(info);
                  return callback(null, member);
                })
              });
            });
          } else {
            console.log('Password is incorrect');
            var errMsg = new Error('Password is incorrect');
            errMsg.status = 500; // HTTP status code
            return callback(errMsg);
          }
        });
      } else {
        console.log('Cannot find Member');
        var errMsg = new Error('Cannot find Member');
        errMsg.status = 500; // HTTP status code
        return callback(errMsg);
      }
    });
  };

  Member.detail = function (data, callback) {
    let Stat = Member.app.models.Stat;
    let myLocation;
    var findMember = function (id, callback) {
      Member.findOne({
        where: {
          id: data.memberId
        },
        include: [{
          relation: 'profileImage',
          scope: {
            where: {
              hidden: 0
            }
          }
        }, {
          relation: 'coverImage',
          scope: {
            where: {
              hidden: 0
            }
          }
        }]
      }, function (err, member) {
        if (err) {
          console.log(err);
          var errMsg = new Error('Error finding Member');
          errMsg.status = 500; // HTTP status code
          return callback(errMsg);
        }
        if (member) {
          var Follow = Member.app.models.Follow;
          Follow.count({ followingMemberId: data.memberId }, function (err, count) {
            if (err) {
              return callback(err);
            }
            member.nFollower = count;
            Follow.count({ followerMemberId: data.memberId }, function (err, count) {
              if (err) {
                return callback(err);
              }
              member.nFollowing = count;
              if (typeof data.location === 'string') {
                try {
                  myLocation = JSON.parse(data.location);
                } catch (e) {
                  console.log(e);
                }
              } else {
                myLocation = data.location ? [parseFloat(data.location[0]), parseFloat(data.location[1])] : data.location;
              }
              Stat.create({ profileId: data.profileId, memberId: data.memberId, location: myLocation }, function(err, stat) {
                if (err) {
                  console.log(err);
                  return callback(err);
                }
                return callback(null, member);
              });
            });
          });
        } else {
          var errMember = new Error('Not Authorized.');
          errMember.status = 401; // HTTP status code
          return callback(errMember);
        }
      });
    };

    findMember(data.memberId, function (err, member) {
      return callback(err, member);
    });
  };


  Member.getBalanceTotal = function (data, callback) {

    var CoinHistory = Member.app.models.CoinHistory;
    var PurchaseHistory = Member.app.models.PurchaseHistory;
    var totalCoin = 0.00;
    var totalPurchase = 0.00;

    async.waterfall([

      // Retrieve Coin records
      function (callback) {
        CoinHistory.find({
          where: {
            memberId: data.memberId
          },
          fields: 'coin'
        }, function (err, coinRecords) {

          if (err) {
            return callback(err);
          }

          if (coinRecords.length) {
            coinRecords.forEach((coinRecord) => {
              totalCoin += coinRecord.coin;
            });
          } else {
            totalCoin = 0.00;
          }

          return callback(null, totalCoin);
        });
      },

      // Retrieve Purchase records
      function (totalCoin, callback) {
        PurchaseHistory.find({
          where: {
            memberId: data.memberId
          },
          fields: 'coin'
        }, function (err, purchaseRecords) {

          if (err) {
            return callback(err);
          }

          if (purchaseRecords.length) {
            purchaseRecords.forEach((purchaseRecord) => {
              totalPurchase += purchaseRecord.coin;
            });
          } else {
            totalPurchase = 0.00;
          }

          return callback(null, totalCoin, totalPurchase);
        });
      },

    ], function (err, result) {

      if (err) {
        console.log(err);
        return callback(null, {
          status: 'error',
          message: err
        })
      }

      return callback(null, {
        status: 'success',
        coin: totalCoin - totalPurchase
      });
    });
  };


  Member.listCoach = function (data, callback) {
    Member.find({
      where: {
        coach: 1
      },
      include: [{
        relation: 'profileImage',
        scope: {
          where: {
            hidden: 0
          },
          limit: 1
        }
      }, {
        relation: 'coverImage',
        scope: {
          where: {
            hidden: 0
          },
          limit: 1
        }
      }]
    }, function (err, member) {
      if (err) {
        console.log(err);
        var errMsg = new Error('Error finding listCoach');
        errMsg.status = 500; // HTTP status code
        return callback(errMsg);
      }
      if (member) {
        return callback(null, member);
      } else {
        var errMember = new Error('Coach 404 not found.');
        errMember.status = 404; // HTTP status code
        return callback(errMember);
      }

    });
  }


  // TODO:
  // - Create contract when request (status pending);
  // - Change create offer API to updateContract api
  // - Change message to include contract
  // - Add convenient API list Coach Request (to test)
  // - Test Flow
  // Nice to have:
  // - Browser push/ push noti
  // - socket!
  Member.requestCoach = function (data, callback) {
    console.log(data);
    var Chat = Member.app.models.Chat;
    var Message = Member.app.models.Message;
    var Contract = Member.app.models.Contract;

    // Inline helper function to create a new message.
    function createMessage(chat, owner, cb) {
      Contract.create({ memberId: data.memberId, coachId: data.coachId, postId: data.postId },
        function (err, contract) {
          if (err) {
            console.log(err);
            return cb(err);
          }
          Message.create({
            message: 'ได้ร้องขอให้คุณเป็นโค้ช',
            owner: owner,
            status: 'unread',
            chatId: chat.id,
            contractId: contract.id,
          }, function (err, message) {
            if (err) {
              return cb(err);
            }
            return cb(null, {
              status: "success",
              message: "",
              data: message
            });
          });
        });

    }
    if (data.memberId && data.coachId) {
      Member.findById(data.coachId, function (err, coach) {
        if (coach.coach) {
          // Create message to coach.

          // Find existing chat
          Chat.find({
            where: {
              and: [{
                or: [{
                  fromMemberId: data.memberId
                }, {
                  toMemberId: data.coachId
                }]
              }, {
                or: [{
                  fromMemberId: data.coachId
                }, {
                  toMemberId: data.memberId
                }]
              }]
            },
          }, function (err, chat) {
            if (err) {
              console.log(err);
              return cb(err);
            }

            if (chat.length === 0) {
              // Create a new chat
              Chat.create({
                fromMemberId: data.memberId,
                toMemberId: data.coachId,
                status: 'active'
              }, function (err, newchat) {
                if (err) {
                  console.log(err);
                  return cb(err);
                }

                console.log(newchat);
                // create a new message with newchat id
                createMessage(newchat, data.coachId, function (err, returnValue) {
                  if (err) {
                    console.log(err);
                  }
                  return callback(null, returnValue);
                });
              });
            } else {
              var currentChat = chat[0];
              console.log(currentChat);
              currentChat.status = 'active';
              currentChat.update_at = moment().toDate();
              currentChat.save(function (err, savedChat) {
                createMessage(savedChat, data.coachId, function (err, returnValue) {
                  if (err) {
                    console.log(err);
                  }
                  return callback(null, returnValue);
                });
              });
            }
          });
        } else {
          var errMsg = new Error('Invalid Coach.');
          errMsg.status = 422; // HTTP status code
          return cb(errMsg);
        }
      })
    } else {
      var errMsg = new Error('Validate.');
      errMsg.status = 422; // HTTP status code
      return cb(errMsg);
    }



    // Should Create message to coach

  }


  /*!
   * Setup an extended user model.
   */
  Member.setup = function () {

    Member.remoteMethod('getSuggestion', {
      http: {
        path: '/getSuggestion',
        verb: 'post'
      },
      accepts: {
        arg: 'data',
        type: 'object',
        http: {
          source: 'body'
        }
      },
      returns: {
        type: 'object',
        root: true
      }
    });

    Member.remoteMethod(
      'loginWithFacebookAccessToken', {
        description: 'Login a user with Facebook Access Token.',
        accepts: {
          arg: 'data',
          type: 'object',
          http: {
            source: 'body'
          }
        },
        returns: {
          type: 'object',
          root: true
        },
        http: {
          verb: 'post'
        }
      });

    Member.remoteMethod(
      'signup', {
        description: 'Singup User Endpoint',
        accepts: [{
          arg: 'data',
          type: 'object',
          http: {
            source: 'body'
          }
        }],
        returns: {
          type: 'object',
          root: true
        },
        http: {
          verb: 'post'
        }
      });

    Member.remoteMethod('updateProfile', {
      http: {
        path: '/updateProfile',
        verb: 'post'
      },
      accepts: [{
        arg: 'data',
        type: 'object',
        http: {
          source: 'body'
        }
      }],
      returns: {
        type: 'object',
        root: true
      }
    });
    Member.remoteMethod('unlinkfacebook', {
      http: {
        path: '/unlinkfacebook',
        verb: 'post'
      },
      accepts: [{
        arg: 'data',
        type: 'object',
        http: {
          source: 'body'
        }
      }],
      returns: {
        type: 'object',
        root: true
      }
    });

    Member.remoteMethod('changePassword', {
      http: {
        path: '/changePassword',
        verb: 'post'
      },
      accepts: [{
        arg: 'data',
        type: 'object',
        http: {
          source: 'body'
        }
      }],
      returns: {
        type: 'object',
        root: true
      }
    });

    Member.remoteMethod('detail', {
      http: {
        path: '/detail',
        verb: 'post'
      },
      accepts: [{
        arg: 'data',
        type: 'object',
        http: {
          source: 'body'
        }
      }],
      returns: {
        type: 'object',
        root: true
      }
    });

    Member.remoteMethod('listCoach', {
      http: {
        path: '/listCoach',
        verb: 'post'
      },
      accepts: [{
        arg: 'data',
        type: 'object',
        http: {
          source: 'body'
        }
      }],
      returns: {
        type: 'object',
        root: true
      }
    });


    Member.remoteMethod('requestCoach', {
      http: {
        path: '/requestCoach',
        verb: 'post'
      },
      accepts: [{
        arg: 'data',
        type: 'object',
        http: {
          source: 'body'
        }
      }],
      returns: {
        type: 'object',
        root: true
      }
    });

    Member.remoteMethod('getBalanceTotal', {
      http: {
        path: '/getBalanceTotal',
        verb: 'post'
      },
      accepts: [{
        arg: 'data',
        type: 'object',
        http: {
          source: 'body'
        }
      }],
      returns: {
        type: 'object',
        root: true
      }
    });
  };

  /*!
   * Setup the member.
   */
  Member.setup();

};
