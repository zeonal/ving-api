var checkAccessToken = require('../lib/checkAccessToken');
var bsonUrlEncoding = require('./../lib/bsonUrlEncoding');

module.exports = function (Interest) {

  Interest.beforeRemote('**', function (ctx, unused, next) {
    console.log('Interest.beforeRemote');
    console.log(ctx.methodString);

    if (ctx.methodString === 'Interest.byMember') { //get
      checkAccessToken(Interest.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
        next(err);
      });
    } else if (ctx.methodString === 'Interest.getInterest') { //get
      //Allow all
      next();
    } else {
      if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
        var Member = Interest.app.models.Member;
        Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
          if (err) {
            var errMsg = new Error('Database error.');
            errMsg.status = 500; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
          if (memberObj.username === 'admin') { //Admin -> allows all.
            next(); //OK
          } else {
            var errMsg = new Error('Not Authorized. You don\'t have a permission.');
            errMsg.status = 401; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
        })
      } else {
        var errMsg = new Error('Not Authorized. You don\'t have a permission.');
        errMsg.status = 401; // HTTP status code
        console.log(errMsg);
        next(errMsg);
      }
    }
  });

  Interest.byMember = function (data, callback) {
    if (data.memberId) {
      var query = {
        hidden: 0
      };
      if (data.next) {
        var next = bsonUrlEncoding.decode(data.next);
        query = {
          id: {
            lt: next.toString()
          },
          hidden: 0
        };
      }
      var Member = Interest.app.models.Member;
      Member.findById(data.memberId, {
        where: {},
        fields: ['id'],
        include: [{
          relation: 'interest',
          scope: {
            where: query,
            limit: 10,
            order: "create_at DESC",
            fields: ['id', 'name'],
            include: [{
              relation: 'image',
              scope: {
                fields: ['id', 'url'],
                where: {
                  hidden: 0
                }
              }
            }]
          }
        }]
      }, function (err, returnMember) {
        if (err) {
          return callback(err);
        }
        if (returnMember) {
          var results = returnMember.interest();
          return callback(null, {
            results: results,
            next: results[results.length - 1] && results.length == 10 ? bsonUrlEncoding.encode(results[results.length - 1].id) : undefined
          });
        } else {
          return callback(null, {
            status: 'error',
            message: 'Member not found.',
          });
        }
      })
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };


  Interest.getInterest = function (data, callback) {
    var query = {
      hidden: 0
    };
    if (data.next) {
      var next = bsonUrlEncoding.decode(data.next);
      query = {
        id: {
          lt: next.toString()
        },
        hidden: 0
      };
    }
    Interest.find({
      where: query,
      limit: 10,
      order: "create_at DESC",
      include: {
        relation: 'image',
        scope: {
          fields: ['id', 'url'],
          where: {
            hidden: 0
          }
        }
      }
    }, function (err, instances) {
      if (err) {
        console.log(err);
        return callback(err);
      }
      var results = instances;
      return callback(null, {
        results: results,
        next: results[results.length - 1] && results.length == 10 ? bsonUrlEncoding.encode(results[results.length - 1].id) : undefined
      });
    });
  };

  Interest.remoteMethod('byMember', {
    http: {
      path: '/byMember',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Interest.remoteMethod('getInterest', {
    http: {
      path: '/getInterest',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

};