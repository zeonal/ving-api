var checkAccessToken = require('../lib/checkAccessToken');
// var redis = require('redis'),
//   client = redis.createClient();
var redisHelper = require('./../lib/redishelper');
var client = redisHelper.client;
var request = require('request');
var async = require('async');
module.exports = function (Comment) {

  Comment.beforeRemote('**', function (ctx, unused, next) {
    console.log('Comment.beforeRemote');
    console.log(ctx.methodString);

    if (ctx.methodString === 'Comment.createComment') { //post
      checkAccessToken(Comment.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
        next(err);
      });
    } else if (ctx.methodString === 'Comment.deleteComment') { //post
      checkAccessToken(Comment, ctx.req.body.commentId, ctx.req.accessToken, function (err) {
        next(err);
      });
    } else if (ctx.methodString === 'Comment.byPost') { //get
      //Allow all
      next();
    } else if (ctx.methodString === 'Comment.listComment') { //get
      //Allow all
      next();
    } else {
      if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
        var Member = Comment.app.models.Member;
        Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
          if (err) {
            var errMsg = new Error('Database error.');
            errMsg.status = 500; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
          if (memberObj.username === 'admin') { //Admin -> allows all.
            next(); //OK
          } else {
            var errMsg = new Error('Not Authorized. You don\'t have a permission.');
            errMsg.status = 401; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
        })
      } else {
        var errMsg = new Error('Not Authorized. You don\'t have a permission.');
        errMsg.status = 401; // HTTP status code
        console.log(errMsg);
        next(errMsg);
      }
    }
  });


  Comment.createComment = function (data, callback) {
    if (data.memberId && data.postId && data.comment) {
      let Member = Comment.app.models.Member;
      Member.findById(data.memberId, { fields: ['coach'] }, function (err, returnMember) {
        if (err) {
          console.log(err);
          return callback(err);
        };
        Comment.create({
          memberId: data.memberId,
          postId: data.postId,
          comment: data.comment,
          isCoach: returnMember.coach ? 1 : 0
        }, function (err, returnComment) {
          if (err) {
            console.log(err);
            return callback(err);
          }
          if (returnComment) {
            client.zadd('comment:' + returnComment.postId, new Date().getTime(), data.memberId, function (err) {
              if (err) {
                console.log(err);
              }
              return callback(null, { status: 'success' });
            });
          } else {
            return callback(null, { status: 'success' });
          }
        });
      });

    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  Comment.deleteComment = function (data, callback) {
    if (data.commentId) {
      Comment.findById(data.commentId, function (err, returnComment) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        if (returnComment.postId) {
          client.zrem('comment:' + returnComment.postId, memberId, function (err) {
            if (err) {
              console.log(err);
              return callback(err);
            }
            returnComment.destroy(function () {
              return callback(null, { status: 'success' });
            });
          });
        };
        return callback(null, { status: 'success' });
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  Comment.byPost = function (data, callback) {
    if (data.postId) {
      var bsonUrlEncoding = require('./../lib/bsonUrlEncoding');
      var ObjectID = require('mongodb').ObjectID;
      var cursor = data.nextCursor;
      var query = {
        hidden: 0
      };
      if (data.next) {
        var next = bsonUrlEncoding.decode(data.next);
        query = {
          id: {
            lt: next.toString()
          },
          hidden: 0
        };
      }
      var Post = Comment.app.models.Post;
      Post.findById(data.postId, {
        include: [{
          relation: 'comment',
          scope: {
            where: query,
            limit: 10,
            order: ['isCoach DESC', "id ASC"],
            include: [{
              relation: 'image',
              scope: {
                fields: ['id', 'url'],
                where: {
                  hidden: 0
                },
                order: "id DESC"
              }
            }]
          }
        }]
      }, function (err, returnPost) {
        if (err) {
          console.log(err);
          return callback(err);
        }

        if (returnPost) {
          var results = returnPost.comment();
          return callback(null, {
            results: results,
            next: (results[results.length - 1] && results.length == 10) ? bsonUrlEncoding.encode(results[results.length - 1].id) : undefined
          });
        } else {
          return callback(null, {
            status: 'error',
            message: 'Post not found',
            results: []
          });
        }


      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  Comment.listComment = function (data, callback) {
    let result = [];
    let page = 1;
    if (data.page) {
      page = data.page;
    }
    if (data.postId) {
      var bsonUrlEncoding = require('./../lib/bsonUrlEncoding');
      var ObjectID = require('mongodb').ObjectID;
      var cursor = data.nextCursor;
      var query = {
        postId: data.postId,
        hidden: 0
      };
      if (data.next) {
        var next = bsonUrlEncoding.decode(data.next);
        query = {
          id: {
            gt: next.toString()
          },
          postId: data.postId,
          hidden: 0
        };
      }
      Comment.find({
        where: query,
        limit: 10,
        order: ['isCoach DESC', "id ASC"],
        include: [{
          relation: 'member',
          scope: {
            fields: ['id', 'displayName', 'create_at', 'coach'],
            include: [{
              relation: 'profileImage',
              scope: {
                fields: ['id', 'url'],
                where: {
                  hidden: false
                },
                order: "create_at DESC"
              }
            }]
          }
        }]
      }, function (err, comments) {
        if (err) {
          return callback(err);
        }
        async.waterfall([
          function (cb) {
            async.forEach(comments, function (comment, cb) {
              if (comment.member().coach === true) {
                result.push(comment);
              }
              return cb(null);
            }, function (err, result) {
              if (err) {
                console.log(err);
                return cb(err);
              }
              return cb(null, result);
            });
          },
          function (result, cb) {
            async.forEach(comments, function (comment, cb) {
              if (comment.member().coach === false) {
                result.push(comment);
              }
              return cb(null);
            }, function (err, result) {
              if (err) {
                console.log(err);
                return cb(err);
              }
              return cb(null, result);
            });
          }
        ], function (err, result) {
          if (err) {
            console.log(err);
            return callback(err);
          }
          return callback(null, result[0]);
        })
      })
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  // Comment.countPostComment = function (data, callback) {
  //   if (data.postId) {
  //     client.get('comment:' + data.postId.toString(), function (err, reply) {
  //       if (err) {
  //         console.log(err);
  //         return callback(err);
  //       }
  //       if (!reply) {
  //         return callback(null, reply);
  //       }
  //       else {
  //         return callback(null, '0');
  //       }
  //     });
  //   } else {
  //     var error = new Error('Validate.');
  //     error.status = 422;
  //     return callback(error);
  //   }
  // };


  Comment.remoteMethod('createComment', {
    http: {
      path: '/createComment',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Comment.remoteMethod('deleteComment', {

    http: {
      path: '/deleteComment',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Comment.remoteMethod('byPost', {
    http: {
      path: '/byPost',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });
  Comment.remoteMethod('listComment', {
    http: {
      path: '/listComment',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });
};
