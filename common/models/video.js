var checkAccessToken = require('../lib/checkAccessToken');
var async = require('async');

module.exports = function (Video) {
  Video.myvideo = function (memberId, cb) {
    var Playlist = Video.app.models.Playlist;

    var playlist = [];
    var video = [];
    async.parallel({
        video: function (callback) {
          Video.find({
            where: {
              memberId: memberId
            }
          }, function (err, video) {
            if (err) {
              console.log(err);
              return callback(err);
            }
            return callback(null, video);
          });
        },

        playlist: function (callback) {
          Playlist.find({
            where: {
              memberId: memberId
            },
            include: {
              relation: 'playlistVideo',
              scope: {
                order: "sequence ASC",
                include:['video']
              }
            }
          }, function (err, member) {
            if (err) {
              console.log(err);
              return callback(err);
            }
            return callback(null, member);
          });
        }
      },
      //      // optional callback
      function (err, results) {
        if (err) {
          return cb(err);
        }
        //        console.log(results);
        return cb(null, results);
      });
  };

  // FIXME: Video view write locks
  // Should re-implement in redis
  Video.view = function (videoId, cb) {
    Video.findById(videoId, function (err, video) {
      if (err) {
        return cb(err);
      }
      if (video.length !== 0) {
        Video.updateAll({
          id: videoId
        }, {
          view: video.view + 1
        }, function (err, view) {
          if (err) {
            return cb(err);
          }
          if (view.count === 1)
            return cb(null, {
              status: 'success'
            });
        })
      }
    })
  };

  Video.remoteMethod('myvideo', {
    http: {
      path: '/myvideo',
      verb: 'post'
    },
    accepts: {
      arg: 'memberId',
      type: 'string',
      required: true
    },
    returns: {
      type: 'object',
      root: true
    }
  });
  Video.remoteMethod('view', {
    http: {
      path: '/view',
      verb: 'post'
    },
    accepts: {
      arg: 'videoId',
      type: 'string',
      required: true
    },
    returns: {
      type: 'object',
      root: true
    }
  });
};
