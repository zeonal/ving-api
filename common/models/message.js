var moment = require('moment');
var checkAccessToken = require('../lib/checkAccessToken');
module.exports = function (Message) {

  Message.beforeRemote('**', function (ctx, unused, next) {
    console.log('Message.beforeRemote');
    console.log(ctx.methodString);

    if (ctx.methodString === 'Message.sendMessage') {
      checkAccessToken(Message.app.models.Chat, ctx.req.body.chatId, ctx.req.accessToken, function (err) {
        next(err);
      })
    } else {

      if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
        var Member = Message.app.models.Member;
        Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
          if (err) {
            var errMsg = new Error('Database error.');
            errMsg.status = 500; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
          if (memberObj.username === 'admin') { //Admin -> allows all.
            next(); //OK
          } else {
            var errMsg = new Error('Not Authorized. You don\'t have a permission.');
            errMsg.status = 401; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
        })
      } else {
        var errMsg = new Error('Not Authorized. You don\'t have a permission.');
        errMsg.status = 401; // HTTP status code
        console.log(errMsg);
        next(errMsg);
      }
    }
  });


  Message.observe('after save', function (ctx, next) {
    //    console.log(ctx.instance);
    if (ctx.instance) {
      var Notification = Message.app.models.Notification;
      var Chat = Message.app.models.Chat;
      Chat.findById(ctx.instance.toJSON().chatId, {
        include: [{
          relation: 'fromMember',
          scope: {
            fields: ['displayName'],
            include: {
              relation: 'profileImage',
              scope: {
                fields: ['url'],
                where: {
                  hidden: 0
                }
              }
            }
          }
        }, {
          relation: 'toMember',
          scope: {
            fields: ['displayName'],
            include: {
              relation: 'profileImage',
              scope: {
                fields: ['url'],
                where: {
                  hidden: 0
                }
              }
            }
          }
        }]
      }, function (err, chat) {
        if (err) {
          return next(err);
        }
        var image = '';
        if (ctx.instance.toJSON().status === 'unread') {
          if (ctx.instance.toJSON().owner.toString() === chat.fromMemberId.toString()) {
            console.log(chat.fromMember().displayName);
            Notification.create({
              status: 'unread',
              action: 'message',
              memberId: chat.toMemberId,
              senderId: chat.fromMemberId,
              text: chat.fromMember().displayName + " ส่งข้อความสนทนาถึงคุณ",
              message: ctx.instance.message ? ctx.instance.message : '',
              //              image: image,
              chatId: chat.id
            }, function (err, noti) {
              if (err) {
                return next(err);
              }
              return next();
            });
          } else {
            Notification.create({
              status: 'unread',
              action: 'message',
              memberId: chat.fromMemberId,
              senderId: chat.toMemberId,
              text: (chat.toMember().displayName ? chat.toMember().displayName : 'มีคน') + " ส่งข้อความสนทนาถึงคุณ",
              message: ctx.instance.message ? ctx.instance.message : '',
              //              image: image,
              chatId: chat.id
            }, function (err, noti) {
              if (err) {
                return next(err);
              }
              return next();
            });
          }
        } else {
          if (ctx.instance.toJSON().owner.toString() === chat.fromMemberId.toString()) {
            Notification.findOne({
              where: {
                chatId: ctx.instance.toJSON().chatId,
                memberId: chat.toMemberId
              }
            }, function (err, noti) {
              if (err) {
                return next(err);
              }
              noti.status = "read";
              noti.save(function (err) {
                if (err) {
                  return next(err);
                }
                return next();
              });
            })
          } else {
            Notification.findOne({
              where: {
                chatId: ctx.instance.toJSON().chatId,
                memberId: chat.fromMemberId
              }
            }, function (err, noti) {
              if (err) {
                return next(err);
              }
              noti.status = "read";
              noti.save(function (err) {
                if (err) {
                  return next(err);
                }
                return next();
              });
            });
          }
        }
      })
    } else {
      return next();
    }
  });

  Message.sendMessage = function (data, cb) {
    if (data.memberId && data.chatId) {
      var Chat = Message.app.models.Chat;
      Chat.findById(data.chatId, function (err, chat) {
        if (err) {
          console.log(err);
          return cb(err);
        }
        if (chat.fromMemberId.toString() === data.memberId || chat.toMemberId.toString() === data.memberId) {
          chat.status = "active";
          chat.update_at = moment().toDate();
          chat.save(function (err, myChat) {
            if (err) {
              console.log(err);
              return cb(err);
            }
            if (myChat) {
              Message.create({
                message: data.message,
                owner: data.memberId,
                status: 'unread',
                chatId: chat.id
              }, function (err, message) {
                if (err) {
                  return cb(err);
                }
                return cb(null, {
                  status: "success",
                  message: "",
                  data: message
                });
              });
            } else {
              return cb();
            }
          });

        } else {
          var error = new Error('Chat not found.');
          error.status = 422;
          //          return cb(error);
          return cb(null, {
            status: "fail",
            message: "ChatId นี้ไม่มีอยู่ในระบบ",
            data: null,
            error: error
          });
        }
      })
    } else {
      var error = new Error('Validate Error.');
      error.status = 422;
      //      return cb(error);
      return cb(null, {
        status: "fail",
        message: "กรุณส่งข้อมูลให้ครบค่ะ",
        data: null,
        error: error
      });
    }
  };
  Message.remoteMethod('sendMessage', {
    http: {
      path: '/sendMessage',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
            }],
    returns: {
      type: 'object',
      root: true
    }
  });
};
