var checkAccessToken = require('../lib/checkAccessToken');
var async = require('async');
var redisHelper = require('./../lib/redishelper');
var client = redisHelper.client;

module.exports = function (Share) {

  var Member, Post, Feed;
  Share.on('attached', function () {
    // perform any setup that requires the app object; for improve performance
    Member = Share.app.models.Member;
    Post = Share.app.models.Post;
    Feed = Share.app.models.Feed;
  });


  Share.beforeRemote('**', function (ctx, unused, next) {
    console.log('Share.beforeRemote');
    console.log(ctx.methodString);

    if (ctx.methodString === 'Share.createShare') { //post
      checkAccessToken(Share.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
        next(err);
      });
    } if (ctx.methodString === 'Share.deleteShare') { //post
      checkAccessToken(Share.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
        next(err);
      });
    } else if (ctx.methodString === 'Share.countShare') { //get
      //Allow all
      next();
    } else {
      if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
        var Member = Share.app.models.Member;
        Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
          if (err) {
            var errMsg = new Error('Database error.');
            errMsg.status = 500; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
          if (memberObj.username === 'admin') { //Admin -> allows all.
            next(); //OK
          } else {
            var errMsg = new Error('Not Authorized. You don\'t have a permission.');
            errMsg.status = 401; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
        })
      } else {
        var errMsg = new Error('Not Authorized. You don\'t have a permission.');
        errMsg.status = 401; // HTTP status code
        console.log(errMsg);
        next(errMsg);
      }
    }
  });

  Share.createShare = function (data, callback) {
    //TODO: create a new post, update Feed
    if (data.postId && data.memberId) {
      async.parallel([
        function (parallelCallback) {
          Post.exists(data.postId, function (err, exists) {
            if (err) {
              return parallelCallback(err);
            } else if (!exists) {
              var error = new Error('PostId doesn\'t exist.');
              error.status = 422;
              return parallelCallback(error);
            }
            return parallelCallback(null);
          })
        }
      ], function (err) {
        if (err) {
          console.log(err);
          return callback(err);
        } else {
          Share.create({ memberId: data.memberId, postId: data.postId }, function (err, instance) {
            if (err) {
              console.log(err);
              return callback(err);
            }
            var Member = Share.app.models.Member;
            Member.findById(data.memberId, {
              fields: ['id'],
              include: [{
                relation: 'follower',
                scope: {
                  fields: ['id']
                }
              }]
            }, function (err, returnMember) {
              if (err) {
                console.log(err);
                return callback(err);
              }
              //add feed
              async.each(returnMember.follower(), function (follower, eachCallback) {
                var Feed = Share.app.models.Feed;
                Feed.create({ memberId: follower.id, postId: returnPost.id }, function (err, unused) {
                  if (err) {
                    console.log(err);
                    return eachCallback(err);
                  }
                  eachCallback(null);
                });
              }, function (err) {
                if (err) {
                  console.log(err);
                  return callback(err);
                }
                if (data.postId) {
                  client.incr('share:' + data.postId, function (err) {
                    if (err) {
                      console.log(err);
                    }
                    return callback(null, { status: 'success' });
                  });
                } else {
                  return callback(null, { status: 'success' });
                }
              });
            });
          })
        }
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  }

  Share.deleteShare = function (data, callback) {
    if (data.shareId) {
      Share.findById(data.shareId, function (err, returnShare) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        if (returnShare.postId) {
          client.decr('share:' + returnShare.postId, function (err) {
            if (err) {
              console.log(err);
            }
            returnShare.destroy(function () {
              return callback(null, { status: 'success' });
            });
          });
        }
        else {
          return callback(null, { status: 'success' });
        }
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  // Share.countPostShare = function (data, callback) {
  //   if (data.postId) {
  //     async.parallel([
  //       function (parallelCallback) {
  //         Post.exists(data.postId, function (err, exists) {
  //           if (err) {
  //             return parallelCallback(err);
  //           } else if (!exists) {
  //             var error = new Error('PostId doesn\'t exist.');
  //             error.status = 422;
  //             return parallelCallback(error);
  //           }
  //           return parallelCallback(null);
  //         })
  //       }
  //     ], function (err) {
  //       if (err) {
  //         console.log(err);
  //         return callback(err);
  //       } else {
  //         Share.count({ postId: data.postId }, function (err, count) {
  //           if (err) {
  //             console.log(err);
  //             return callback(err);
  //           }
  //           return callback(null, { count: count });
  //         })
  //       }
  //     });
  //   } else {
  //     var error = new Error('Validate.');
  //     error.status = 422;
  //     return callback(error);
  //   }
  // }

  Share.remoteMethod('createShare', {
    http: {
      path: '/createShare',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Share.remoteMethod('deleteShare', {
    http: {
      path: '/deleteShare',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

};