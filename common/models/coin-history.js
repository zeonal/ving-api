var checkAccessToken = require('../lib/checkAccessToken');
var async = require('async');
var moment = require('moment');
var request = require("request");
var qs = require("querystring");

module.exports = function (CoinHistory) {

	CoinHistory.beforeRemote('**', function (ctx, unused, next) {
		console.log('CoinHistory.beforeRemote');
		console.log(ctx.methodString);
		if (ctx.methodString === 'CoinHistory.requestUrl') { //post
			checkAccessToken(CoinHistory.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
				next(err);
			});
		} else {
			if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
				var Member = CoinHistory.app.models.Member;
				Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
					if (err) {
						var errMsg = new Error('Database error.');
						errMsg.status = 500; // HTTP status code
						console.log(errMsg);
						next(errMsg);
					}
					if (memberObj.username === 'admin') { //Admin -> allows all.
						next(); //OK
					} else {
						var errMsg = new Error('Not Authorized. You don\'t have a permission.');
						errMsg.status = 401; // HTTP status code
						console.log(errMsg);
						next(errMsg);
					}
				})
			} else {
				var errMsg = new Error('Not Authorized. You don\'t have a permission.');
				errMsg.status = 401; // HTTP status code
				console.log(errMsg);
				next(errMsg);
			}
		}
	});

	CoinHistory.requestUrl = function (data, callback) {
		var options = {
			method: 'POST',
			url: 'http://vpay-sandbox.paotung.co.th/api/createToken',
			qs: { ApiKey: '1111111111', RefNumber1: data.memberId },
			headers: {
				'cache-control': 'no-cache'
			}
		};
		request(options, function (error, response, body) {
			if (error) {
				return callback(error);
			}
			var result = JSON.parse(body);
			if (result.Status === '200' && result.StatusText === 'success') {
				CoinHistory.create({ memberId: data.memberId, status: 'pending', coin: 0, token: result.Token, serviceURL: result.ServiceURL }, function (err, returnObj) {
					if (err) {
						console.log(err);
						return callback(err);
					};
					delete result.Token; //important
					delete result.Status;
					delete result.StatusText;
					result.status = 'success';
					result.ServiceURL = decodeURIComponent(result.ServiceURL);
					return callback(null, result);
				});
			} else {
				var error = new Error('Error: Request Unsuccessful.');
				error.status = 204;
				return callback(error);
			}
		});
	}

	CoinHistory.remoteMethod('requestUrl', {
		http: {
			path: '/requestUrl',
			verb: 'post'
		},
		accepts: [{
			arg: 'data',
			type: 'object',
			http: {
				source: 'body'
			}
		}],
		returns: {
			type: 'object',
			root: true
		}
	});
}