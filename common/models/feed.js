var checkAccessToken = require('../lib/checkAccessToken');
var async = require('async');
var moment = require('moment');
var redisHelper = require('./../lib/redishelper');
var client = redisHelper.client;
var bsonUrlEncoding = require('./../lib/bsonUrlEncoding');

module.exports = function (Feed) {

  Feed.beforeRemote('**', function (ctx, unused, next) {
    console.log('Feed.beforeRemote');
    console.log(ctx.methodString);

    if (ctx.methodString === 'Feed.byMember') { //post
      checkAccessToken(Feed.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
        next(err);
      });
    } else if (ctx.methodString === 'Feed.feed') { //post
      checkAccessToken(Feed.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
        next(err);
      });
    } else {
      if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
        var Member = Feed.app.models.Member;
        Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
          if (err) {
            var errMsg = new Error('Database error.');
            errMsg.status = 500; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
          if (memberObj.username === 'admin') { //Admin -> allows all.
            next(); //OK
          } else {
            var errMsg = new Error('Not Authorized. You don\'t have a permission.');
            errMsg.status = 401; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
        })
      } else {
        var errMsg = new Error('Not Authorized. You don\'t have a permission.');
        errMsg.status = 401; // HTTP status code
        console.log(errMsg);
        next(errMsg);
      }
    }
  });

  function countingStars(returnArray, callback) {
    //counting
    async.parallel({
        like: function (callback) {
          async.each(returnArray, function (feed, eachCallback) {
            client.zcard('like:' + feed.post().id.toString(), function (err, reply) {
              if (err) {
                console.log(err);
                return eachCallback(err);
              }
              feed.post().countLike = reply;
              return eachCallback();
            });
          }, function (err) {
            if (err) {
              console.log(err);
              return callback(err);
            }
            return callback(null);
          });
        },
        isLiked: function (callback) {
          async.each(returnArray, function (feed, eachCallback) {
            //sorted set
            var args = ['like:' + feed.post().id.toString(), '[' + feed.post().memberId, '[' + feed.post().memberId + 'a'];
            client.zrangebylex(args, function (err, reply) {
              if (err) {
                console.log(err);
                return eachCallback(err);
              }
              feed.post().isLiked = reply.length ? true : false;
              return eachCallback();
            });
          }, function (err) {
            if (err) {
              console.log(err);
              return callback(err);
            }
            return callback(null);
          });
        },
        comment: function (callback) {
          async.each(returnArray, function (feed, eachCallback) {
            client.zcard('comment:' + feed.post().id.toString(), function (err, reply) {
              if (err) {
                console.log(err);
                return eachCallback(err);
              }
              feed.post().countComment = reply;
              return eachCallback();
            });
          }, function (err) {
            if (err) {
              console.log(err);
              return callback(err);
            }
            return callback(null);
          });
        },
        share: function (callback) {
          async.each(returnArray, function (feed, eachCallback) {
            client.get('share:' + feed.post().id.toString(), function (err, reply) {
              if (err) {
                console.log(err);
                return eachCallback(err);
              }
              if (!reply) {
                feed.post().countShare = 0;
              } else {
                feed.post().countShare = reply;
              }
              return eachCallback();
            });
          }, function (err) {
            if (err) {
              console.log(err);
              return callback(err);
            }
            return callback(null);
          });
        }
      },
      // optional callback
      function (err) {
        if (err) {
          return callback(err);
        }
        return callback(null, returnArray);
      });
  }

  Feed.byMember = function (data, callback) {
    var query = {
      memberId: data.memberId
    };
    if (data.next) {
      var next = bsonUrlEncoding.decode(data.next);
      query = {
        memberId: data.memberId,
        id: {
          lt: next.toString()
        }
      };
    }
    Feed.find({
      where: query,
      fields: ['id', 'memberId', 'postId'],
      order: "id DESC",
      limit: 10,
      include: [{
        relation: 'post',
        scope: {
          fields: ['id', 'memberId', 'defaultImageUrl', 'name', 'description', 'coin', 'status', 'create_at'],
          where: {
            hidden: 0
          },
          include: [{
            relation: 'member',
            scope: {
              fields: ['id', 'displayName'],
              include: [{
                relation: 'profileImage',
                scope: {
                  fields: ['id', 'url'],
                  where: {
                    hidden: 0
                  }
                }
              }]
            }
          }]
        }
      }]
    }, function (err, feeds) {
      // Clean data
      // Not returning dangling feed (a feed without a post)
      var returnArray = [];
      for (var i = feeds.length - 1; i >= 0; i--) {
        if (feeds[i].post()) {
          returnArray.push(feeds[i]);
        }
      }

      // The fault is not in our stars.
      countingStars(returnArray, function (err, response) {
        // console.log(response);
        return callback(null, {
          results: response,
          next: response[response.length -1] && response.length === 10 ? bsonUrlEncoding.encode(response[response.length - 1].id): undefined
        });
      });

    });
  }

  Feed.remoteMethod('byMember', {
    http: {
      path: '/byMember',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });
};
