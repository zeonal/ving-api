var checkAccessToken = require('../lib/checkAccessToken');
var async = require('async');
var crypto = require('crypto');
var loopback = require('loopback');
var moment = require('moment');
var redisHelper = require('./../lib/redishelper');
var client = redisHelper.client;

module.exports = function(Post) {

  Post.beforeRemote('**', function(ctx, unused, next) {
    console.log('Post.beforeRemote');
    console.log(ctx.methodString);

    if (ctx.methodString === 'Post.createPost') { //post
      checkAccessToken(Post.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function(err) {
        next(err);
      });
    } else if (ctx.methodString === 'Post.updatePost') { //post
      checkAccessToken(Post, ctx.req.body.postId, ctx.req.accessToken, function(err) {
        next(err);
      });
    } else if (ctx.methodString === 'Post.deletePost') { //post
      checkAccessToken(Post, ctx.req.body.postId, ctx.req.accessToken, function(err) {
        next(err);
      });
    } else if (ctx.methodString === 'Post.byMember') { //get
      //Allow all
      next();
    } else if (ctx.methodString === 'Post.detail') { //post
      //Allow all
      next();
    } else if (ctx.methodString === 'Post.search') { //post
      //Allow all
      next();
    } else if (ctx.methodString === "Post.buyPost") {
      checkAccessToken(Post.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function(err) {
        next(err);
      });
    } else {
      if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
        var Member = Post.app.models.Member;
        Member.findById(ctx.req.accessToken.userId.toString(), function(err, memberObj) {
          if (err) {
            var errMsg = new Error('Database error.');
            errMsg.status = 500; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
          if (memberObj.username === 'admin') { //Admin -> allows all.
            next(); //OK
          } else {
            var errMsg = new Error('Not Authorized. You don\'t have a permission.');
            errMsg.status = 401; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
        })
      } else {
        var errMsg = new Error('Not Authorized. You don\'t have a permission.');
        errMsg.status = 401; // HTTP status code
        console.log(errMsg);
        next(errMsg);
      }
    }
  });

  Post.createPost = function(data, callback) {
    if (data.memberId) { //Doesn't required previewImageId
      Post.create(data, function(err, returnPost) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        var Feed = Post.app.models.Feed;
        Feed.create({ memberId: data.memberId, postId: returnPost.id }, function(err, returnFeed) {
          if (err) {
            console.log(err);
            return callback(err);
          }
          var Member = Post.app.models.Member;
          Member.findById(data.memberId, {
            fields: ['id'],
            include: [{
              relation: 'follower',
              scope: {
                fields: ['id']
              }
            }]
          }, function(err, returnMember) {
            if (err) {
              console.log(err);
              return callback(err);
            }
            async.each(returnMember.follower(), function(follower, eachCallback) {
              Feed.create({ memberId: follower.id, postId: returnPost.id }, function(err, unused) {
                if (err) {
                  console.log(err);
                  return eachCallback(err);
                }
                eachCallback(null);
              });
            }, function(err) {
              if (err) {
                console.log(err);
                return callback(err);
              }
              return callback(null, returnPost);
            });
          });
        });
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  Post.updatePost = function(data, callback) {
    if (data.postId) { //Doesn't required previewImageId
      Post.findById(data.postId, function(err, instance) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        delete data.postId;
        instance.hidden = false;
        instance.update_at = new Date();
        async.forEachOf(data, function(value, key, callbackforEachOf) {
          instance[key] = data[key] ? data[key] : instance[key];
          callbackforEachOf();
        }, function(err) {
          if (err) {
            console.log(err);
            return callback(err);
          }
          instance.save(function() {
            return callback(null, instance);
          });
        });
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };


  Post.deletePost = function(data, callback) {
    var Feed = Post.app.models.Feed;
    if (data.postId) {
      Post.findById(data.postId, {
        // fields: ['id'],
        include: [{
          relation: 'feed',
          scope: {
            fields: ['id']
          }
        }]
      }, function(err, returnPost) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        async.each(returnPost.feed(), function(feed, eachCallback) {
          Feed.destroyById(feed.id, function(err) {
            if (err) {
              console.log(err);
              return eachCallback(err);
            }
            eachCallback(null);
          });
        }, function(err) {
          if (err) {
            console.log(err);
            return callback(err);
          }
          if (!returnPost.id) {
            var error = new Error('Validate.');
            error.status = 422;
            return callback(error);
          };
          var Image = Post.app.models.Image;
          Image.destroyAll({
              where: {
                postId: returnPost.id
              }
            },
            function(err, info) {
              returnPost.destroy(function() {
                callback(null, { status: 'success' });
              });
            });
        });
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  Post.byMember = function(data, callback) {
    var Contract = Post.app.models.Contract;

    if (!data.page && typeof data.page === 'undefined') {
      data.page = 1;
    }
    if (data.memberId) {
      var Member = Post.app.models.Member;
      Member.findById(data.memberId, {
        fields: ['id', 'displayName'],
        include: [{
          relation: 'profileImage',
          scope: {
            fields: ['id', 'url'],
            where: {
              hidden: 0
            }
          }
        }, {
          relation: 'post',
          scope: {
            fields: ['id', 'memberId', 'defaultImageUrl', 'name', 'description', 'coin', 'create_at'],
            where: {
              hidden: 0
            },
            limit: 10,
            skip: ((parseInt(data.page) - 1) * 10),
            order: "create_at DESC",
          }
        }]
      }, function(err, returnMember) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        console.log('returnMember', returnMember);

        async.waterfall([

          // find post that coach coaches another member
          function(callback) {
            Contract.find({
              where: {
                coachId: returnMember.id
              },
              fields: 'postId'
            }, function(err, postIds) {

              if (err) {
                return callback(err);
              }

              console.log("post id: " + postIds);

              return callback(null, postIds);
            });
          },

        ], function(err, result) {

          if (err) {
            return callback(err);
          }

          async.forEach(result, function(postId, cb) {
            Post.find({
              where: {
                id: postId
              }
            }, function(err, aPost) {
              returnMember.post().push(aPost);
              cb();
            });
          }, function(err) {
            console.log("member's post: " + returnMember.post().length);
          });

        });

        //counting
        async.parallel({
            like: function(callback) {
              async.each(returnMember.post(), function(post, eachCallback) {
                if (!post.id) {
                  var error = new Error('Validate. Post does\'nt exist');
                  error.status = 422;
                  return eachCallback(error);
                }
                //sorted set
                console.log('like parallel', 'like:' + post.id.toString())
                var args1 = ['like:' + post.id.toString()];
                // var args1 = [ 'myzset', '+inf', '-inf' ];
                client.zcard(args1, function(err, reply) {
                  if (err) {
                    console.log(err);
                    return eachCallback(err);
                  }
                  post.countLike = reply;
                  return eachCallback();
                });
              }, function(err) {
                if (err) {
                  console.log(err);
                  return callback(err);
                }
                return callback(null);
              });
            },
            isLiked: function(callback) {
              async.each(returnMember.post(), function(post, eachCallback) {
                if (!post.id) {
                  var error = new Error('Validate. Post does\'nt exist');
                  error.status = 422;
                  return eachCallback(error);
                }
                //sorted set
                var args = ['like:' + post.id.toString(), '[' + post.memberId, '[' + post.memberId];
                client.zrangebylex(args, function(err, reply) {
                  if (err) {
                    console.log(err);
                    return eachCallback(err);
                  }
                  post.isLiked = reply.length > 0 ? true : false;
                  return eachCallback();
                });
              }, function(err) {
                if (err) {
                  console.log(err);
                  return callback(err);
                }
                return callback(null);
              });
            },
            comment: function(callback) {
              async.each(returnMember.post(), function(post, eachCallback) {
                if (!post.id) {
                  var error = new Error('Validate. Post does\'nt exist');
                  error.status = 422;
                  return eachCallback(error);
                }
                //sorted set
                client.zcard('comment:' + post.id.toString(), function(err, reply) {
                  if (err) {
                    console.log(err);
                    return eachCallback(err);
                  }
                  post.countComment = reply;
                  return eachCallback();
                });
              }, function(err) {
                if (err) {
                  console.log(err);
                  return callback(err);
                }
                return callback(null);
              });
            },
            share: function(callback) {
              async.each(returnMember.post(), function(post, eachCallback) {
                if (!post.id) {
                  var error = new Error('Validate. Post does\'nt exist');
                  error.status = 422;
                  return eachCallback(error);
                }
                //string
                client.get('share:' + post.id.toString(), function(err, reply) {
                  if (err) {
                    console.log(err);
                    return eachCallback(err);
                  }
                  if (!reply) {
                    post.countShare = 0;
                  } else {
                    post.countShare = reply;
                  }
                  return eachCallback();
                });
              }, function(err) {
                if (err) {
                  console.log(err);
                  return callback(err);
                }
                return callback(null);
              });
            }
          },
          //      // optional callback
          function(err) {
            if (err) {
              return callback(err);
            }
            return callback(null, returnMember);
          });
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  Post.detail = function(data, callback) {
    let Stat = Post.app.models.Stat;
    let myLocation;
    if (data.postId) {
      Post.findById(data.postId, {
        fields: ['id', 'memberId', 'defaultImageUrl', 'name', 'description', 'coin', 'create_at'],
        include: [{
          relation: 'member',
          scope: {
            fields: ['id', 'displayName'],
            include: [{
              relation: 'profileImage',
              scope: {
                fields: ['id', 'url'],
                where: {
                  hidden: 0
                }
              }
            }]
          }
        }, {
          relation: 'page',
          scope: {
            fields: ['id']
          }
        }]
      }, function(err, returnPost) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        console.log(returnPost);
        if (!returnPost) {
          var error = new Error('Validate. Post does\'nt exist');
          error.status = 422;
          return callback(error);
        }
        //counting
        async.parallel({
            like: function(callback) {
              client.zcard('like:' + returnPost.id.toString(), function(err, reply) {
                if (err) {
                  console.log(err);
                  return callback(err);
                }
                returnPost.countLike = reply;
                return callback();
              });
            },
            isLiked: function(callback) {
              //sorted set ZRANGEBYLEX
              var args = ['like:' + returnPost.id.toString(), '[' + returnPost.memberId, '[' + returnPost.memberId + 'a'];
              client.zrangebylex(args, function(err, reply) {
                if (err) {
                  console.log(err);
                  return callback(err);
                }
                returnPost.isLiked = reply.length > 0 ? true : false;
                return callback();
              });
            },
            comment: function(callback) {
              client.zcard('comment:' + returnPost.id.toString(), function(err, reply) {
                if (err) {
                  console.log(err);
                  return callback(err);
                }
                returnPost.countComment = reply;
                return callback();
              });
            },
            share: function(callback) {
              client.get('share:' + returnPost.id.toString(), function(err, reply) {
                if (err) {
                  console.log(err);
                  return callback(err);
                }
                if (!reply) {
                  returnPost.countShare = 0;
                } else {
                  returnPost.countShare = reply;
                }
                return callback();
              });
            }
          },
          //      // optional callback
          function(err) {
            if (err) {
              return callback(err);
            }
            if (typeof data.location === 'string') {
              try {
                myLocation = JSON.parse(data.location);
              } catch (e) {
                console.log(e);
              }
            } else {
              myLocation = data.location ? [parseFloat(data.location[0]), parseFloat(data.location[1])] : myLocation;
            }
            Stat.create({ memberId: data.memberId, postId: data.postId, ipaddress: data.ipaddress, location: myLocation }, function(err, stat) {
              if (err) {
                console.log(err);
                return callback(err);
              }
              return callback(null, returnPost);
            });
          });
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  /**
  API Search Post
  @param data type object
  ex.
  data: {
    keyword: 'test'
  }
  */
  Post.search = function(data, callback) {
    if (!data.page && typeof data.page === 'undefined') {
      data.page = 1;
    }
    console.log(data.keyword);
    Post.find({
      limit: 10,
      skip: ((parseInt(data.page) - 1) * 10),
      order: "create_at DESC",
      where: {
        name: {
          like: data.keyword,
          options: "i"
        }
      },
      include: ['interest', {
        relation: 'member',
        scope: {
          fields: ['id', 'displayName'],
          include: [{
            relation: 'profileImage',
            scope: {
              fields: ['id', 'url'],
              where: {
                hidden: 0
              }
            }
          }]
        }
      }]
    }, function(err, returnPosts) {
      if (err) {
        console.log(err);
        return callback(err);
      }
      var returnArray = [];
      if (data.interest) {
        for (var i = returnPosts.length - 1; i >= 0; i--) {
          if (returnPosts[i].interest() && returnPosts[i].interest().length > 0) {
            for (var j = returnPosts[i].interest().length - 1; j >= 0; j--) {
              if (returnPosts[i].interest()[j] && returnPosts[i].interest()[j].name === data.interest) {
                returnArray.push(returnPosts[i]);
                break;
              }
            };
          }
        }
        console.log(returnArray);
        return callback(null, returnArray);
      }
      console.log(returnPosts);
      return callback(null, returnPosts);
    });

  };


  Post.buyPost = function(data, callback) {
    if (data.postId && data.memberId) {
      //data.postId รหัสโพสที่ซื้อ , data.memberId รหัสผู้ซื้อ
      Post.findById(data.postId, {
        fields: ['id', 'memberId', 'coin'],
      }, function(err, dataPost) {
        if (err) {
          console.log('err Post.buyPost ', err);
          return callback(err);
        }


        let PurchasePost = Post.app.models.PurchasePost; //โพสที่ซื้อ
        let hashid = crypto.createHash('sha256').update(data.memberId + data.postId).digest('hex'); //สร้าง hash id for PurchasePost
        //***ตรวจสอบว่า เคยซื้อโพสนี้แล้วหรือยัง
        PurchasePost.findById(hashid, function(err, dataPurchasePost) {
          if (err) {
            return callback(err);
          }
          if (dataPurchasePost) { // ถ้ามีข้อมูล
            return callback(null, { status: 'success', message: 'Purchase Success.' });
          } else {

            let defaultExpiredate = 365 * 100; //จำนวณวันหมดอายุ
            let Contract = Post.app.models.Contract;
            let Member = Post.app.models.Member;
            let PurchaseHistory = Post.app.models.PurchaseHistory; //บันทึกประวัติการซื้อ Post
            let MemberBalance = Post.app.models.MemberBalance; //บันทึกจำนวนเงินที่ได้
            let MemberCoin = 0;
            let moneyForVing = Math.floor((parseFloat(dataPost.coin) * 30) / 100); //หักเปอเซ็นให้ Ving 30%
            let TotalCoin = parseFloat(dataPost.coin); //ราคาของโพสนี้
            //หักเปอเซ็นให้ Ving 30%
            TotalCoin = TotalCoin - moneyForVing;
            //คำณวณ วันหมดอายุ
            let Cal_expire_date = new Date(new Date().setDate(new Date().getDate() + defaultExpiredate));

            //เชค Coach From Post
            Contract.find({
              where: {
                postId: dataPost.postId,
              }
            }, function(err, dataContract) {
              if (err) {
                //  console.log('err',err);
                return callback(err);
              }

              /// Get MemberCoin
              Member.getBalanceTotal({ memberId: data.memberId }, function(err, dataMember) {
                if (err) {
                  return callback(err);
                }
                MemberCoin = dataMember.coin;
                if (MemberCoin < parseFloat(dataPost.coin)) { //ถ้าจำนวน Coin ไม่พอ
                  var error = new Error('Error not enough Coin');
                  error.status = 422;
                  error.coin = MemberCoin;
                  return callback(error);
                }
                /// Get MemberCoin

                async.waterfall([
                  function(cb) {
                    if (dataContract.length > 0) { // ถ้าโพสนี้มี  Coach

                      async.each(dataContract, function(Contr, eachCallback) {
                        //หักเปอเซ็นให้โคท ตามตกลง
                        let moneyForCoach = Math.floor((parseFloat(dataPost.coin) * parseFloat(Contr.percent)) / 100);
                        TotalCoin = TotalCoin - moneyForCoach;

                        //บันทึก Coin ให้ Coach จากการแบ่ง %
                        MemberBalance.create({ coin: moneyForCoach, memberId: Contr.coachId, postId: data.postId, expire_date: Cal_expire_date }, function(err, returnMemberBalance) {
                          if (err) {
                            console.log(err);
                            return callback(err);
                          };
                          return eachCallback(null);
                        });
                      }, function(err) {
                        if (err) {
                          console.log('async.each', err);
                          return callback(err);
                        }
                        //บันทึก coin ให้เจ้าของ Post
                        MemberBalance.create({ coin: TotalCoin, memberId: dataPost.memberId, postId: data.postId, expire_date: Cal_expire_date }, function(err, dataMemberBalance) {
                          if (err) {
                            console.log(err);
                            return callback(err);
                          };
                        });
                        cb(null, 'one');
                        //return callback(null, eachCallback);
                      });

                    } else { // ถ้าโพสนี้ไม่มี  Coach

                      //บันทึก coin ให้เจ้าของ Post
                      MemberBalance.create({ coin: TotalCoin, memberId: dataPost.memberId, postId: data.postId, expire_date: Cal_expire_date }, function(err, dataMemberBalance) {
                        if (err) {
                          console.log(err);
                          return callback(err);
                        };
                      });
                      cb(null, 'one');
                      //return callback(null, {status:'success',message:'Purchase Success'});
                    }

                  },
                  function(arg1, cb) {
                    //บันทึกโพสที่ซื้อ
                    PurchasePost.create({ id: hashid, expire_date: Cal_expire_date }, function(err, dataPurchasePost) {
                      if (err) {
                        console.log(err);
                        return callback(err);
                      };
                    });
                    cb(null, 'two');
                  },
                  function(arg1, cb) {
                    //บันทึกประวัติการซื้อ
                    PurchaseHistory.create({ memberId: data.memberId, postId: data.postId, coin: dataPost.coin, expire_date: Cal_expire_date }, function(err, dataPurchasePost) {
                      if (err) {
                        console.log(err);
                        return callback(err);
                      };
                    });
                    cb(null, 'done');
                  }
                ], function(err, result) {
                  // result now equals 'done'
                  if (err) {
                    console.log(err);
                    return callback(err);
                  };
                  return callback(null, { status: 'success', message: 'Purchase Success' });
                });
                //End async.waterfall


              });
              ///End Get MemberCoin
            });
            ////End เชค Coach From Post
          }

        });
        //***End ตรวจสอบว่า เคยซื้อโพสนี้แล้วหรือยัง


      });
      /// End Post.findById

    } else {
      var error = new Error('Error Validation! postId memberId');
      error.status = 422;
      return callback(error);
    }
  };

  Post.remoteMethod('detail', {
    http: {
      path: '/detail',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });


  Post.remoteMethod('createPost', {
    http: {
      path: '/createPost',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Post.remoteMethod('updatePost', {
    http: {
      path: '/updatePost',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Post.remoteMethod('deletePost', {
    http: {
      path: '/deletePost',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });


  Post.remoteMethod('byMember', {
    http: {
      path: '/byMember',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Post.remoteMethod('search', {
    http: {
      path: '/search',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Post.remoteMethod('buyPost', {
    http: {
      path: '/buyPost',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });
};
