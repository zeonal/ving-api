var checkAccessToken = require('../lib/checkAccessToken');
var bsonUrlEncoding = require('./../lib/bsonUrlEncoding');

module.exports = function (Notification) {

  Notification.beforeRemote('**', function (ctx, unused, next) {
    console.log('Notification.beforeRemote');
    console.log(ctx.methodString);

    if (ctx.methodString === 'Notification.createNotification') { //post
      checkAccessToken(Notification.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
        next(err);
      });
    } else if (ctx.methodString === 'Notification.byMember') { //post
      checkAccessToken(Notification.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
        next(err);
      });
    } else {
      if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
        var Member = Notification.app.models.Member;
        Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
          if (err) {
            var errMsg = new Error('Database error.');
            errMsg.status = 500; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
          if (memberObj.username === 'admin') { //Admin -> allows all.
            next(); //OK
          } else {
            var errMsg = new Error('Not Authorized. You don\'t have a permission.');
            errMsg.status = 401; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
        })
      } else {
        var errMsg = new Error('Not Authorized. You don\'t have a permission.');
        errMsg.status = 401; // HTTP status code
        console.log(errMsg);
        next(errMsg);
      }
    }
  });

  Notification.createNotification = function (data, callback) {
    //TODO: need implement
    if (data.memberId) {
      Notification.create(data, function (err, instance) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        return callback(null, instance);
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  Notification.byMember = function (data, callback) {
    if (!data.page && typeof data.page === 'undefined') {
      data.page = 1;
    }
    var query = {};
    if (data.next) {
      var next = bsonUrlEncoding.decode(data.next);
      query = {
        id: {
          lt: next.toString()
        }
      };
    }
    if (data.memberId) {
      var Member = Notification.app.models.Member
      Member.findById(data.memberId, {
        fields: ['id'],
        include: [{
          relation: 'notification',
          scope: {
            where: query,
            limit: 10,
            skip: ((parseInt(data.page) - 1) * 10),
            order: "create_at DESC",
            fields: ['id', 'name', 'action', 'message', 'postId'],
            include: [{
              relation: 'post',
              scope: {
                fields: ['id', 'memberId', 'defaultImageUrl', 'name', 'description', 'coin', 'status', 'create_at'],
                where: {
                  hidden: 0
                },
              }
            }, {
              relation: 'chat'
            }, {
              relation: 'member',
              scope: {
                fields: ['id', 'displayName'],
                include: [{
                  relation: 'profileImage',
                  scope: {
                    fields: ['id', 'url'],
                    where: {
                      hidden: 0
                    }
                  }
                }]
              }
            }]
          }
        }]
      }, function (err, returnMember) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        if (returnMember) {
          var results = returnMember.notification()
          return callback(null, {
            results: results,
            next: results[results.length - 1] && results.length == 10 ? bsonUrlEncoding.encode(results[results.length - 1].id) : undefined
          });
        } else {
          return callback(null, {
            status: 'error',
            message: 'Member not found.',
          });
        }
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  Notification.remoteMethod('createNotification', {
    http: {
      path: '/createNotification',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Notification.remoteMethod('byMember', {
    http: {
      path: '/byMember',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });
};
