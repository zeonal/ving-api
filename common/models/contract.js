var checkAccessToken = require('../lib/checkAccessToken');
var redisHelper = require('./../lib/redishelper');
var client = redisHelper.client;
var moment = require('moment');
var async = require('async');
var Helper = require('./../lib/helper');
var crypto = require('crypto');

module.exports = function (Contract) {

  Contract.beforeRemote('**', function (ctx, unused, next) {
    console.log('Member.beforeRemote');
    console.log(ctx.methodString);

    var Member = Contract.app.models.Member;

    var allowAll = [
      'Contract.listContent',
    ];

    var checkAccess = [
      'Contract.createOffer', 'Contract.detail', 'Contract.updateContract'
    ];

    // Allow All
    if (allowAll.indexOf(ctx.methodString) > -1) {
      return next();
    }

    if ('Contract.createOffer' === ctx.methodString) {
      checkAccessToken(Contract.app.models.Member, ctx.req.body.coachId, ctx.req.accessToken, function (err) {
        return next(err);
      });
    } else if ('Contract.detail' === ctx.methodString) {
      // checkAccessToken(Contract.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
      // TODO: FIXME: Find an efficient way to check access control
      return next();
      // });
    } else if ('Contract.updateContract' === ctx.methodString) {
      checkAccessToken(Contract.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
        return next(err);
      });
    } else {
      if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
        Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
          if (err) {
            var errMsg = new Error('Database error.');
            errMsg.status = 500; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
          if (memberObj.username === 'admin') { //Admin -> allows all.
            next(); //OK
          } else {
            var errMsg = new Error('Not Authorized. You don\'t have a permission.');
            errMsg.status = 401; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
        })
      } else {
        var errMsg = new Error('Not Authorized. You don\'t have a permission.');
        errMsg.status = 401; // HTTP status code
        console.log(errMsg);
        next(errMsg);
      }
    }
  });

  Contract.listContent = function (data, callback) {
    var Content = Contract.app.models.Content;

    Content.find({
      where: {
        hidden: 0,
      },
      fields: ['id', 'content']
    }, function (err, contents) {
      if (err) {
        console.log(err);
      }
      if (contents && contents.length === 0) {
        // content is null
        // create content on the fly
        // FIXME: should remove in production.
        ['ห้ามเนื้อหาที่ผิดศีลธรรม', 'ต้องเป็นผลงานที่ไม่ได้ถูกคัดลอกมาจากที่อื่น', 'ต้องมีคุณค่า'].map(function (item, index) {
          Content.create({ content: item });
          return item;
        });
        return callback(null, []);
      } else {
        return callback(null, contents);
      }

    });
  }

  Contract.remoteMethod('listContent', {
    http: {
      path: '/listContent',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  function findChat(Member, Chat, data, callback) {
    Member.findById(data.coachId, function (err, coach) {
      if (coach && coach.coach) {
        // Create message to coach.

        // Find existing chat
        Chat.find({
          where: {
            or: [{
              or: [{
                fromMemberId: data.memberId
              }, {
                toMemberId: data.coachId
              }]
            }, {
              or: [{
                fromMemberId: data.coachId
              }, {
                toMemberId: data.memberId
              }]
            }]
          },
        }, function (err, chat) {
          if (err) {
            console.log(err);
            return callback(err);
          }

          if (chat.length === 0) {
            var errMsg = new Error('Invalid Request.');
            errMsg.status = 422; // HTTP status code
            return callback(errMsg);
          } else {
            var currentChat = chat[0];
            console.log(currentChat);
            currentChat.status = 'active';
            currentChat.update_at = moment().toDate();
            currentChat.save(function (err, savedChat) {
              return callback(err, savedChat);
            });
          }
        });
      } else {
        var errMsg = new Error('Invalid Coach.');
        errMsg.status = 422; // HTTP status code
        return callback(errMsg);
      }
    });
  }

  Contract.createOffer = function (data, callback) {
    var Content = Contract.app.models.Content;
    var Member = Contract.app.models.Member;
    var Chat = Contract.app.models.Chat;
    var Message = Contract.app.models.Message;

    // Inline helper function to create a new message.
    function createMessage(chat, data, cb) {
      // var validate = validateData(data);
      var validate = Helper.validate('Contract', data);
      if (!validate.status) {
        validate.status = 'error';
        return cb(null, validate);
      }

      var model = data;
      if (data.contractId) {
        model.id = data.contractId.toString();
      } else {
        return cb({ status: 'error', message: 'contractId is undefined.' })
      }

      delete model.contractId;
      try {
        model.content = JSON.parse(data.content);
      } catch (e) {
        console.log(e);
      }


      model.status = 'offer';
      console.log(model, data);
      Contract.upsert(model, function (err, contract) {
        if (err) {
          console.log(err);
          return cb(err);
        }

        async.each(data.content, function (content, eachCallback) {
          Content.create({ content: content, contractId: contract.id }, function (err, content) {
            if (err) {
              return eachCallback(err);
            }
            return eachCallback();
          })

        }, function (err) {
          // Create a contract base on data
          Message.create({
            message: 'ได้ยื่นข้อเสนอการเป็นโค้ชให้คุณ',
            owner: data.memberId,
            status: 'unread',
            chatId: chat.id,
            type: 'coach',
            contractId: contract.id
          }, function (err, message) {
            if (err) {
              return cb(err);
            }
            return cb(null, {
              status: "success",
              message: "",
              data: message
            });
          });
        });

      });
    }

    if (data.memberId && data.coachId) {
      findChat(Member, Chat, data, function (err, chat) {
        if (err) {
          return callback(err);
        }
        createMessage(chat, data, function (err, returnValue) {
          if (err) {
            console.log(err);
            var errMsg = new Error(err);
            errMsg.status = 422; // HTTP status code
            return callback(errMsg);
          }
          return callback(null, returnValue);
        });
      });
    } else {
      var errMsg = new Error('Validate.');
      errMsg.status = 422; // HTTP status code
      return cb(errMsg);
    }
  }

  Contract.remoteMethod('createOffer', {
    http: {
      path: '/createOffer',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Contract.detail = function (data, callback) {
    Contract.findById(data.contractId, {
      include: [{
        relation: 'content',
        scope: {
          fields: ['id', 'content']
        }
      }, 'coach', 'member', 'post']
    }, function (err, contract) {
      if (err) {
        console.log(err);
        return callback(null, {
          status: 'error',
          message: 'contract not found.'
        });
      }
      if (contract) {
        return callback(null, contract);
      } else {
        return callback(null, {
          status: 'error',
          message: 'contract not found.'
        });
      }

    });
  }

  Contract.remoteMethod('detail', {
    http: {
      path: '/detail',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });



  Contract.updateContract = function (data, callback) {
    var Content = Contract.app.models.Content;
    var Member = Contract.app.models.Member;
    var Chat = Contract.app.models.Chat;
    var Message = Contract.app.models.Message;

    // Helper function to create message
    function createMessage(msg, owner, chatId, contractId, cb) {
      Message.create({
        message: msg,
        owner: owner,
        status: 'unread',
        chatId: chatId,
        contractId: contractId
      }, function (err, message) {
        if (err) {
          return cb(err);
        }
        return cb(null, {
          status: "success",
          message: "",
          data: message
        });
      });
    }

    // Inline helper function to update contract status
    function updateContractModel(chat, data, cb) {
      // var validate = validateData(data);
      // var validate = Helper.validate('Contract', data);
      // if (!validate.status) {
      //   return cb(validate.message);
      // }

      Contract.findById(data.contractId, function (err, contract) {
        if (err) {
          console.log(err);
          return cb(err);
        }

        if (contract) {
          console.log(contract);
          if (data.status === 'accept' && contract.status === 'offer') {
            // matched
            contract.status = 'active';
            contract.save(function (err) {
              if (err) {
                return cb(err);
              }
              if (contract.postId) {
                const hash = crypto.createHash('sha256').update(contract.coachId + contract.postId).digest('hex');
                var PurchasePost = Contract.app.models.PurchasePost;
                PurchasePost.upsert({
                  id: hash,
                  postId: contract.postId,
                  memberId: contract.coachId,
                  expire_date: contract.expire_date
                }, function (err, instance) {
                  if (err) {
                    console.log(err);
                    return cb(err);
                  }
                  createMessage('ได้ตอบรับคุณเป็นโค้ช', contract.coachId, chat.id, contract.id, function (err, data) {
                    if (err) {
                      return cb(err);
                    }
                    return cb(null, data);
                  });
                });
              } else if (contract.groupId) {
                const hash = crypto.createHash('sha256').update(contract.coachId + contract.groupId).digest('hex');
                var PurchaseGroup = Contract.app.models.PurchaseGroup;
                PurchaseGroup.upsert({
                  id: hash,
                  groupId: contract.groupId,
                  memberId: contract.coachId,
                  expire_date: contract.expire_date
                }, function (err, instance) {
                  if (err) {
                    console.log(err);
                    return cb(err);
                  }
                  createMessage('ได้ตอบรับคุณเป็นโค้ช', contract.coachId, chat.id, contract.id, function (err, data) {
                    if (err) {
                      return cb(err);
                    }
                    return cb(null, data);
                  });
                });
              };
            });
          } else if (data.status === 'reject' && contract.status === 'offer') {
            contract.status = data.status;
            var receiver = contract.coachId;
            if (data.memberId.toString() === contract.coachId.toString()) {
              receiver = contract.memberId;
              contract.status = 'rejectByCoach';
            } else {
              receiver = contract.coachId;
              contract.status = 'rejectByMember';
            }
            contract.save(function (err) {
              if (err) {
                return cb(err);
              }
              createMessage('ได้ปฏิเสธข้อเสนอของคุณ', receiver, chat.id, contract.id, function (err, data) {
                if (err) {
                  return cb(err);
                }
                return cb(null, data);
              });
            });

          } else {
            return cb(null, {
              status: "error",
              message: "Invalid request.",
            });
          }
        } else {
          return cb(null, {
            status: "error",
            message: "Contract not found.",
          });
        }
      });
    }

    if (data.memberId && data.contractId && data.status) {
      // Find existing chat
      findChat(Member, Chat, data, function (err, chat) {
        if (err) {
          return callback(err);
        }
        // Update contract based on receiving data
        updateContractModel(chat, data, function (err, returnValue) {
          if (err) {
            console.log(err);
            var errMsg = new Error(err);
            errMsg.status = 422; // HTTP status code
            return callback(errMsg);
          }
          // return status + message
          return callback(null, returnValue);
        });
      });
    } else {
      var errMsg = new Error('Validate.');
      errMsg.status = 422; // HTTP status code
      return cb(errMsg);
    }
  }

  Contract.remoteMethod('updateContract', {
    http: {
      path: '/updateContract',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });
}