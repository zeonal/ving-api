var checkAccessToken = require('../lib/checkAccessToken');
// var redis = require('redis'),
//   client = redis.createClient();
var redisHelper = require('./../lib/redishelper');
var client = redisHelper.client;
var loopback = require('loopback');
var async = require('async');
var crypto = require('crypto');
var bsonUrlEncoding = require('./../lib/bsonUrlEncoding');

module.exports = function (Follow) {

  Follow.beforeRemote('**', function (ctx, unused, next) {
    console.log('Follow.beforeRemote');
    console.log(ctx.methodString);

    if (ctx.methodString === 'Follow.follow') { //post
      checkAccessToken(Follow.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
        next(err);
      });
    } else if (ctx.methodString === 'Follow.unfollow') { //post
      checkAccessToken(Follow.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
        next(err);
      });
    } else if (ctx.methodString === 'Follow.getFollower') { //get
      //Allow all
      next();
    } else if (ctx.methodString === 'Follow.getFollowing') { //get
      //Allow all
      next();
    } else {
      if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
        var Member = Follow.app.models.Member;
        Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
          if (err) {
            var errMsg = new Error('Database error.');
            errMsg.status = 500; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
          if (memberObj.username === 'admin') { //Admin -> allows all.
            next(); //OK
          } else {
            var errMsg = new Error('Not Authorized. You don\'t have a permission.');
            errMsg.status = 401; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
        })
      } else {
        var errMsg = new Error('Not Authorized. You don\'t have a permission.');
        errMsg.status = 401; // HTTP status code
        console.log(errMsg);
        next(errMsg);
      }
    }
  });

  Follow.follow = function (data, callback) {
    if (data.memberId && data.followingMemberId) {
      //TODO: make it parellel
      //TODO: add post of new following to Feed
      var hash = crypto.createHash('sha256').update('following' + data.memberId + data.followingMemberId).digest('hex'); //following + memberId + followerMemberId
      //create following
      Follow.upsert({
        id: hash,
        memberId: data.memberId,
        followingMemberId: data.followingMemberId
      }, function (err, instance) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        hash = crypto.createHash('sha256').update('follower' + data.followingMemberId + data.memberId).digest('hex'); //follower + memberId + followerMemberId
        //create follower
        Follow.upsert({
          id: hash,
          memberId: data.followingMemberId,
          followerMemberId: data.memberId
        }, function (err, instance) {
          if (err) {
            console.log(err);
            return callback(err);
          }
          return callback(null, { status: 'success' });
        });
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  Follow.unfollow = function (data, callback) {
    //TODO: delete post of unfollowed member from Feed
    if (data.memberId && data.unFollowingMemberId) {
      var hash = crypto.createHash('sha256').update('following' + data.memberId + data.unFollowingMemberId).digest('hex'); //following + memberId + followerMemberId
      Follow.destroyById(hash, function (err) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        hash = crypto.createHash('sha256').update('follower' + data.unFollowingMemberId + data.memberId).digest('hex'); //following + memberId + followerMemberId
        Follow.destroyById(hash, function (err) {
          if (err) {
            console.log(err);
            return callback(err);
          }
          return callback(null, { status: 'success' });
        });
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  Follow.getFollower = function (data, callback) {
    if (!data.page && typeof data.page === 'undefined') {
      data.page = 1;
    }
    if (data.memberId) {
      var query = {
        hidden: 0
      };
      if (data.next) {
        var next = bsonUrlEncoding.decode(data.next);
        query = {
          id: {
            lt: next.toString()
          },
          hidden: 0
        };
      }
      var Member = Follow.app.models.Member;
      Member.findById(data.memberId, {
        include: [{
          relation: 'follower',
          scope: {
            where: query,
            limit: 10,
            order: "create_at DESC",
            include: [{
              relation: 'profileImage',
              scope: {
                fields: ['id', 'url'],
                where: {
                  hidden: 0
                }
              }
            }]
          }
        }]
      }, function (err, returnMember) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        if (returnMember) {
          var results = returnMember.follower()
          return callback(null, {
            results: results,
            next: results[results.length - 1] && results.length === 10 ? bsonUrlEncoding.encode(results[results.length - 1].id) : undefined
          });
        } else {
          return callback(null, {
            status: 'error',
            message: 'Member not found.',
          });
        }
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  Follow.getFollowing = function (data, callback) {
    if (!data.page && typeof data.page === 'undefined') {
      data.page = 1;
    }
    if (data.memberId) {
      var query = {
        hidden: 0
      };
      if (data.next) {
        var next = bsonUrlEncoding.decode(data.next);
        query = {
          id: {
            lt: next.toString()
          },
          hidden: 0
        };
      }
      var Member = Follow.app.models.Member;
      Member.findById(data.memberId, {
        include: [{
          relation: 'following',
          scope: {
            where: query,
            limit: 10,
            order: "create_at DESC",
            include: [{
              relation: 'profileImage',
              scope: {
                fields: ['id', 'url'],
                where: {
                  hidden: 0
                }
              }
            }]
          }
        }]
      }, function (err, returnMember) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        if (returnMember) {
          var results = returnMember.following()
          return callback(null, {
            results: results,
            next: results[results.length - 1] && results.length == 10 ? bsonUrlEncoding.encode(results[results.length - 1].id) : undefined
          });
        } else {
          return callback(null, {
            status: 'error',
            message: 'Member not found.',
          });
        }
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  Follow.remoteMethod('follow', {
    http: {
      path: '/follow',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Follow.remoteMethod('unfollow', {
    http: {
      path: '/unfollow',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Follow.remoteMethod('getFollowing', {
    http: {
      path: '/getFollowing',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Follow.remoteMethod('getFollower', {
    http: {
      path: '/getFollower',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

};