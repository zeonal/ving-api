var checkAccessToken = require('../lib/checkAccessToken');
var async = require('async');

module.exports = function (Report) {

	Report.beforeRemote('**', function (ctx, unused, next) {
    console.log('Report.beforeRemote');
    console.log(ctx.methodString);

    if (ctx.methodString === 'Report.createReport') { //post
      checkAccessToken(Report.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
        next(err);
      });
    } else if (ctx.methodString === 'Report.deleteReport') { //post
      checkAccessToken(Report, ctx.req.body.reportId, ctx.req.accessToken, function (err) {
        next(err);
      });
    } else {
      if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
        var Member = Report.app.models.Member;
        Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
          if (err) {
            var errMsg = new Error('Database error.');
            errMsg.status = 500; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
          if (memberObj.username === 'admin') { //Admin -> allows all.
            next(); //OK
          } else {
            var errMsg = new Error('Not Authorized. You don\'t have a permission.');
            errMsg.status = 401; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
        })
      } else {
        var errMsg = new Error('Not Authorized. You don\'t have a permission.');
        errMsg.status = 401; // HTTP status code
        console.log(errMsg);
        next(errMsg);
      }
    }
  });

	Report.createReport = function (data, callback) {
    if (data.memberId && (data.postId || data.commentId || data.groupId)) {
      Report.create({
        memberId: data.memberId,
        postId: data.postId,
        commentId: data.commentId,
        groupId: data.groupId,
        message: data.message
      }, function (err, instance) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        return callback(null, instance);
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  Report.deleteReport = function (data, callback) {
    if (data.reportId) {
      Report.destroyById(data.reportId, function (err) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        return callback(null, { status: 'success' });
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  Report.remoteMethod('createReport', {
    http: {
      path: '/createReport',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Report.remoteMethod('deleteReport', {
    http: {
      path: '/deleteReport',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

};
