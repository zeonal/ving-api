var checkAccessToken = require('../lib/checkAccessToken');
var bsonUrlEncoding = require('./../lib/bsonUrlEncoding');
var request = require('request');
var crypto = require('crypto');
var async = require('async');
var moment = require('moment');
var bsonUrlEncoding = require('./../lib/bsonUrlEncoding');

module.exports = function (Group) {

  Group.beforeRemote('**', function (ctx, unused, next) {
    console.log('Group.beforeRemote');
    console.log(ctx.methodString);

    if (ctx.methodString === 'Group.createGroup') { //post
      checkAccessToken(Group.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
        next(err);
      });
    } else if (ctx.methodString === 'Group.deleteGroup') { //post
      checkAccessToken(Group, ctx.req.body.groupId, ctx.req.accessToken, function (err) {
        next(err);
      });
    } else if (ctx.methodString === 'Group.byMember') { //get
      //Allow all
      next();
    } else if (ctx.methodString === 'Group.getGroup') { //get
      //Allow all
      next();
    } else if (ctx.methodString === 'Group.subscribeGroup') { //get
      //Allow all
      next();
    } else if (ctx.methodString === 'Group.subscribeByMember') { //get
      //Allow all
      next();
    }else {
      if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
        var Member = Group.app.models.Member;
        Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
          if (err) {
            var errMsg = new Error('Database error.');
            errMsg.status = 500; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
          if (memberObj.username === 'admin') { //Admin -> allows all.
            next(); //OK
          } else {
            var errMsg = new Error('Not Authorized. You don\'t have a permission.');
            errMsg.status = 401; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
        })
      } else {
        var errMsg = new Error('Not Authorized. You don\'t have a permission.');
        errMsg.status = 401; // HTTP status code
        console.log(errMsg);
        next(errMsg);
      }
    }
  });

  Group.createGroup = function(data, callback) {
    if (data.memberId && data.name) { //Doesn't required previewImageId
      Group.create(data, function(err, instance) {

        if (err) {
          console.log(err);
          return callback(err);
        }
        return callback(null, { status: 'success', results: instance});
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  Group.deleteGroup = function (data, callback) {
    if (data.groupId) {
      Group.destroyById(data.groupId, function (err) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        return callback(null, { status: 'success' });
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  Group.byMember = function (data, callback) {
    if (!data.page && typeof data.page === 'undefined') {
      data.page = 1;
    }

    if (data.memberId) {
      var query = {
        hidden: 0
      };
      if (data.next) {
        var next = bsonUrlEncoding.decode(data.next);
        query = {
          id: {
            lt: next.toString()
          },
          hidden: 0
        };
      }
      var Member = Group.app.models.Member;
      Member.findById(data.memberId, {
        fields: ['id', 'displayName'],
        include: [{
          relation: 'profileImage',
          scope: {
            fields: ['id', 'url'],
            where: {
              hidden: 0
            }
          }
        }, {
          relation: 'group',
          scope: {
            where: query,
            limit: 10,
            order: "create_at DESC",
            include: [{
              relation: 'post',
              scope: {
                fields: ['id', 'defaultImageUrl', 'name', 'description', 'coin', 'create_at'],
                // limit: 1, //Fix this
                order: "create_at ASC"
              }
            }]
          }
        }]
      }, function (err, returnMember) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        if (returnMember) {

          var results = returnMember.group();
          return callback(null, {
            results: returnMember,
            next: results[results.length - 1] && results.length == 10 ? bsonUrlEncoding.encode(results[results.length - 1].id) : undefined
          });
        } else {
          return callback(null, {
            status: 'error',
            message: 'Member not found.',
          });
        }
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };


  Group.getGroup = function (data, callback) {
    if (!data.page && typeof data.page === 'undefined') {
      data.page = 1;
    }
    var query = {
      hidden: 0
    };
    if (data.next) {
      var next = bsonUrlEncoding.decode(data.next);
      query = {
        id: {
          lt: next.toString()
        },
        hidden: 0
      };
    }
    Group.find({
      where: query,
      limit: 10,
      order: "create_at DESC",
      include: [{
        relation: 'member',
        scope: {
          fields: ['id', 'memberId', 'displayName'],
          where: {
            hidden: 0
          },
          include: [{
            relation: 'profileImage',
            scope: {
              fields: ['id', 'url'],
              where: {
                hidden: 0
              }
            }
          }]
        }
      }, {
        relation: 'post',
        scope: {
          fields: ['id', 'defaultImageUrl', 'name', 'description', 'coin', 'create_at'],
          // limit: 1, //Fix this
          order: "create_at ASC"
        }
      }]
    }, function (err, instances) {
      if (err) {
        console.log(err);
        return callback(err);
      }
      if (instances) {
        var results = instances;
        return callback(null, {
          results: results,
          next: results[results.length - 1] && results.length == 10 ? bsonUrlEncoding.encode(results[results.length - 1].id) : undefined
        });
      } else {
        return callback(null, {
          status: 'error',
          message: 'Group not found.',
        });
      }
    });
  };
  Group.subscribeGroup = function (data, callback) {
    var Contract = Group.app.models.Contract;
    var PurchaseHistory = Group.app.models.PurchaseHistory;
    var PurchaseGroup = Group.app.models.PurchaseGroup;
    var MemberBalance = Group.app.models.MemberBalance;
    var Member = Group.app.models.Member;
    var Notification = Group.app.models.Notification;
    var id;
    var sum = 0;
    var total = 0;

    if (data.groupId && data.memberId) {
      Member.getBalanceTotal({ memberId: data.memberId }, function (err, balance) {
        Group.findById(data.groupId, function (err, myGroup) {
          if (err) {
            return callback(err);
          }
          if (balance.coin >= myGroup.coin) {
            PurchaseHistory.create({
              coin: myGroup.coin,
              groupId: myGroup.id,
              memberId: data.memberId
            }, function (err, purHistory) {
              if (err) {
                return callback(err);
              }
              var id = data.memberId + data.groupId;
              var hash = crypto.createHash('sha256').update(id).digest('hex');

              PurchaseGroup.upsert({
                id: hash,
                groupId: myGroup.id,
                memberId: data.memberId,
                expire_date: moment().add(30, 'day')
              }, function (err, purGroup) {
                if (err) {
                  return callback(err);
                }
                Contract.find({
                  where: {
                    groupId: data.groupId
                  }
                }, function (err, contracts) {
                  if (err) {
                    return callback(err);
                  }
                  async.forEach(contracts, function (contract, cb) {
                    sum = (contract.percent / 100) * myGroup.coin;

                    MemberBalance.create({
                      memberId: contract.coachId,
                      groupId: myGroup.id,
                      coin: sum
                    }, function (err, memberBalance) {
                      if (err) {
                        return cb(err);
                      }
                      total = sum + total;
                      cb();
                    });
                  }, function (err) {
                    if (err) {
                      console.log(err);
                    }
                    var balMember = myGroup.coin - total;
                    MemberBalance.create({
                      memberId: myGroup.memberId,
                      groupId: myGroup.id,
                      coin: balMember
                    }, function (err, memberBal) {
                      Notification.create({
                        action: 'subscribe',
                        memberId: myGroup.memberId,
                        message: "subscribe กรุ๊ปของคุณ",
                        groupId: myGroup.id
                      }, function (err, noti) {
                        if (err) {
                          console.log(err);
                        }
                        return callback(null, {
                          status: 'success',
                          expire_date: purGroup.expire_date
                        });
                      })
                    })
                  })
                })
              })
            })
          } else {
            var error = new Error('Error not enough Coin');
            error.status = 422;
            error.coin = balance.coin;
            return callback(error);
          }
        })
      })
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };


  Group.subscribeByMember = function (data, callback) {
    if (!data.memberId) {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
    var query = {
      memberId: data.memberId
    };
    if (data.next) {
      var next = bsonUrlEncoding.decode(data.next);
      query = {
        id: {
          lt: next.toString()
        },
        memberId: data.memberId,
        hidden: 0
      };
    }
    var PurchaseGroup = Group.app.models.PurchaseGroup;
    PurchaseGroup.find({
      where: query,
      order: "id DESC",
      limit: 10,
      include: [{
        relation: 'group',
        scope: {
          fields: ['id', 'memberId', 'defaultImageUrl', 'name', 'description', 'coin', 'status', 'create_at'],
          where: {
            hidden: 0
          },
          include: [{
            relation: 'member',
            scope: {
              fields: ['id', 'displayName'],
              include: [{
                relation: 'profileImage',
                scope: {
                  fields: ['id', 'url'],
                  where: {
                    hidden: 0
                  }
                }
              }]
            }
          }, {
            relation: 'post',
            scope: {
              fields: ['id', 'defaultImageUrl', 'name', 'description', 'coin', 'create_at'],
              // limit: 1, //Fix this
              order: "create_at ASC"
            }
          }]
        }
      }]
    }, function (err, returnPurchaseGroup) {
      if (err) {
        console.log(err);
        return callback(err);
      }
      if (returnPurchaseGroup.group()) {
        var results = returnPurchaseGroup;
        return callback(null, {
          results: returnPurchaseGroup.group(), //not return results, dif from others
          next: results[results.length - 1] && results.length == 10 ? bsonUrlEncoding.encode(results[results.length - 1].id) : undefined
        });
      } else {
        return callback(null, {
          status: 'error',
          message: 'Group not found.',
        });
      }
    });
  }


  Group.detail = function (data, callback) {
    let Stat = Group.app.models.Stat;
    let myLocation;

    Group.findOne({
      where: {
        id: data.groupId
      },
      include: [{
        relation: 'member',
        scope: {
          fields: ['id', 'memberId', 'displayName'],
          where: {
            hidden: 0
          },
          include: [{
            relation: 'profileImage',
            scope: {
              fields: ['id', 'url'],
              where: {
                hidden: 0
              }
            }
          }]
        }
      }, {
        relation: 'post',
        scope: {
          fields: ['id', 'defaultImageUrl', 'name', 'description', 'coin', 'create_at'],
          // limit: 1, //Fix this
          order: "create_at ASC"
        }
      }]
    }, function (err, group) {
      if (err) {
        console.log(err);
        return callback(err);
      }
      if (group) {
        if (typeof data.location === 'string') {
          try {
            myLocation = JSON.parse(data.location);
          } catch (e) {
            console.log(e);
          }
        } else {
          myLocation = data.location ? [parseFloat(data.location[0]), parseFloat(data.location[1])] : data.location;
        }
        Stat.create({ groupId: data.groupId, memberId: data.memberId, location: myLocation }, function(err, stat) {
          if (err) {
            console.log(err);
            return callback(err);
          }
          return callback(null, group);
        });
      } else {
        return callback(null, {
          status: 'error',
          message: 'Group not found.',
        });
      }
    });   
  };


  Group.remoteMethod('createGroup', {
    http: {
      path: '/createGroup',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Group.remoteMethod('deleteGroup', {
    http: {
      path: '/deleteGroup',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Group.remoteMethod('byMember', {
    http: {
      path: '/byMember',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Group.remoteMethod('getGroup', {
    http: {
      path: '/getGroup',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });
  Group.remoteMethod('subscribeGroup', {
    http: {
      path: '/subscribeGroup',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Group.remoteMethod('subscribeByMember', {
    http: {
      path: '/subscribeByMember',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Group.remoteMethod('detail', {
    http: {
      path: '/detail',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });
};
