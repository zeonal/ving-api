var async = require('async');
var checkAccessToken = require('../lib/checkAccessToken');
var crypto = require('crypto');

module.exports = function (Page) {

	Page.beforeRemote('**', function (ctx, unused, next) {
		console.log('Page.beforeRemote');
		console.log(ctx.methodString);

		if (ctx.methodString === 'Page.createPage') { //post
			checkAccessToken(Page.app.models.Post, ctx.req.body.postId, ctx.req.accessToken, function (err) {
				next(err);
			});
		} else if (ctx.methodString === 'Page.updatePage') { //post
			checkAccessToken(Page, ctx.req.body.pageId, ctx.req.accessToken, function (err) {
				next(err);
			});
		} else if (ctx.methodString === 'Page.deletePage') { //post
			checkAccessToken(Page, ctx.req.body.pageId, ctx.req.accessToken, function (err) {
				next(err);
			});
		} else if (ctx.methodString === 'Page.detail') { //post
			//check if owner
			checkAccessToken(Page, ctx.req.body.pageId, ctx.req.accessToken, function (err) {
				if (!err) {
					next(null);
				} else {
					Page.findById(ctx.req.body.pageId, {
						include: [{
							relation: 'post',
							scope: {
								fields: ['id', 'coin'],
								include: [{
									relation: 'group',
									scope: {
										fields: ['id', 'coin', 'expire_date']
									}
								}]
							}
						}]
					}, function (err, returnPage) {
						if (err) {
							console.log(err);
							return next(err);
						} else if (!returnPage || !returnPage.post() || !ctx.req.accessToken) {
							var error = new Error('Validate.');
							error.status = 422;
							return next(error);
						} else if (returnPage.post().coin === 0 && (!returnPage.post().group() || returnPage.post().group().coin === 0)) {
							return next(null);
						}
						var hash = crypto.createHash('sha256').update(ctx.req.accessToken.userId + returnPage.post().id).digest('hex'); //subscribe post
						checkAccessToken(Page.app.models.PurchasePost, hash, ctx.req.accessToken, function (err) {
							if (err) {
								if (returnPage.post().group() && returnPage.post().group().expire_date >= moment.tz("Asia/Bangkok").toDate()) {
									hash = crypto.createHash('sha256').update(ctx.req.accessToken.userId + returnPage.post().group().id).digest('hex'); //subscribe post
									checkAccessToken(Page.app.models.PurchaseGroup, hash, ctx.req.accessToken, function (err2) {
										return next(err2);
									});
								} else {
									var errMsg = new Error('Not Authorized. You don\'t have a permission.');
									errMsg.status = 401; // HTTP status code
									console.log(errMsg);
									return next(errMsg);
								}
							} else {
								return next(null);
							}
						});
					});
				}
			});
		} else {
			if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
				var Member = Page.app.models.Member;
				Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
					if (err) {
						var errMsg = new Error('Database error.');
						errMsg.status = 500; // HTTP status code
						console.log(errMsg);
						return next(errMsg);
					}
					if (memberObj.username === 'admin') { //Admin -> allows all.
						next(); //OK
					} else {
						var errMsg = new Error('Not Authorized. You don\'t have a permission.');
						errMsg.status = 401; // HTTP status code
						console.log(errMsg);
						return next(errMsg);
					}
				})
			} else {
				var errMsg = new Error('Not Authorized. You don\'t have a permission.');
				errMsg.status = 401; // HTTP status code
				console.log(errMsg);
				return next(errMsg);
			}
		}
	});

	Page.createPage = function (data, callback) {
		if (data.memberId && data.postId && data.templateId && data.element) { //Doesn't required previewImageId
			console.log(data);
			Page.create(data, function (err, instance) {
				if (err) {
					console.log(err);
					return callback(err);
				}
				return callback(null, instance);
			});
		} else {
			var error = new Error('Validate.');
			error.status = 422;
			return callback(error);
		}
	};

	Page.updatePage = function (data, callback) {
		if (data.pageId && (data.element)) {
			data.update_at = new Date();
			Page.findById(data.pageId, function (err, pageObj) {
				async.forEachOf(data, function (value, key, callbackforEachOf) {
					pageObj[key] = data[key] ? data[key] : pageObj[key];
					callbackforEachOf();
				}, function (err) { //end forEachOf
					if (err) {
						console.log(err);
						return callback(err);
					}
					pageObj.save();
					return callback(null, pageObj);
				});
			});
		} else {
			var error = new Error('Validate.');
			error.status = 422;
			return callback(error);
		}
	};

	Page.deletePage = function (data, callback) {
		if (data.pageId) {
			Page.findById(data.pageId, function (err, instance) {
				if (err) {
					console.log(err);
					return callback(err);
				}
				if (instance.id) {
					var Image = Page.app.models.Image;
					Image.destroyAll({
						pageId: instance.id
					}, function (err, info) {
						if (err) {
							console.log(err);
							return callback(err);
						}
						instance.destroy(function () {
							return callback(null, { status: 'success' });
						})
					});
				} else {
					instance.destroy(function () {
						return callback(null, { status: 'success' });
					})
				}
			});
		} else {
			var error = new Error('Validate.');
			error.status = 422;
			return callback(error);
		}
	};

	Page.detail = function (data, callback) {
		if (!data.page && typeof data.page === 'undefined') {
			data.page = 1;
		}
		var Stat = Page.app.models.Stat;
		var myLocation;
		if (typeof data.location === 'string') {
			try {
				myLocation = JSON.parse(data.location);
				myLocation = data.location ? [parseFloat(data.location[0]), parseFloat(data.location[1])] : myLocation;
			} catch (err) {
				console.log(err);
			}
		}
		Stat.create({ memberId: data.memberId, pageId: data.pageId, ipaddress: data.ipaddress, location: myLocation }, function (err, stat) {
			if (err) {
				console.log(err);
			}
		});
		if (data.pageId) {
			Page.findById(data.pageId, {
				include: ['template', {
					relation: 'image',
					scope: {
						fields: ['id', 'url'],
						where: {
							hidden: 0
						},
						order: "create_at ASC"
					}
				}]
			}, function (err, instances) {
				if (err) {
					console.log(err);
					return callback(err);
				}
				return callback(null, instances);
			});
		} else {
			var error = new Error('Validate.');
			error.status = 422;
			return callback(error);
		}
	};


	Page.remoteMethod('createPage', {
		http: {
			path: '/createPage',
			verb: 'post'
		},
		accepts: [{
			arg: 'data',
			type: 'object',
			http: {
				source: 'body'
			}
		}],
		returns: {
			type: 'object',
			root: true
		}
	});

	Page.remoteMethod('updatePage', {
		http: {
			path: '/updatePage',
			verb: 'post'
		},
		accepts: [{
			arg: 'data',
			type: 'object',
			http: {
				source: 'body'
			}
		}],
		returns: {
			type: 'object',
			root: true
		}
	});

	Page.remoteMethod('deletePage', {
		http: {
			path: '/deletePage',
			verb: 'post'
		},
		accepts: [{
			arg: 'data',
			type: 'object',
			http: {
				source: 'body'
			}
		}],
		returns: {
			type: 'object',
			root: true
		}
	});

	Page.remoteMethod('detail', {
		http: {
			path: '/detail',
			verb: 'post'
		},
		accepts: [{
			arg: 'data',
			type: 'object',
			http: {
				source: 'body'
			}
		}],
		returns: {
			type: 'object',
			root: true
		}
	});

};
