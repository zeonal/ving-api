var checkAccessToken = require('../lib/checkAccessToken');
var async = require('async');
var redisHelper = require('./../lib/redishelper');
var client = redisHelper.client;
var bsonUrlEncoding = require('./../lib/bsonUrlEncoding');

module.exports = function (Suggestion) {

	Suggestion.beforeRemote('**', function (ctx, unused, next) {
		console.log('Suggestion.beforeRemote');
		console.log(ctx.methodString);

		if (ctx.methodString === 'Suggestion.getMemberSuggestion') { //post
			//Allow all
			next();
		} else if (ctx.methodString === 'Suggestion.getPostSuggestion') { //post
			//Allow all
			next();
		} else {
			if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
				var Member = Suggestion.app.models.Member;
				Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
					if (err) {
						var errMsg = new Error('Database error.');
						errMsg.status = 500; // HTTP status code
						console.log(errMsg);
						next(errMsg);
					}
					if (memberObj.username === 'admin') { //Admin -> allows all.
						next(); //OK
					} else {
						var errMsg = new Error('Not Authorized. You don\'t have a permission.');
						errMsg.status = 401; // HTTP status code
						console.log(errMsg);
						next(errMsg);
					}
				})
			} else {
				var errMsg = new Error('Not Authorized. You don\'t have a permission.');
				errMsg.status = 401; // HTTP status code
				console.log(errMsg);
				next(errMsg);
			}
		}
	});

	//for admin
	Suggestion.createSuggestion = function (data, callback) {
		if (data.memberId || data.postId) {
			Suggestion.create(data, function (err, instance) {
				if (err) {
					console.log(err);
					return callback(err);
				}
				return callback(null, { status: 'success' });
			});
		} else {
			var error = new Error('Validate.');
			error.status = 422;
			return callback(error);
		}
	};

	//for admin
	Suggestion.deleteSuggestion = function (data, callback) {
		if (data.suggestionId) {
			Suggestion.destroyById(data.suggestionId, function (err) {
				if (err) {
					console.log(err);
					return callback(err);
				}
				return callback(null, { status: 'success' });
			});
		} else {
			var error = new Error('Validate.');
			error.status = 422;
			return callback(error);
		}
	};

	Suggestion.getMemberSuggestion = function (data, callback) {
		if (!data.page && typeof data.page === 'undefined') {
			data.page = 1;
		}
		var query = {
			memberId: {
				exists: true
			}
    };
    if (data.next) {
      var next = bsonUrlEncoding.decode(data.next);
      query = {
				memberId: {
					exists: true
				},
        id: {
          lt: next.toString()
        }
      };
    }
		Suggestion.find({
			where: query,
			fields: ['id', 'memberId'],
			limit: 10,
			skip: ((parseInt(data.page) - 1) * 10),
			order: "create_at DESC",
			include: [{
				relation: 'member',
				scope: {
					fields: ['id', 'displayName'],
					include: [{
						relation: 'profileImage',
						scope: {
							fields: ['id', 'url'],
							where: {
								hidden: 0
							}
						}
					}]
				}
			}]
		}, function (err, instances) {
			if (err) {
				console.log(err);
				return callback(err);
			}
			return callback(null, {
				results: instances,
				next: instances[instances.length - 1] && instances.length == 10 ? bsonUrlEncoding.encode(instances[instances.length - 1].id) : undefined
			});
		});
	};


	Suggestion.getPostSuggestion = function (data, callback) {
		if (!data.page && typeof data.page === 'undefined') {
			data.page = 1;
		}
		var query = {
			postId: {
				exists: true
			}
    };
    if (data.next) {
      var next = bsonUrlEncoding.decode(data.next);
      query = {
				postId: {
					exists: true
				},
        id: {
          lt: next.toString()
        }
      };
    }
		Suggestion.find({
			// fields: ['postId'],
			where: query,
			limit: 10,
			skip: ((parseInt(data.page) - 1) * 10),
			order: "postId DESC",
			include: [{
				relation: 'post',
				scope: {
					fields: ['id', 'memberId', 'defaultImageUrl', 'name', 'description', 'coin', 'create_at'],
					order: "create_at DESC",
					include: [{
						relation: 'member',
						scope: {
							fields: ['id', 'displayName'],
							include: [{
								relation: 'profileImage',
								scope: {
									fields: ['id', 'url'],
									where: {
										hidden: 0
									}
								}
							}]
						}
					}]
				}
			}]
		}, function (err, instances) {
			if (err) {
				console.log(err);
				return callback(err);
			}
			if (!instances || instances.length < 1) {
				return callback(null, instances);
			};
			//counting
			async.parallel({
					like: function (callback) {
						async.each(instances, function (instance, eachCallback) {
							if (!instance.post) {
								var error = new Error('Validate. Post does\'nt exist');
								error.status = 422;
								return callback(error);
							}
							console.log('instance', instance);
							client.zcard('like:' + instance.post().id.toString(), function (err, reply) {
								if (err) {
									console.log(err);
									return callback(err);
								}
								instance.post.countLike = reply;
								return eachCallback();
							});
						}, function (err) {
							if (err) {
								console.log(err);
								return callback(err);
							}
							return callback(null);
						});
					},
					isLiked: function (callback) {
						async.each(instances, function (instance, eachCallback) {
							if (!instance.post) {
								var error = new Error('Validate. Post does\'nt exist');
								error.status = 422;
								return callback(error);
							}
							//sorted set
							var args = ['like:' + instance.post().id.toString(), '[' + instance.post.memberId, '[' + instance.post.memberId + 'a'];
							client.zrangebylex(args, function (err, reply) {
								if (err) {
									console.log(err);
									return callback(err);
								}
								instance.post.isLiked = reply.length ? true : false;
								return eachCallback();
							});
						}, function (err) {
							if (err) {
								console.log(err);
								return callback(err);
							}
							return callback(null);
						});
					},
					comment: function (callback) {
						async.each(instances, function (instance, eachCallback) {
							if (!instance.post) {
								var error = new Error('Validate. Post does\'nt exist');
								error.status = 422;
								return callback(error);
							}
							client.zcard('comment:' + instance.post().id.toString(), function (err, reply) {
								if (err) {
									console.log(err);
									return callback(err);
								}
								instance.post.countComment = reply;
								return eachCallback();
							});
						}, function (err) {
							if (err) {
								console.log(err);
								return callback(err);
							}
							return callback(null);
						});
					},
					share: function (callback) {
						async.each(instances, function (instance, eachCallback) {
							if (!instance.post) {
								var error = new Error('Validate. Post does\'nt exist');
								error.status = 422;
								return callback(error);
							}
							client.get('share:' + instance.post().id.toString(), function (err, reply) {
								if (err) {
									console.log(err);
									return callback(err);
								}
								if (!reply) {
									instance.post.countShare = 0;
								} else {
									instance.post.countShare = reply;
								}
								return eachCallback();
							});
						}, function (err) {
							if (err) {
								console.log(err);
								return callback(err);
							}
							return callback(null);
						});
					}
				},
				// optional callback
				function (err) {
					if (err) {
						return callback(err);
					}

          return callback(null, {
            results: instances,
            next: instances[instances.length - 1] && instances.length == 10 ? bsonUrlEncoding.encode(instances[instances.length - 1].id) : undefined
          });

					//return callback(null, instances);
				});
		});
	};

	Suggestion.remoteMethod('createSuggestion', {
		http: {
			path: '/createSuggestion',
			verb: 'post'
		},
		accepts: [{
			arg: 'data',
			type: 'object',
			http: {
				source: 'body'
			}
		}],
		returns: {
			type: 'object',
			root: true
		}
	});

	Suggestion.remoteMethod('deleteSuggestion', {
		http: {
			path: '/deleteSuggestion',
			verb: 'post'
		},
		accepts: [{
			arg: 'data',
			type: 'object',
			http: {
				source: 'body'
			}
		}],
		returns: {
			type: 'object',
			root: true
		}
	});

	Suggestion.remoteMethod('getMemberSuggestion', {
		http: {
			path: '/getMemberSuggestion',
			verb: 'post'
		},
		accepts: [{
			arg: 'data',
			type: 'object',
			http: {
				source: 'body'
			}
		}],
		returns: {
			type: 'object',
			root: true
		}
	});

	Suggestion.remoteMethod('getPostSuggestion', {
		http: {
			path: '/getPostSuggestion',
			verb: 'post'
		},
		accepts: [{
			arg: 'data',
			type: 'object',
			http: {
				source: 'body'
			}
		}],
		returns: {
			type: 'object',
			root: true
		}
	});

};
