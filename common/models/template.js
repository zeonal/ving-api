var checkAccessToken = require('../lib/checkAccessToken');

module.exports = function (Template) {

	//special model, only admin can execute
	Template.beforeRemote('**', function (ctx, unused, next) {
		console.log('Template.beforeRemote');
		console.log(ctx.methodString);

		if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
			var Member = Template.app.models.Member;
			Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
				if (err) {
					var errMsg = new Error('Database error.');
					errMsg.status = 500; // HTTP status code
					console.log(errMsg);
					next(errMsg);
				}
				if (memberObj && memberObj.username === 'admin') { //Admin -> allows all.
					next(); //OK
				} else {

					if (ctx.methodString === 'Template.getTemplate') { //post
						//Allow all
						next();
					} else {
						var errMsg = new Error('Not Authorized. You don\'t have a permission.');
						errMsg.status = 401; // HTTP status code
						console.log(errMsg);
						next(errMsg);
					}
				}
			})
		} else {
			var errMsg = new Error('Not Authorized. You don\'t have a permission.');
			errMsg.status = 401; // HTTP status code
			console.log(errMsg);
			next(errMsg);
		}
	});

	//for admin
	Template.createTemplate = function (data, callback) {
		if (data.themeId && data.css && data.html && data.element) { //Doesn't required previewImageId
			Template.create(data, function (err, instance) {
				if (err) {
					console.log(err);
					return callback(err);
				}
				return callback(null, instance);
			});
		} else {
			var error = new Error('Validate.');
			error.status = 422;
			return callback(error);
		}
	};

	//for admin
	Template.updateTemplate = function (data, callback) {
		if (data.templateId && (data.html || data.css || data.element)) {
			data.update_at = new Date();
			Template.findById(data.templateId, function (err, templateObj) {
				async.forEachOf(data, function (value, key, callbackforEachOf) {
					templateObj[key] = data[key] ? data[key] : templateObj[key];
					callbackforEachOf();
				}, function (err) { //end forEachOf
					if (err) {
						console.log(err);
						return callback(err);
					}
					templateObj.save(function () {
						return callback(null, templateObj);
					});
				});
			});
		} else {
			var error = new Error('Validate.');
			error.status = 422;
			return callback(error);
		}
	};

	//for admin
	Template.deleteTemplate = function (data, callback) {
		if (data.templateId) {
			Template.findById(data.templateId, function (err, instance) {
				if (err) {
					console.log(err);
					return callback(err);
				}
				if (instance.id) {
					var Image = Template.app.models.Image;
					Image.destroyAll({
						templateId: instance.id
					}, function (err, info) {
						if (err) {
							console.log(err);
							return callback(err);
						}
						instance.destroy(function () {
							return callback(null, { status: 'success' });
						})
					});
				} else {
					instance.destroy(function () {
						return callback(null, { status: 'success' });
					})
				}
			});
		} else {
			var error = new Error('Validate.');
			error.status = 422;
			return callback(error);
		}
	};

	Template.getTemplate = function (data, callback) {
		Template.find({
			where: {
				themeId: data.themeId
			},
			include: [{
				relation: 'previewImage',
				scope: {
					fields: ['id', 'url'],
					where: {
						hidden: 0
					}
				}
			}, {
				relation: 'theme',
				scope: {
					fields: ['id', 'name']
				}
			}]
		}, function (err, instances) {
			if (err) {
				console.log(err);
				return callback(err);
			}
			return callback(null, instances);
		});
	};


	Template.remoteMethod('createTemplate', {
		http: {
			path: '/createTemplate',
			verb: 'post'
		},
		accepts: [{
			arg: 'data',
			type: 'object',
			http: {
				source: 'body'
			}
		}],
		returns: {
			type: 'object',
			root: true
		}
	});

	Template.remoteMethod('updateTemplate', {
		http: {
			path: '/updateTemplate',
			verb: 'post'
		},
		accepts: [{
			arg: 'data',
			type: 'object',
			http: {
				source: 'body'
			}
		}],
		returns: {
			type: 'object',
			root: true
		}
	});

	Template.remoteMethod('deleteTemplate', {
		http: {
			path: '/deleteTemplate',
			verb: 'post'
		},
		accepts: [{
			arg: 'data',
			type: 'object',
			http: {
				source: 'body'
			}
		}],
		returns: {
			type: 'object',
			root: true
		}
	});

	Template.remoteMethod('getTemplate', {
		http: {
			path: '/getTemplate',
			verb: 'post'
		},
		accepts: [{
			arg: 'data',
			type: 'object',
			http: {
				source: 'body'
			}
		}],
		returns: {
			type: 'object',
			root: true
		}
	});

};
