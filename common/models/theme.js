var checkAccessToken = require('../lib/checkAccessToken');

module.exports = function (Theme) {

	//special model, only admin can execute
	Theme.beforeRemote('**', function (ctx, unused, next) {
		console.log('Theme.beforeRemote');
		console.log(ctx.methodString);

		if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
			var Member = Theme.app.models.Member;
			Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
				if (err) {
					var errMsg = new Error('Database error.');
					errMsg.status = 500; // HTTP status code
					console.log(errMsg);
					next(errMsg);
				}
				if (memberObj && memberObj.username === 'admin') { //Admin -> allows all.
					next(); //OK
				} else {

					if (ctx.methodString === 'Theme.getTheme') { //post
						//Allow all
						next();
					} else {
						var errMsg = new Error('Not Authorized. You don\'t have a permission.');
						errMsg.status = 401; // HTTP status code
						console.log(errMsg);
						next(errMsg);
					}
				}
			});
		} else {
			var errMsg = new Error('Not Authorized. You don\'t have a permission.');
			errMsg.status = 401; // HTTP status code
			console.log(errMsg);
			next(errMsg);
		}
	});

	Theme.createTheme = function (data, callback) {
		if (data.name) { //Doesn't required previewImageId
			Theme.create(data, function (err, instance) {
				if (err) {
					console.log(err);
					return callback(err);
				}
				return callback(null, instance);
			});
		} else {
			var error = new Error('Validate.');
			error.status = 422;
			return callback(error);
		}
	};

	Theme.updateTheme = function (data, callback) {
		if (data.themeId && (data.name)) {
			data.update_at = new Date();
			Theme.findById(data.themeId, function (err, themeObj) {
				async.forEachOf(data, function (value, key, callbackforEachOf) {
					themeObj[key] = data[key] ? data[key] : themeObj[key];
					callbackforEachOf();
				}, function (err) { //end forEachOf
					if (err) {
						console.log(err);
						return callback(err);
					}
					themeObj.save();
					return callback(null, themeObj);
				});
			});
		} else {
			var error = new Error('Validate.');
			error.status = 422;
			return callback(error);
		}
	};

	Theme.deleteTheme = function (data, callback) {
		if (data.themeId) {
			Theme.findById(data.themeId, function (err, instance) {
				if (err) {
					console.log(err);
					return callback(err);
				}
				if (instance.id) {
					var Image = Theme.app.models.Image;
					Image.destroyAll({
						themeId: instance.id
					}, function (err, info) {
						if (err) {
							console.log(err);
							return callback(err);
						}
						instance.destroy(function () {
							return callback(null, { status: 'success' });
						});
					});
				} else {
					instance.destroy(function () {
						return callback(null, { status: 'success' });
					});
				}
			});
		} else {
			var error = new Error('Validate.');
			error.status = 422;
			return callback(error);
		}
	};

	Theme.getTheme = function (data, callback) {
		Theme.find({
			where: {},
			include: [{
				relation: 'previewImage',
				scope: {
					fields: ['id', 'url'],
					where: {
						hidden: 0
					}
				}
			}]
		}, function (err, instances) {
			if (err) {
				console.log(err);
				return callback(err);
			}
			return callback(null, instances);
		});
	};


	Theme.remoteMethod('createTheme', {
		http: {
			path: '/createTheme',
			verb: 'post'
		},
		accepts: [{
			arg: 'data',
			type: 'object',
			http: {
				source: 'body'
			}
		}],
		returns: {
			type: 'object',
			root: true
		}
	});

	Theme.remoteMethod('updateTheme', {
		http: {
			path: '/updateTheme',
			verb: 'post'
		},
		accepts: [{
			arg: 'data',
			type: 'object',
			http: {
				source: 'body'
			}
		}],
		returns: {
			type: 'object',
			root: true
		}
	});

	Theme.remoteMethod('deleteTheme', {
		http: {
			path: '/deleteTheme',
			verb: 'post'
		},
		accepts: [{
			arg: 'data',
			type: 'object',
			http: {
				source: 'body'
			}
		}],
		returns: {
			type: 'object',
			root: true
		}
	});

	Theme.remoteMethod('getTheme', {
		http: {
			path: '/getTheme',
			verb: 'post'
		},
		accepts: [{
			arg: 'data',
			type: 'object',
			http: {
				source: 'body'
			}
		}],
		returns: {
			type: 'object',
			root: true
		}
	});

};
