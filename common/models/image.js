var checkAccessToken = require('../lib/checkAccessToken');

module.exports = function (Image) {

	Image.beforeRemote('**', function (ctx, unused, next) {
		console.log('Image.beforeRemote');
		console.log(ctx.methodString);

		if (ctx.methodString === 'Image.createImage') { //post
			//Allow all
			next();
		} else if (ctx.methodString === 'Image.deleteImage') { //post
			//Allow all
			next();
		} else {
			if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
				var Member = Image.app.models.Member;
				Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
					if (err) {
						var errMsg = new Error('Database error.');
						errMsg.status = 500; // HTTP status code
						console.log(errMsg);
						next(errMsg);
					}
					if (memberObj.username === 'admin') { //Admin -> allows all.
						next(); //OK
					} else {
						var errMsg = new Error('Not Authorized. You don\'t have a permission.');
						errMsg.status = 401; // HTTP status code
						console.log(errMsg);
						next(errMsg);
					}
				})
			} else {
				var errMsg = new Error('Not Authorized. You don\'t have a permission.');
				errMsg.status = 401; // HTTP status code
				console.log(errMsg);
				next(errMsg);
			}
		}
	});

	// Image.createImage = function (data, callback) {
	// 	if (data.url && (data.themeId || data.templateId)) { //Doesn't required previewImageId
	// 		Image.create(data, function (err, returnImage) {
	// 			if (err) {
	// 				console.log(err);
	// 				return callback(err);
	// 			}
	// 			return callback(null, returnImage);
	// 		});
	// 	} else if (data.url && (data.pageId)) {
	// 		var Page = Image.app.models.Page;
	// 		Page.findById(data.pageId, {
	// 			include: [{
	// 				relation: 'post',
	// 				scope: {
	// 					fields: ['id', 'defaultImageUrl']
	// 				}
	// 			}]
	// 		}, function (err, returnPage) {
	// 			if (err) {
	// 				console.log(err);
	// 				return callback(err);
	// 			}
	// 			data.postId = returnPage.post().id
	// 			Image.create(data, function (err, returnImage) {
	// 				if (err) {
	// 					console.log(err);
	// 					return callback(err);
	// 				}
	// 				if (!returnPage.post().defaultImageUrl) {
	// 					returnPage.post().defaultImageUrl = data.url;
	// 					returnPage.post().save(function () {
	// 						return callback(null, returnImage);
	// 					});
	// 				} else {
	// 					return callback(null, returnImage);
	// 				}
	// 			});
	// 		});
	// 	} else {
	// 		var error = new Error('Validate.');
	// 		error.status = 422;
	// 		return callback(error);
	// 	}
	// };

	// Image.deleteImage = function (data, callback) {
	// 	//TODO: delete image's file
	// 	if (data.imageId) {
	// 		Image.findById(data.imageId, {
	// 			include: [{
	// 				relation: 'post',
	// 				scope: {
	// 					fields: ['id', 'defaultImageUrl'],
	// 					relation: 'image',
	// 					scope: {
	// 						fields: ['id', 'url'],
	// 						limit: 2,
	// 						order: "create_at ASC"
	// 					}
	// 				}
	// 			}]
	// 		}, function (err, returnImage) {
	// 			if (err) {
	// 				console.log(err);
	// 				return callback(err);
	// 			}
	// 			if (returnImage.post() && returnImage.post().defaultImageUrl === returnImage.url) {
	// 				returnImage.post().defaultImageUrl = returnImage.post().image()[1].url;
	// 				returnImage.post().save(function () {
	// 					returnImage.destroy(function () {
	// 						return callback(null, returnImage);
	// 					});
	// 				});
	// 			} else {
	// 				returnImage.destroy(function () {
	// 					return callback(null, returnImage);
	// 				});
	// 			}
	// 		});
	// 	} else {
	// 		var error = new Error('Validate.');
	// 		error.status = 422;
	// 		return callback(error);
	// 	}
	// };


	Image.remoteMethod('createImage', {
		http: {
			path: '/createImage',
			verb: 'post'
		},
		accepts: [{
			arg: 'data',
			type: 'object',
			http: {
				source: 'body'
			}
		}],
		returns: {
			type: 'object',
			root: true
		}
	});

	Image.remoteMethod('deleteImage', {
		http: {
			path: '/deleteImage',
			verb: 'post'
		},
		accepts: [{
			arg: 'data',
			type: 'object',
			http: {
				source: 'body'
			}
		}],
		returns: {
			type: 'object',
			root: true
		}
	});

};