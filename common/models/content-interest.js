var checkAccessToken = require('../lib/checkAccessToken');
var async = require('async');

module.exports = function (ContentInterest) {

  ContentInterest.beforeRemote('**', function (ctx, unused, next) {
    console.log('ContentInterest.beforeRemote');
    console.log(ctx.methodString);

    if (ctx.methodString === 'ContentInterest.createContentInterest') {
      if (ctx.req.body.postId) {
        checkAccessToken(ContentInterest.app.models.Post, ctx.req.body.postId, ctx.req.accessToken, function (err) {
          return next(err);
        });
      } else if (ctx.req.body.groupId) {
        checkAccessToken(ContentInterest.app.models.Group, ctx.req.body.groupId, ctx.req.accessToken, function (err) {
          return next(err);
        });
      } else {
        var error = new Error('Validate.');
        error.status = 422;
        return next(error);
      }
    } else if (ctx.methodString === 'ContentInterest.deleteContentInterest') { //post
      if (ctx.req.body.postId) {
        checkAccessToken(ContentInterest.app.models.Post, ctx.req.body.postId, ctx.req.accessToken, function (err) {
          return next(err);
        });
      } else if (ctx.req.body.groupId) {
        checkAccessToken(ContentInterest.app.models.Group, ctx.req.body.groupId, ctx.req.accessToken, function (err) {
          return next(err);
        });
      } else {
        var error = new Error('Validate.');
        error.status = 422;
        return next(error);
      }
    } else {
      if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
        var Member = ContentInterest.app.models.Member;
        Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
          if (err) {
            var errMsg = new Error('Database error.');
            errMsg.status = 500; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
          if (memberObj.username === 'admin') { //Admin -> allows all.
            next(); //OK
          } else {
            var errMsg = new Error('Not Authorized. You don\'t have a permission.');
            errMsg.status = 401; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
        })
      } else {
        var errMsg = new Error('Not Authorized. You don\'t have a permission.');
        errMsg.status = 401; // HTTP status code
        console.log(errMsg);
        next(errMsg);
      }
    }
  });

  ContentInterest.createContentInterest = function (data, callback) {
    if (data.interestId && (data.postId || data.groupId)) {
      ContentInterest.create({
        interestId: data.interestId,
        postId: data.postId,
        groupId: data.groupId
      }, function (err, instance) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        return callback(null, { status: 'success' });
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  ContentInterest.deleteContentInterest = function (data, callback) {
    if (data.interestId && (data.postId || data.groupId)) {
      ContentInterest.findOne({ where: { interestId: data.interestId, postId: data.postId, groupId: data.groupId } }, function (err, instance) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        instance.destroy();
        return callback(null, { status: 'success' });
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  ContentInterest.remoteMethod('createContentInterest', {
    http: {
      path: '/createContentInterest',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  ContentInterest.remoteMethod('deleteContentInterest', {
    http: {
      path: '/deleteContentInterest',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

};