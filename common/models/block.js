var checkAccessToken = require('../lib/checkAccessToken');
var redisHelper = require('./../lib/redishelper');
var client = redisHelper.client;
var async = require('async');
var crypto = require('crypto');
var bsonUrlEncoding = require('./../lib/bsonUrlEncoding');

module.exports = function (Block) {

  Block.beforeRemote('**', function (ctx, unused, next) {
    console.log('Block.beforeRemote');
    console.log(ctx.methodString);

    if (ctx.methodString === 'Block.block') { //post
      checkAccessToken(Block.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
        next(err);
      });
    } else if (ctx.methodString === 'Block.unblock') { //post
      checkAccessToken(Block.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
        next(err);
      });
    } else if (ctx.methodString === 'Block.getBlockingList') {
      checkAccessToken(Block.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
        next(err);
      });
    } else {
      if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
        var Member = Block.app.models.Member;
        Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
          if (err) {
            var errMsg = new Error('Database error.');
            errMsg.status = 500; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
          if (memberObj.username === 'admin') { //Admin -> allows all.
            next(); //OK
          } else {
            var errMsg = new Error('Not Authorized. You don\'t have a permission.');
            errMsg.status = 401; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
        })
      } else {
        var errMsg = new Error('Not Authorized. You don\'t have a permission.');
        errMsg.status = 401; // HTTP status code
        console.log(errMsg);
        next(errMsg);
      }
    }
  });

  // Block.block = function (data, callback) {
  //   //TODO: delete post of unfollowed member from Feed
  //   if (data.memberId && data.blockedMemberId) {
  //     Block.create({
  //       memberId: data.memberId,
  //       blockedMemberId: data.blockedMemberId
  //     }, function (err, info) {
  //       if (err) {
  //         console.log(err);
  //         return callback(err);
  //       }
  //       callback(null, info);
  //     });
  //   } else {
  //     var error = new Error('Validate.');
  //     error.status = 422;
  //     return callback(error);
  //   }
  // };

  // Block.unblock = function (data, callback) {
  //   if (data.memberId && data.blockedMemberId) {
  //     Block.findById(data.blockingMemberId, function (err, instance) {
  //       if (err) {
  //         console.log(err);
  //         return callback(err);
  //       } else if (instance.memberId && instance.blockingMemberId) {
  //         var error = new Error('Validate.');
  //         error.status = 422;
  //         return callback(error);
  //       } else {
  //         Block.destroyAll({
  //           where: {
  //             blockerMemberId: instance.memberId,
  //             memberId: instance.blockingMemberId,
  //           }
  //         }, function (err, info) {
  //           if (err) {
  //             console.log(err);
  //             return callback(err);
  //           }
  //           instance.destroy(function () {
  //             return callback(null, { status: 'success' });
  //           })
  //         });
  //       }
  //     });
  //   } else {
  //     var error = new Error('Validate.');
  //     error.status = 422;
  //     return callback(error);
  //   }
  // };


  Block.block = function (data, callback) {
    if (data.memberId && data.blockingMemberId) {
      //TODO: make it parellel
      //TODO: add post of new following to Feed
      var hash = crypto.createHash('sha256').update('blocking' + data.memberId + data.blockingMemberId).digest('hex'); //following + memberId + followerMemberId
      //create following
      Block.upsert({
        id: hash,
        memberId: data.memberId,
        blockingMemberId: data.blockingMemberId
      }, function (err, instance) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        hash = crypto.createHash('sha256').update('blocker' + data.blockingMemberId + data.memberId).digest('hex'); //follower + memberId + followerMemberId
        //create blocker
        Block.upsert({
          id: hash,
          memberId: data.blockingMemberId,
          blockerMember: data.memberId
        }, function (err, instance) {
          if (err) {
            console.log(err);
            return callback(err);
          }
          return callback(null, { status: 'success' });
        });
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  Block.unblock = function (data, callback) {
    //TODO: delete post of unfollowed member from Feed
    if (data.memberId && data.unBlockingMemberId) {
      var hash = crypto.createHash('sha256').update('blocking' + data.memberId + data.unBlockingMemberId).digest('hex'); //blocking + memberId + followerMemberId
      Block.destroyById(hash, function (err) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        hash = crypto.createHash('sha256').update('blocker' + data.unBlockingMemberId + data.memberId).digest('hex'); //blocker + memberId + followerMemberId
        Block.destroyById(hash, function (err) {
          if (err) {
            console.log(err);
            return callback(err);
          }
          return callback(null, { status: 'success' });
        });
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  Block.getBlockingList = function (data, callback) {
    if (!data.page && typeof data.page === 'undefined') {
      data.page = 1;
    }
    if (data.memberId) {
      var query = {
        hidden: 0
      };
      if (data.next) {
        var next = bsonUrlEncoding.decode(data.next);
        query = {
          id: {
            lt: next.toString()
          },
          hidden: 0
        };
      }
      var Member = Block.app.models.Member;
      Member.findById(data.memberId, {
        include: [{
          relation: 'blocking',
          scope: {
            where: query,
            limit: 10,
            order: "id DESC"
          }
        }]
      }, function (err, returnMember) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        if (returnMember) {
          var results = returnMember.blocking()
          return callback(null, {
            results: results,
            next: results[results.length - 1] && results.length == 10 ? bsonUrlEncoding.encode(results[results.length - 1].id) : undefined
          });
        } else {
          return callback(null, {
            status: 'error',
            message: 'Member not found.',
          });
        }

      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };


  Block.remoteMethod('block', {
    http: {
      path: '/block',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Block.remoteMethod('unblock', {
    http: {
      path: '/unblock',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });


  Block.remoteMethod('getBlockingList', {
    http: {
      path: '/getBlockingList',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

};
