var checkAccessToken = require('../lib/checkAccessToken');
var async = require('async');
var moment = require('moment');
var bsonUrlEncoding = require('./../lib/bsonUrlEncoding');

module.exports = function (Chat) {

  Chat.beforeRemote('**', function (ctx, unused, next) {
    if (ctx.methodString === 'Chat.list') {
      checkAccessToken(Chat.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
        next(err);
      })
    } else if (ctx.methodString === 'Chat.message') {
      checkAccessToken(Chat, ctx.req.body.chatId, ctx.req.accessToken, function (err) {
        next(err);
      })
    } else if (ctx.methodString === 'Chat.delete') {
      checkAccessToken(Chat, ctx.req.body.chatId, ctx.req.accessToken, function (err) {
        next(err);
      })
    } else if (ctx.methodString === 'Chat.createChat') {
      checkAccessToken(Chat.app.models.Member, ctx.req.body.fromMemberId, ctx.req.accessToken, function (err) {
        next(err);
      })
    } else {
      if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
        var Member = Chat.app.models.Member;
        Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
          if (err) {
            var errMsg = new Error('Database error.');
            errMsg.status = 500; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
          if (memberObj.username === 'admin') { //Admin -> allows all.
            next(); //OK
          } else {
            var errMsg = new Error('Not Authorized. You don\'t have a permission.');
            errMsg.status = 401; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
        })
      } else {
        var errMsg = new Error('Not Authorized. You don\'t have a permission.');
        errMsg.status = 401; // HTTP status code
        console.log(errMsg);
        next(errMsg);
      }
    }
  });


  Chat.message = function (data, cb) {
    //FIXME: memberId doesn't need.
    var query = {};
    if (data.last_timestamp && typeof data.last_timestamp !== 'undefined') {
      if ((!data.page && typeof data.page === 'undefined') || data.page === '') {
        data.page = 1;
      }
      query = {
        include: {
          relation: 'message',
          scope: {
            where: {
              create_at: {
                lt: moment(last_timestamp).toDate()
              }
            },
            limit: 20,
            skip: ((parseInt(data.page) - 1) * 20),
            order: "create_at DESC"
          }
        }
      }
    } else {
      if ((!data.page && typeof data.page === 'undefined') || data.page === '') {
        data.page = 1;
      }

      query = {
        include: {
          relation: 'message',
          scope: {
            where: {},
            limit: 20,
            skip: ((parseInt(data.page) - 1) * 20),
            order: "create_at DESC",
            include: 'contract'
          }
        }
      };

      if (data.next) {
        var next = bsonUrlEncoding.decode(data.next);
        query.include.scope.where.id = { lt: next.toString() };
      }
    }
    var Message = Chat.app.models.Message;
    if (data.chatId && data.memberId) {
      Chat.findById(data.chatId, query, function (err, chat) {
        if (err) {
          console.log(err);
          return cb(err);
        }
        var result = [];

        async.forEach(chat.message(), function (message, callback) {
            if (message.status === 'unread' && message.toJSON().owner.toString() !== data.memberId) {
              Message.upsert({
                id: message.id,
                status: 'read',
                message: message.message,
                chatId: message.chatId,
                owner: message.owner
              }, function (err, msg) {
                if (err) {
                  console.log(err);
                  return cb(err);
                }
                result.push(msg);
                callback();
              });
            } else {
              result.push(message);
              callback();
            }
          },
          function (err) {
            if (err) {
              return cb(err);
            }
            return cb(null, {
              status: "success",
              message: "",
              data: result,
              next: result[result.length -1] && result.length === 10 ? bsonUrlEncoding.encode(result[result.length - 1].id): undefined
            });
          });
      });
    } else {
      var error = new Error('Please enter correctly chatId.');
      error.status = 422;
      //      return cb(error);
      return cb(null, {
        status: "fail",
        message: "กรุณาส่งข้อมูลให้ครบ",
        data: null,
        error: error
      });
    }
  }

  Chat.list = function (data, cb) {
    var Message = Chat.app.models.Message;
    var query = {};

    if (data.last_timestamp && typeof data.last_timestamp !== 'undefined') {
      if ((!data.page && typeof data.page === 'undefined') || data.page === '') {
        data.page = 1;
      }
      query = {
        where: {
          or: [{
            fromMemberId: data.memberId
          }, {
            toMemberId: data.memberId
          }],
          status: 'active',
          update_at: {
            lt: moment(data.last_timestamp).toDate()
          },
          id: {
              lt: next.toString()
          }
        },
        order: "update_at DESC",
        limit: 10,
        skip: ((parseInt(data.page) - 1) * 10)
      }
    } else {
      if ((!data.page && typeof data.page === 'undefined') || data.page === '') {
        data.page = 1;
      }

      query = {
        where: {
          or: [{
            fromMemberId: data.memberId
          }, {
            toMemberId: data.memberId
          }],
          status: 'active'
        },
        order: "update_at DESC",
        limit: 10,
        skip: ((parseInt(data.page) - 1) * 10)
      }
      if (data.next) {
        var next = bsonUrlEncoding.decode(data.next);
        query.where.id = {
            lt: next.toString()
        };
      }
    }

    query.include = [{
      relation: 'fromMember',
      scope: {
        fields: ['id', 'displayName']
      }
    }, {
      relation: 'toMember',
      scope: {
        fields: ['id', 'displayName']
      }
    }, {
      relation: 'message',
      scope: {
        limit: 1,
        order: 'create_at DESC'
      }
    }];

    // // Pagination by cursor
    // if (data.next) {
    //   var next = bsonUrlEncoding.decode(data.next);
    //   query.id = {
    //       lt: next.toString()
    //   };
    // }

    Chat.find(query, function (err, chats) {
      async.forEach(chats, function (chat, callback) {
        Message.count({
          chatId: chat.id,
          status: 'unread',
          owner: {
            neq: data.memberId
          }
        }, function (err, info) {
          chat.count = info;
          return callback();
        });
      }, function (err) {
        return cb(null, {
          status: "success",
          message: "",
          data: chats,
          next: chats[chats.length -1] && chats.length === 10 ? bsonUrlEncoding.encode(chats[chats.length - 1].id): undefined
        });
        //        return cb(null, chats);
      });
    });
  }

  Chat.createChat = function (data, cb) {
    if (data.fromMemberId && data.toMemberId) {
      Chat.find({
        where: {
          and: [{
            or: [{
              fromMemberId: data.fromMemberId
            }, {
              toMemberId: data.fromMemberId
            }]
          }, {
            or: [{
              fromMemberId: data.toMemberId
            }, {
              toMemberId: data.toMemberId
            }]
          }]
        },
        include: [{
          relation: 'fromMember',
          scope: {
            fields: ['id', 'displayName'],
            include: {
              relation: "profileImage",
              scope: {
                fields: ['id', 'url'],
                where: {
                  hidden: 0
                }
              }
            }
          }
        }, {
          relation: 'toMember',
          scope: {
            fields: ['id', 'displayName'],
            include: {
              relation: "profileImage",
              scope: {
                fields: ['id', 'url'],
                where: {
                  hidden: 0
                }
              }
            }
          }
        }]
      }, function (err, chat) {
        if (err) {
          console.log(err);
          return cb(err);
        }
        //        console.log(chat);
        if (chat.length === 0) {
          Chat.create({
            fromMemberId: data.fromMemberId,
            toMemberId: data.toMemberId,
            status: 'active'
          }, function (err, newchat) {
            if (err) {
              console.log(err);
              return cb(err);
            }
            Chat.findById(newchat.id, {
              include: [{
                relation: 'fromMember',
                scope: {
                  fields: ['id', 'displayName'],
                  include: {
                    relation: "profileImage",
                    scope: {
                      fields: ['id', 'url'],
                      where: {
                        hidden: 0
                      }
                    }
                  }
                }
              }, {
                relation: 'toMember',
                scope: {
                  fields: ['id', 'displayName'],
                  include: {
                    relation: "profileImage",
                    scope: {
                      fields: ['id', 'url'],
                      where: {
                        hidden: 0
                      }
                    }
                  }
                }
              }]
            }, function (err, myChat) {
              return cb(null, {
                status: "success",
                message: "สร้างแชทสำเร็จค่ะ",
                data: myChat
              });
              //              cb(null, myChat);
            });
          })
        } else {
          return cb(null, {
            status: "success",
            message: "",
            data: chat[0]
          });
          //          cb(null, chat[0]);
        }
      });
    } else {
      var error = new Error('Validate Error.');
      error.status = 422;
      //      return cb(error);
      return cb(null, {
        status: "fail",
        message: "กรุณากรอกข้อมูลให้ครบค่ะ",
        data: null,
        error: error
      });
    }
  }

  Chat.delete = function (data, cb) {
    if (!data.chatId) {
      var error = new Error('Please enter correctly chatId.');
      error.status = 422;
      return cb(error);
    };
    Chat.findById(data.chatId, function (err, myChat) {
      if (err) {
        console.log(err);
        return cb(err);
      }
      myChat.status = 'inactive';
      myChat.save(function (err, oldChat) {
        if (err) {
          console.log(err);
          return cb(err);
        }

        return cb(null, {
          status: "success",
          message: "ลบแชทสำเร็จค่ะ",
          data: oldChat
        });
      })
    })
  }


  Chat.remoteMethod('message', {
    http: {
      path: '/message',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Chat.remoteMethod('list', {
    http: {
      path: '/list',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });
  Chat.remoteMethod('createChat', {
    http: {
      path: '/createChat',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });
  Chat.remoteMethod('delete', {
    http: {
      path: '/delete',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });
};
