var checkAccessToken = require('../lib/checkAccessToken');
var redisHelper = require('./../lib/redishelper');
var client = redisHelper.client;
var crypto = require('crypto');

module.exports = function (Like) {

  Like.beforeRemote('**', function (ctx, unused, next) {
    console.log('Like.beforeRemote');
    console.log(ctx.methodString);

    if (ctx.methodString === 'Like.like') { //post
      checkAccessToken(Like.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
        next(err);
      });
    } else if (ctx.methodString === 'Like.unlike') { //post
      checkAccessToken(Like.app.models.Member, ctx.req.body.memberId, ctx.req.accessToken, function (err) {
        next(err);
      });
    } else {
      if (ctx.req.accessToken && ctx.req.accessToken.userId) { //Admin -> allows all.
        var Member = Like.app.models.Member;
        Member.findById(ctx.req.accessToken.userId.toString(), function (err, memberObj) {
          if (err) {
            var errMsg = new Error('Database error.');
            errMsg.status = 500; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
          if (memberObj.username === 'admin') { //Admin -> allows all.
            next(); //OK
          } else {
            var errMsg = new Error('Not Authorized. You don\'t have a permission.');
            errMsg.status = 401; // HTTP status code
            console.log(errMsg);
            next(errMsg);
          }
        })
      } else {
        var errMsg = new Error('Not Authorized. You don\'t have a permission.');
        errMsg.status = 401; // HTTP status code
        console.log(errMsg);
        next(errMsg);
      }
    }
  });


  Like.like = function (data, callback) {
    if (data.memberId && ((data.postId && data.point) || data.commentId || data.interestId)) {
      var hash;
      if (data.postId) {
        hash = crypto.createHash('sha256').update(data.memberId + data.postId).digest('hex'); //following + memberId + followerMemberId
      } else if (data.commentId) {
        hash = crypto.createHash('sha256').update(data.memberId + data.commentId).digest('hex'); //following + memberId + followerMemberId
      } else if (data.interestId) {
        hash = crypto.createHash('sha256').update(data.memberId + data.interestId).digest('hex'); //following + memberId + followerMemberId
      }
      Like.upsert({
        id: hash,
        memberId: data.memberId,
        postId: data.postId,
        commentId: data.commentId,
        interestId: data.interestId,
        point: data.point
      }, function (err, instance) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        if (data.postId) {
          data.point = parseInt(data.point);
          //sorted set, ZADD like:postId int_score memberId
          var args = ['like:' + data.postId, data.point, data.memberId];
          client.zadd(args, function (err, reply) {
            if (err) {
              console.log(err);
            }
            return callback(null, { status: 'success' });
          });
        }
        if (data.commentId) {
          //sorted set, ZADD like:postId score=1 memberId
          var args = ['like:' + data.commentId, 1, data.memberId];
          client.zadd(args, function (err, reply) {
            if (err) {
              console.log(err);
            }
            return callback(null, { status: 'success' });
          });
        }
        if (data.interestId) {
          //sorted set, ZADD like:postId score=1 memberId
          var args = ['like:' + data.interestId, 1, data.memberId];
          client.zadd(args, function (err, reply) {
            if (err) {
              console.log(err);
            }
            return callback(null, { status: 'success' });
          });
        }
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  Like.unlike = function (data, callback) {
    if (data.memberId && (data.postId || data.commentId || data.interestId)) {
      var hash;
      if (data.postId) {
        hash = crypto.createHash('sha256').update(data.memberId + data.postId).digest('hex'); //following + memberId + followerMemberId
      } else if (data.commentId) {
        hash = crypto.createHash('sha256').update(data.memberId + data.commentId).digest('hex'); //following + memberId + followerMemberId
      } else if (data.interestId) {
        hash = crypto.createHash('sha256').update(data.memberId + data.interestId).digest('hex'); //following + memberId + followerMemberId
      }
      Like.destroyById(hash, function (err) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        if (data.postId) {
          //sorted set
          var args = ['like:' + data.postId, data.memberId];
          client.zrem(args, function (err) {
            if (err) {
              console.log(err);
            }
            return callback(null, { status: 'success' });
          });
        };
        if (data.commentId) {
          //sorted set
          var args = ['like:' + data.commentId, data.memberId];
          client.zrem(args, function (err) {
            if (err) {
              console.log(err);
            }
            return callback(null, { status: 'success' });
          });
        };
        if (data.interestId) {
          //sorted set
          var args = ['like:' + data.interestId, data.memberId];
          client.zrem(args, function (err) {
            if (err) {
              console.log(err);
            }
            return callback(null, { status: 'success' });
          });
        };
      });
    } else {
      var error = new Error('Validate.');
      error.status = 422;
      return callback(error);
    }
  };

  // Like.countLike = function (data, callback) {
  //   if (data.postId || data.commentId || data.interestId) {
  //     var aught = data.postId || data.commentId || data.interestId;
  //     client.get('like:' + aught.toString(), function (err, reply) {
  //       if (err) {
  //         console.log(err);
  //       }
  //       if (!reply) {
  //         return callback(null, reply);
  //       }
  //       else {
  //         return callback(null, '0');
  //       }
  //     });
  //   } else {
  //     var error = new Error('Validate.');
  //     error.status = 422;
  //     return callback(error);
  //   }
  // };

  Like.remoteMethod('like', {
    http: {
      path: '/like',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  Like.remoteMethod('unlike', {
    http: {
      path: '/unlike',
      verb: 'post'
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      type: 'object',
      root: true
    }
  });

  // Like.remoteMethod('countLike', {
  //   http: {
  //     path: '/countLike',
  //     verb: 'post'
  //   },
  //   accepts: [{
  //     arg: 'data',
  //     type: 'object',
  //     http: {
  //       source: 'body'
  //     }
  //   }],
  //   returns: {
  //     type: 'object',
  //     root: true
  //   }
  // });


};