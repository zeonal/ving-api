module.exports = function (Model, id, accessToken, callback) {
  // var accessToken = req.query.access_token;
  console.log(id, accessToken);
  if (!accessToken || !accessToken.userId || !id) {
    var errMsg = new Error('Not Authorized. No Access Token.');
    errMsg.status = 401; // HTTP status code
    console.log(errMsg);
    return callback(errMsg);
  }
  var token = accessToken;
  if (token) {
    var Member = Model.modelName.toString() === 'Member' ? Model : Model.app.models.Member;
    Member.findOne({
      where: {
        username: "admin"
      }
    }, function (err, admin) {
      if (err) {
        var errMsg = new Error('Database error.');
        errMsg.status = 500; // HTTP status code
        console.log(errMsg);
        return callback(errMsg);
      }
      if (token.userId.toString() === admin.id.toString()) { //Admin -> allows all.
        return callback(null); //OK
      } else { //Other Models. Use memberId
        Model.findById(id, function (err, obj) {
          if (err) {
            console.log(err);
            return callback(err);
          } else if (!obj) {
            var errMsg = new Error('Not Authorized. You don\'t have a permission.');
            errMsg.status = 401; // HTTP status code
            console.log(errMsg);
            return callback(errMsg);
          } else if (Model.modelName.toString() === 'Member' && token.userId.toString() === obj.id.toString()) { //Member Models. Use "id".
            return callback(null); //OK
          } else if (obj.memberId && token.userId.toString() === obj.memberId.toString()) { //Other Models. Use "memberId".
            return callback(null); //OK
          } else if ((obj.fromMemberId && token.userId.toString() === obj.fromMemberId.toString()) ||
            (obj.toMemberId && token.userId.toString() === obj.toMemberId.toString())
          ) {
            return callback(null); //OK
          } else {
            var errMsg = new Error('Not Authorized. You don\'t have a permission.');
            errMsg.status = 401; // HTTP status code.
            console.log(errMsg);
            return callback(errMsg);
          }
        })
      }
    });
  } else {
    var errMsg = new Error('Not Authorized. You don\'t have a permission.');
    errMsg.status = 401; // HTTP status code
    console.log(errMsg);
    return callback(errMsg);
  }
}