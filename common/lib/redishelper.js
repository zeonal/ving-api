var redis = require('redis'),
    client = redis.createClient();

// client.auth(process.env.REDIS_PASS);

var getStatsFromPrograms = function (programs, memberId, callback) {

  var keys = [];
  var multiFollowCommands = [];
  var multiLikeCommands = [];
  // console.log(programs);
  for (var i = 0; i < programs.length; i++) {
    if (programs[i] && programs[i].id) {
      var likeKey = 'like:' + programs[i].id.toString();
      var followKey = 'follow:' + programs[i].id.toString();
      var commentKey = 'comment:' + programs[i].id.toString();
      keys.push(likeKey, followKey, commentKey);
      multiFollowCommands.push(['sismember', 'member:follow:' + memberId, programs[i].id.toString()]);
      multiLikeCommands.push(['sismember', 'member:like:' + memberId, programs[i].id.toString()]);
    } else {
      console.log('Program is undefined.');
    }

  }

  if (programs && programs.length > 0 && keys.length  > 0) {

    client.mget(keys, function (err, replies) {
      if (err) {
        return callback(err);
      }
      // console.log(replies);

      for (var i = 0; i < replies.length - 2; i += 3) {
        // console.log(replies[i]);
        programs[i/3].likes = replies[i] ? parseInt(replies[i]): 0;
        // console.log(programs[i/3].likes);
        programs[i/3].follows = replies[i + 1] ? parseInt(replies[i + 1]): 0;
        programs[i/3].comments = replies[i + 2] ? parseInt(replies[i + 2]): 0;
      }

      client.multi(multiFollowCommands).exec(function (err, follows) {
        if (err) {
          console.log(err);
          return callback(err);
        }
        // console.log(follows);
        for (var i = 0; i < programs.length; i++) {
          if (follows[i] === 1) {
            programs[i].isFollowed = true;
          } else {
            programs[i].isFollowed = false;
          }
        }

        client.multi(multiLikeCommands).exec(function (err, likes) {
          if (err) {
            console.log(err);
            return callback(err);
          }
          // console.log(likes);
          for (var i = 0; i < programs.length; i++) {
            if (likes[i] === 1) {
              programs[i].isLiked = true;
            } else {
              programs[i].isLiked = false;
            }
          }
          // console.log(programs);
          return callback(null, programs);
        });

      });


    });
  } else {
    return callback(null, []);
  }

};

exports.getStatsFromPrograms = getStatsFromPrograms;
exports.client = client;
