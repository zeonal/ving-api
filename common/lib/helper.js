var moment = require('moment');

function validateContract(data) {
  var returnStatus = {
    status: true,
    message: '',
  };
  try {
    var period = parseInt(data.period);
  } catch (ex) {
    returnStatus.status = false;
    returnStatus.message = 'ระยะเวลาไม่ได้เป็นตัวเลข';
    return returnStatus;
  }

  try {
    var period = parseInt(data.percent);
  } catch (ex) {
    returnStatus.status = false;
    returnStatus.message = 'ส่วนแบ่งไม่ได้เป็นตัวเลข';
    return returnStatus;
  }

  try {
    var content = JSON.parse(data.content);
    if (content && content.length === 0) {
      returnStatus.status = false;
      returnStatus.message = 'ไม่ได้ระบุเนื้อหาเงื่อนไขในสัญญา';
      return returnStatus;
    } else if (!content) {
      returnStatus.status = false;
      returnStatus.message = 'ไม่ได้ระบุเนื้อหาเงื่อนไขในสัญญา';
      return returnStatus;
    }
  } catch (ex) {
    console.log(ex);
    returnStatus.status = false;
    returnStatus.message = 'Content format error';
    return returnStatus;
  }
  return returnStatus;
}


// Validate request body based on Model Name
function validate(modelName, data) {
  if (modelName === 'Contract') {
    return validateContract(data);
  }
  return {status: true};
}

exports.validate = validate;
