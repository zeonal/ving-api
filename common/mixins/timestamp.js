module.exports = function(Model) {
  Model.defineProperty('createdAt', {type: Date, default: '$now'});
  Model.defineProperty('modifiedAt', {type: Date, default: '$now'});

  Model.observe('before save', function (ctx, next) {
    if (ctx.instance) {
      ctx.instance['modifiedAt'] = new Date();
    } else {
      ctx.data['modifiedAt'] = new Date();
    }

    next();
  
  });

};