var routes = require('./../common/models/member');
var request = require('supertest');
var app = require('./../server/server');
var req = require('request');

var Member = app.models.Member;

var memberId, access_token;
var memberId2, access_token2;
var memberId2, access_token3;
var userId;



function getAccessTokenId(email, password, cb) {
  Member.login({email: email, password: password}, function (err, token) {
    if (err) {
      console.log(err);
      Member.create({email: email, password: password}, function (err, token) {
        if (err) {
          console.log(err);
          return cb(err);
        }
        console.log(token);
        return cb(err, token.id);
      });
    } else {
      userId = token.userId;
      memberId = token.userId;
      return cb(err, token.id);
    }

  });
}

// before(function (done) {
//   Member.find({
//     where: {
//       email: {inq: ['testcreate@unittest.com', 'admin2@unittest.com']}
//     }
//   }, function (err, testusers) {
//     if (err) {
//       console.log(err);
//       return done(err);
//     }

//     if (testusers) {
//       console.log(testusers);
//       for (var i = testusers.length - 1; i >= 0; i--) {
//         Member.destroyById(testusers[i].id);
//       }
//       return done();
//     } else {
//       return done();
//     };

//   });
// })

before(function(done){

  getAccessTokenId('admin2@unittest.com', '123456', function (err, token) {
    if (err) {
      console.log(err);
      return done(err);
    }
    access_token = token;
    // Remove only testcreate user
    Member.find({
      where: {
        email: {inq: ['testcreate@unittest.com', 'admin3@unittest.com', 'mai.peerapoom@gmail.com']}
      }
    }, function (err, testusers) {
      if (err) {
        console.log(err);
        return done(err);
      }

      if (testusers) {
        // console.log(testusers);
        for (var i = testusers.length - 1; i >= 0; i--) {
          Member.destroyById(testusers[i].id);
        }
        return done();
      } else {
        return done();
      };

    });
  });
});



describe('1. Members loginWithFacebookAccessToken', function () {
  describe('Post /api/Members/loginWithFacebookAccessToken', function () {
    it('should return 422', function (done) {
      request(app).post('/api/Members/loginWithFacebookAccessToken').send({
        accessToken: 'dospfjodps',
      }).expect(422, function (err, response) {
        if (response.body.error.message === 'Invalid FacebookAccessToken.') {
          done(null);
        }
      });
    });
  });
});

// Remove test for loginWithFacebookAccessToken as we cannot test it across multiple devs
// describe('2. Members loginWithFacebookAccessToken', function () {
//   describe('Post /api/Members/loginWithFacebookAccessToken', function () {
//     it('should return 200', function (done) {
//       request(app).post('/api/Members/loginWithFacebookAccessToken').send({
//         accessToken: 'EAAOLyDzSGOgBAGwibOl6asnhkpvAMU2FkMnHRuKUI52yMhnKfZCCEDg7flHHyT95ppjIeZBzJzDxKOpuwLTOdwsmzlzpRFCrqn2WPMGpJA0bZAQwvFjeBmRkCZCwMa4o1t1yWQZB6ElY6Tyxn8Ro3zZCX8qxJgZBvoSZCqblQyjARwZDZD',
//       }).expect(200, done);
//     });
//   });
// });


// var facebookId;
// describe('3. Members loginWithFacebookAccessToken', function () {
//   describe('Post /api/Members/loginWithFacebookAccessToken', function () {
//     it('should return 200', function (done) {
//       request(app).post('/api/Members/loginWithFacebookAccessToken').send({
//         accessToken: 'EAAOLyDzSGOgBAGwibOl6asnhkpvAMU2FkMnHRuKUI52yMhnKfZCCEDg7flHHyT95ppjIeZBzJzDxKOpuwLTOdwsmzlzpRFCrqn2WPMGpJA0bZAQwvFjeBmRkCZCwMa4o1t1yWQZB6ElY6Tyxn8Ro3zZCX8qxJgZBvoSZCqblQyjARwZDZD',
//       }).expect(200, function (err, response) {
//         if (response.body.isNewMember) {
//           facebookId = response.body.facebookId;
//           done(null);
//         }
//       });
//     });
//   });
// });

// describe('unlinkfacebook', function () {
//   describe('Post /api/Members/unlinkfacebook', function () {
//     it('should return 200', function (done) {
//       request(app).post('/api/Members/unlinkfacebook').send({
//         accessToken: 'EAAOLyDzSGOgBAFLIrq4rpxa1kLAT1l9dbLlZAfod0Twee0tKFbA2uCJkLddolagzxMYQALR659MNvZAxCwBCbat5T3ZAg5gFACQuh0RgbTTAHfWgarHiEICMRtd4lzlPZCTpYJVGv03exZCUqUtG8DbKeHTAYLyUJRj8cZAMteXAZDZD',
//       }).expect(200, function (err, response) {
//         if (response.body.isNewMember) {
//           done(null);
//         }
//       });
//     });
//   });
// });

//signup without displayName
describe('2. signup without displayName', function () {
  describe('POST /api/Members/signup', function () {
    it('should return 422 Unprocessable Entity', function (done) {
      request(app).post('/api/Members/signup').send({
        username: 'test',
        email: 'admin2@unittest.com',
        firstname: 'test',
        lastname: 'unittest',
        password: '123456'
      }).expect(422, done);
    })
  });
});

//signup without email
describe('3. signup without email', function () {
  describe('POST /api/Members/signup', function () {
    it('should return 422 Unprocessable Entity', function (done) {
      request(app).post('/api/Members/signup').send({
        displayName: 'unittest',
        username: 'test',
        firstname: 'test',
        lastname: 'unittest',
        password: '123456'
      }).expect(422, done);
    })
  });
});

//signup without password
describe('4. signup without password', function () {
  describe('POST /api/Members/signup', function () {
    it('should return 422 Unprocessable Entity', function (done) {
      request(app).post('/api/Members/signup').send({
        displayName: 'unittest',
        username: 'test',
        email: 'admin@unittest.com',
        firstname: 'test',
        lastname: 'unittest',
      }).expect(422, done);
    })
  });
});

//signup without displayName, email, password
describe('5. signup without displayName, email, password', function () {
  describe('POST /api/Members/signup', function () {
    it('should return 422 Unprocessable Entity', function (done) {
      request(app).post('/api/Members/signup').send({
        displayName: 'unittest',
        username: 'test',
        email: 'admin@unittest.com',
        firstname: 'test',
        lastname: 'unittest',
      }).expect(422, done);
    })
  });
});

//signup
describe('6. signup', function () {
  describe('POST /api/Members/signup', function () {
    it('should return 200', function (done) {
      request(app).post('/api/Members/signup').send({
        displayName: 'unittest',
        username: 'test',
        email: 'testcreate@unittest.com',
        firstname: 'test',
        lastname: 'unittest',
        password: '123456'
      }).expect(200, done);
    })
  });
});

//signup to used-email
describe('7. signup to used-email', function () {
  describe('POST /api/Members/signup', function () {
    it('should return 422', function (done) {
      request(app).post('/api/Members/signup').send({
        displayName: 'unittest',
        username: 'test',
        email: 'testcreate@unittest.com',
        firstname: 'test',
        lastname: 'unittest',
        password: '123456'
      }).expect(422, done);
    })
  });
});

//signup to used except email
describe('8. signup to used acceptable email', function () {
  describe('POST /api/Members/signup', function () {
    it('should return 200', function (done) {
      request(app).post('/api/Members/signup').send({
        displayName: 'unittest',
        username: 'test',
        email: 'admin3@unittest.com',
        firstname: 'test',
        lastname: 'unittest',
        password: '123456'
      }).expect(200, done);
    })
  });
});

//login invalid password
describe('9. login invalid password', function () {
  describe('POST /api/Members/login', function () {
    it('should return 401', function (done) {
      request(app).post('/api/Members/login').send({
        email: 'admin@unittest.com',
        password: '111111'
      }).expect(401, done);
    })
  });
});



//login
describe('10. login', function () {
  describe('POST /api/Members/login', function () {
    it('should return 200', function (done) {
      request(app).post('/api/Members/login').send({
        email: 'admin@unittest.com',
        password: '123456'
      }).expect(200, function (err, response) {
        // console.log(response.body);
        // memberId = response.body.token.userId; // or response.body.token.userId;
        // access_token = response.body.token.id
        done(null);
      });
    })
  });
});

//dup login
describe('11. login again', function () {
  describe('POST /api/Members/login', function () {
    it('should return 200', function (done) {
      request(app).post('/api/Members/login').send({
        email: 'admin@unittest.com',
        password: '123456'
      }).expect(200, function (err, response) {
        // memberId = response.body.token.userId; // or response.body.token.userId;
        // access_token = response.body.token.id
        done(null);
      });
    })
  });
});

//invalid access_token
describe('12. updateProfile invalid access_token', function () {
  describe('PUT /api/Members/updateProfile', function () {
    it('should return 401 Unauthorized', function (done) {
      request(app).put('/api/Members/updateProfile?access_token='+memberId).send({
        displayName: 'admin',
        username: 'admin',
        email: 'admin@vingnetwork.com',
        memberId: memberId,
        firstname: 'admin',
        lastname: 'vingggtet',
        password: 'vingpassword'
      }).expect(401, done);
    })
  });
});

//updateProfile
describe('13. updateProfile (plus password), expect: password not change', function () {
  describe('PUT /api/Members/updateProfile', function () {
    it('should return 200', function (done) {
      request(app).put('/api/Members/updateProfile?access_token='+access_token).send({
        displayName: 'admin',
        username: 'admin2test',
        email: 'admin2@unittest.com',
        memberId: memberId,
        firstname: 'admin',
        lastname: 'vingggtet',
        password: 'vingpassword'
      }).expect(200, done);
    })
  });
});

//should fail: can't update password on updateProfile
describe('14. login should fail: can\'t update password on updateProfile', function () {
  describe('POST /api/Members/login', function () {
    it('should return 401 because can\'t update password on updateProfile', function (done) {
      request(app).post('/api/Members/login').send({
        email: 'admin2@unittest.com',
        password: 'vingpassword'
      }).expect(401, function (err, response) {
        // memberId = response.body.token.userId; // or response.body.token.userId;
        // access_token = response.body.token.id
        done(null);
      });
    })
  });
});

//login
describe('15. login', function () {
  describe('POST /api/Members/login', function () {
    it('should return 200', function (done) {
      request(app).post('/api/Members/login').send({
        email: 'admin2@unittest.com',
        password: '123456'
      }).expect(200, function (err, response) {
        // memberId = response.body.token.userId; // or response.body.token.userId;
        // access_token = response.body.token.id
        done(null);
      });
    })
  });
});

//changePassword to oldpassword
describe('16. changePassword to oldpassword', function () {
  describe('POST /api/Members/changePassword', function () {
    it('should return 200', function (done) {
      request(app).post('/api/Members/changePassword?access_token='+access_token).send({
        oldpassword: '123456',
        newpassword: '123456',
        memberId: memberId
      }).expect(200, done);
    })
  });
});

// //invalid oldpassword
describe('17. changePassword invalid oldpassword', function () {
  describe('POST /api/Members/changePassword', function () {
    it('should return 500', function (done) {
      request(app).post('/api/Members/changePassword?access_token='+access_token).send({
        oldpassword: '111111',
        newpassword: 'vingpassword',
        memberId: memberId
      }).expect(500, done);
    })
  });
});

//change password
describe('18. change password should success', function () {
  describe('POST /api/Members/changePassword', function () {
    it('should return 200', function (done) {
      request(app).post('/api/Members/changePassword?access_token='+access_token).send({
        oldpassword: '123456',
        newpassword: 'vingpassword',
        memberId: memberId
      }).expect(200, done);
    })
  });
});

describe('19 change password to default', function () {
  describe('POST /api/Members/changePassword', function () {
    it('should return 200', function (done) {
      request(app).post('/api/Members/changePassword?access_token='+access_token).send({
        oldpassword: 'vingpassword',
        newpassword: '123456',
        memberId: memberId
      }).expect(200, done);
    })
  });
});

// //login invalid password
describe('20. login invalid password', function () {
  describe('POST /api/Members/login', function () {
    it('should return 401', function (done) {
      request(app).post('/api/Members/login').send({
        email: 'admin2@unittest.com',
        password: '123456asdfsaf'
      }).expect(401, function (err, response) {
        // memberId = response.body.token.userId; // or response.body.token.userId;
        // access_token = response.body.token.id
        done(null);
      });
    })
  });
});



// //updateProfile to same email
describe('21. updateProfile to same email', function () {
  describe('PUT /api/Members/updateProfile', function () {
    it('should return 200', function (done) {
      request(app).put('/api/Members/updateProfile?access_token='+access_token).send({
        email: 'admin2@unittest.com',
        memberId: memberId,
      }).expect(200, done);
    })
  });
});

// //updateProfile access_token invalid
describe('22. updateProfile access_token invalid', function () {
  describe('PUT /api/Members/updateProfile', function () {
    it('should return 401', function (done) {
      request(app).put('/api/Members/updateProfile?access_token='+access_token).send({
        email: 'admin2@vingnetwork.com',
        memberId: memberId2,
      }).expect(401, done);
    })
  });
});

//updateProfile memberId invalid
describe('23. updateProfile access_token invalid', function () {
  describe('PUT /api/Members/updateProfile', function () {
    it('should return 401', function (done) {
      request(app).put('/api/Members/updateProfile?access_token='+access_token2).send({
        email: 'admin2@vingnetwork.com',
        memberId: memberId,
      }).expect(401, done);
    })
  });
});

//updateProfile to used-email
describe('24. updateProfile to used-email', function () {
  describe('PUT /api/Members/updateProfile', function () {
    it('should return 422', function (done) {
      request(app).put('/api/Members/updateProfile?access_token='+access_token).send({
        email: 'testcreate@unittest.com',
        memberId: memberId,
      }).expect(422, done);
    })
  });
});

//updateProfile to empty token
describe('25. updateProfile (empty token)', function () {
  describe('PUT /api/Members/updateProfile', function () {
    it('should return 401', function (done) {
      request(app).put('/api/Members/updateProfile').send({
        email: 'admin@unittest.com',
        memberId: memberId,
      }).expect(401, done);
    })
  });
});


//signup
facebookId = '123456678';
describe('26. signup with facebookId', function () {
  describe('POST /api/Members/signup', function () {
    it('should return 200', function (done) {
      request(app).post('/api/Members/signup').send({
        displayName: 'mai',
        username: 'test',
        email: 'mai.peerapoom@gmail.com',
        firstname: 'test',
        lastname: 'unittest',
        password: '123456',
        facebookId: facebookId
      }).expect(200, done);
    })
  });
});

//login
describe('27. login', function () {
  describe('POST /api/Members/login', function () {
    it('should return 200', function (done) {
      request(app).post('/api/Members/login').send({
        email: 'mai.peerapoom@gmail.com',
        password: '123456'
      }).expect(200, function (err, response) {
        console.log(response.body);
        memberId = response.body.token.userId; // or response.body.token.userId;
        access_token = response.body.token.id
        done(null);
      });
    })
  });
});

//unlinkFacebookId null accessToken
describe('28. unlinkFacebookId null accessToken', function () {
  describe('PUT /api/Members/unlinkfacebook', function () {
    it('should return 401', function (done) {
      request(app).put('/api/Members/unlinkfacebook').send({
        memberId: memberId,
        access_token: null
      }).expect(401, done);
    })
  });
});

//unlinkFacebookId invalid accessToken
describe('29. unlinkFacebookId invalid accessToken', function () {
  describe('PUT /api/Members/unlinkfacebook', function () {
    it('should return 401', function (done) {
      request(app).put('/api/Members/unlinkfacebook').send({
        memberId: memberId,
        access_token: access_token2
      }).expect(401, done);
    })
  });
});

//unlinkFacebookId
describe('30. unlinkfacebook', function () {
  describe('PUT /api/Members/unlinkfacebook', function () {
    it('should return 200', function (done) {
      // console.log(access_token);
      request(app).put('/api/Members/unlinkfacebook').send({
        memberId: memberId,
        access_token: access_token
      }).expect(200, done);
    })
  });
});

//reset password
describe('31. reset password with non existing user', function () {
  describe('POST /api/Members/reset', function () {
    it('should return 404', function (done) {
      request(app).post('/api/Members/reset').send({
        email: 'testsf1s3d1fs3df1@test.com'
      }).expect(404, done);
    })
  });
});

//reset password
// describe('32. reset password', function () {
//   describe('POST /api/Members/reset', function () {
//     it('should return 204', function (done) {
//       this.timeout(5000);
//       request(app).post('/api/Members/reset').send({
//         email: 'admin@vingnetwork.com'
//       }).expect(204, function (err, response) {
//         setTimeout(function() {
//           done(null);
//       }, 4000);

//       });
//     })
//   });
// });
