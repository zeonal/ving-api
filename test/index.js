var request = require('supertest');
var app = require('./../server/server');

var memberId, access_token;

function getAccessTokenId(email, password, cb) {
  Member.login({email: email, password: password}, function (err, token) {
    if (err) {
      console.log(err);
      Member.create({email: email, password: password}, function (err, token) {
        if (err) {
          console.log(err);
          return cb(err);
        }
        console.log(token);
        return cb(err, token.id);
      });
    } else {
      access_token = token.id;
      memberId = token.userId;
      return cb(err, token.id);
    }

  });
}


before(function(done){

  getAccessTokenId('test@test.com', '123456', function (err, token) {
    if (err) {
      console.log(err);
      return done(err);
    }
    return done();
  });
});
