angular
  .module('ving-api')
  .factory('AuthService', ['Member', '$q', '$rootScope', function(User, $q,
      $rootScope) {
    function login(username, password) {
      return User
        .login({username: username, password: password})
        .$promise
        .then(function(response) {
          // console.log(response);
          $rootScope.currentUser = {
            id: response.id,
            tokenId: response.token.id,
            username: username,
          };
          // console.log($rootScope.currentUser);
        });
    }

    function logout() {
      return User
       .logout()
       .$promise
       .then(function() {
         $rootScope.currentUser = null;
       });
    }

    function register(email, password) {
      return User
        .create({
         email: email,
         password: password
       })
       .$promise;
    }

    return {
      login: login,
      logout: logout,
      register: register
    };
  }]);
