angular.module('ving-api')
.directive('sidebar', function() {
  return {
    templateUrl: '/views/sidebar.html'
  };
});
