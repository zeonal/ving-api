'use strict';


// Declare app level module which depends on filters, and services
angular.module('ving-api', [
  'ngRoute',
  'ngResource',
  'ngCookies',
  'ngSanitize',
  'lbServices',
  'angularFileUpload',
  'ui.bootstrap.datetimepicker',
  'ngTagsInput',
  'ui.calendar'

]).
config(['$routeProvider', '$locationProvider',
  function ($routeProvider, $locationProvider) {
    //$locationProvider.html5Mode(true);
    $routeProvider.when('/login', {
      templateUrl: 'views/login.html',
      controller: 'LoginController'
    });

    // Just for test
    $routeProvider.when('/signup', {
      templateUrl: 'views/signup.html',
      controller: 'SignupController'
    });
    $routeProvider.when('/upload', {
      templateUrl: 'views/upload.html',
      controller: 'UploadController'
    });
    $routeProvider.when('/resetpassword', {
      templateUrl: 'views/reset.html',
      controller: 'ResetController'
    });
    $routeProvider.when('/forgotpassword', {
      templateUrl: 'views/forgotpassword.html',
      controller: 'ForgotPasswordController'
    });

    $routeProvider.when('/user', {
      templateUrl: 'views/user.html',
      controller: 'UserController',
      authenticate: true
    });
    $routeProvider.when('/userdetail', {
      templateUrl: 'views/userdetail.html',
      controller: 'UserDetailController',
      authenticate: true
    });
    $routeProvider.when('/userdetail/:modelId', {
      templateUrl: 'views/userdetail.html',
      controller: 'UserDetailController',
      authenticate: true
    });
    $routeProvider.when('/comment', {
      templateUrl: 'views/comment.html',
      controller: 'CommentController',
      authenticate: true
    });
    $routeProvider.when('/commentdetail', {
      templateUrl: 'views/commentdetail.html',
      controller: 'CommentDetailController',
      authenticate: true
    });
    $routeProvider.when('/commentdetail/:modelId', {
      templateUrl: 'views/commentdetail.html',
      controller: 'CommentDetailController',
      authenticate: true
    });

    $routeProvider.when('/logout', {
      template: 'Logout',
      controller: 'LogoutController',
      // authenticate: true
    });

    $routeProvider.when('/testupload', {
      templateUrl: 'views/testupload.html',
      controller: 'TestUploadController',
      authenticate: true
    });

    $routeProvider.when('/like', {
      templateUrl: 'views/like.html',
      controller: 'LikeController',
      authenticate: true
    });
    $routeProvider.when('/tag', {
      templateUrl: 'views/tag.html',
      controller: 'TagController',
      authenticate: true
    });
    $routeProvider.when('/tagdetail', {
      templateUrl: 'views/tagdetail.html',
      controller: 'TagDetailController',
      authenticate: true
    });
    $routeProvider.when('/tagdetail/:modelId', {
      templateUrl: 'views/tagdetail.html',
      controller: 'TagDetailController',
      authenticate: true
    });

    $routeProvider.when('/follow', {
      templateUrl: 'views/follow.html',
      controller: 'FollowController',
      authenticate: true
    });

    $routeProvider.when('/home', {
      templateUrl: 'views/home.html',
      controller: 'HomeController',
      authenticate: true
    });


    $routeProvider.when('/theme', {
      templateUrl: 'views/theme.html',
      controller: 'ThemeController',
      authenticate: true
    });
    $routeProvider.when('/themedetail', {
      templateUrl: 'views/themedetail.html',
      controller: 'ThemeDetailController',
      authenticate: true
    });
    $routeProvider.when('/themedetail/:modelId', {
      templateUrl: 'views/themedetail.html',
      controller: 'ThemeDetailController',
      authenticate: true
    });

    $routeProvider.when('/template', {
      templateUrl: 'views/template.html',
      controller: 'TemplateController',
      authenticate: true
    });
    $routeProvider.when('/templatedetail', {
      templateUrl: 'views/templatedetail.html',
      controller: 'TemplateDetailController',
      authenticate: true
    });
    $routeProvider.when('/templatedetail/:modelId', {
      templateUrl: 'views/templatedetail.html',
      controller: 'TemplateDetailController',
      authenticate: true
    });

    $routeProvider.otherwise({
      redirectTo: '/login'
    });
  }
]).run(['$cookies', '$rootScope', '$location', '$log', '$timeout', '$window', '$http',
  function ($cookies, $rootScope, $location, $log, $timeout, $window, $http) {



    if ($cookies.getObject('currentUser') && $location.path() === '/login') {
      $location.path('/home');
    };

    // Prevent authenticated path for public user
    $rootScope.$on('$routeChangeStart', function (event, next) {
      // redirect to login page if not logged in
      if (next.authenticate && !$cookies.get('currentUser')) {
        event.preventDefault(); //prevent current page from loading
        $location.path('/login');
      } else {
        $rootScope.currentUser = $cookies.getObject('currentUser');
      };
    });

    //    $rootScope.$on('$routeChangeSuccess', function (event, next) {
    //      $timeout(function () {
    //        componentHandler.upgradeAllRegistered();
    //      }, 500);
    //
    //    });

    // Common Service
    $rootScope.save = function (model, returnPath, ModelService) {
      if (model.id !== undefined || typeof model.id !== 'undefined') {
        model
          .$save()
          .then(function (model) {
            $location.path('/' + returnPath);
          });
      } else {
        console.log(returnPath);
        // model.access_token = $rootScope.currentUser.tokenId;
        ModelService.create({access_token: $rootScope.currentUser.tokenId}, model)
          .$promise
          .then(function (model) {
            console.log($rootScope.currentComment = model);
            $location.path('/' + returnPath);
          });
      };
    };

    $rootScope.saveWithCallback = function (model, ModelService, callback) {
      if (model.id !== undefined || typeof model.id !== 'undefined') {
        console.log(model.id, typeof model.id);
        model
          .$save()
          .then(function (model) {
            callback(model);
          });
      } else {
        ModelService.create(model)
          .$promise
          .then(function (model) {
            console.log($rootScope.currentComment = model);

            callback(model);
          });
      };
    };

    $rootScope.delete = function (model, models, ModelService) {
      if (confirm('Are you sure?')) {
        for (var i = models.length - 1; i >= 0; i--) {
          console.log(models[i]);
          if (models[i].id.toString() === model.id) {
            models.splice(i, 1);
          };
        };
        ModelService
          .deleteById({
            id: model.id
          })
          .$promise
          .then(function () {
            console.log('deleted');
          });
      };
    };

    $rootScope.updateRedisProgramTag = function (programId, newTags, oldTags) {
      console.log(newTags); //need .txt
      console.log(oldTags); //don't need .text
      //$scope.myTag is a blinding object, it's object in object, so we need to do this

      for (var i = 0; i < newTags.length; i++) {
        if (oldTags.indexOf(newTags[i]) === -1) {
          var req = {
            method: 'POST',
            url: '/addProgramTag',
            data: {
              tagName: newTags[i],
              programId: programId
            }
          }
          $http(req)
        }
      } //end for
      for (var i = oldTags.length - 1; i >= 0; i--) {
        if (newTags.indexOf(oldTags[i]) === -1) {
          var req = {
            method: 'POST',
            url: '/deleteProgramTag',
            data: {
              tagName: oldTags[i],
              programId: programId
            }
          }
          $http(req)
        }
      } //end for
    };
  }
]);
