angular
  .module('ving-api')
  .controller('LoginController', ['$cookies', '$location', '$rootScope', '$scope', 'AuthService',
    function($cookies, $location, $rootScope, $scope, AuthService) {
      $rootScope.title = 'Login';
      $scope.user = {};

      if ($cookies.getObject('currentUser') && $location.path() === '/login') {
        $location.path('/home');
      };

      $scope.login = function(event) {
        event.preventDefault(); //prevent current page from loading

        if ($scope.user.username !== 'admin' && $scope.user.username !== 'graphic' && $scope.user.username !== 'test' && $scope.user.username !== 'ving') {
          return;
        } else {
          AuthService.login($scope.user.username, $scope.user.password)
            .then(function() {
              // console.log($rootScope.currentUser);
              // console.log($cookies.getObject('currentUser'));
              $cookies.putObject('currentUser', $rootScope.currentUser);
              $location.path('/home');
            });
        };

      };
    }
  ])
  .controller('ResetController', ['$cookies', '$location', '$rootScope', '$scope', 'Member', 'SystemLog',
    function($cookies, $location, $rootScope, $scope, User, SystemLog) {
      $rootScope.hideAll = true;
      $scope.user = {};

      function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
          vars[key] = value;
        });
        return vars;
      }

      var params = getUrlVars();
      console.log(params.access_token);
      //$scope.user.username = params.user;

      User.findById({
        id: params.user
      }, function(response) {
        console.log(response);
        $scope.user = response;
      }, function(err) {
        if (err) {
          console.log(err);
          SystemLog.create({
            time: Date.now(),
            detail: err,
            type: "vingApi",
            level: 1
          })
        };
      });

      $scope.resetPassword = function() {
        if ($scope.user.password === $scope.user.password2) {
          delete $scope.user.password2
          User.prototype$updateAttributes({
            id: $scope.user.id
          }, {
            password: $scope.user.password
          }, function(response) {
            console.log(response);
            console.log('success');
            $scope.isSuccess = true;
          });
        };

      };

    }
  ])
  .controller('ForgotPasswordController', ['$location', '$rootScope', '$routeParams', '$scope', 'Member', function($location, $rootScope, $routeParams, $scope, Model) {
    componentHandler.upgradeAllRegistered();
    $scope.model = {};
    $scope.forgotPassword = function() {
      console.log($scope.model);
      Model.resetPassword($scope.model)
        .$promise
        .then(function(model) {
          alert("Success! Please check email");
          $location.path('/login');
        });
    };

  }])

.controller('LogoutController', ['$cookies', '$location', '$rootScope', '$scope', 'AuthService',
    function($cookies, $location, $rootScope, $scope, AuthService) {
      $rootScope.title = 'Logout';
      AuthService.logout()
        .then(function() {
          $cookies.remove('currentUser');
          delete $rootScope.currentUser;
          $location.path('/login');
        });
    }
  ])
  .controller('HomeController', ['$rootScope', '$scope', 'Member', function($rootScope, $scope, Member) {
    $rootScope.title = 'Home';
    $scope.members = Member.find({
      filter: {
        where: {
          role: {
            neq: undefined
          }
        }
      }
    });
  }])


.controller('UserController', ['$rootScope', '$scope', 'Member', function($rootScope, $scope, Model) {
    console.log($rootScope.currentUser);
    $rootScope.title = 'User';
    $scope.models = Model.find({
      filter: {
        include: 'profileImage'
      }
    });
    console.log($scope.models);
    $scope.delete = function(model) {
      // Use common service
      $rootScope.delete(model, $scope.models, Model);
    };

  }])
  .controller('UserDetailController', ['$location', '$rootScope', '$routeParams', '$scope', 'Member', 'Group', function($location, $rootScope, $routeParams, $scope, Model, Group) {

    $scope.model = {};
    if ($routeParams.modelId) {
      $scope.model = Model.findById({
        id: $routeParams.modelId
      });
      $scope.member = $scope.model;
    }

    $scope.update = function() {
      var data = {
        username: $scope.model.displayName,
        displayName: $scope.model.displayName,
        memberId: $scope.model.id,
        firstname: $scope.model.firstname,
        lastname: $scope.model.lastname,
        country: $scope.model.country,
        group: $scope.model.group,
        role: $scope.model.role,
        access_token: $rootScope.currentUser.tokenId
      };
      Model.updateProfile(data, function(member) {
        $location.path('/user');
      });
    }

    var mypackages = [];
    var mystroage = [];
    $scope.save = function() {
      $scope.model.username = $scope.model.displayName;
      $rootScope.save($scope.model, 'member', Model);
    }


    $scope.back = function() {
      $location.path('/user');
    }
  }])
  .controller('CommentController', ['$q', '$rootScope', '$scope', 'Comment', 'Channel', 'ChannelComment', 'Program', function($q, $rootScope, $scope, Model, Channel, ChannelComment, Program) {
    console.log($rootScope.currentUser);
    $rootScope.title = 'Comment';
    //        $scope.models = Model.find();

    $scope.channels = Channel.find();
    $scope.selectChannel = function() {
      console.log($scope.model.channel.id);
      Program.find({
          "filter": {
            "where": {
              "channelId": $scope.model.channel.id,
            }
          }
        }).$promise
        .then(function(program) {
          $scope.models = [];
          $scope.programs = program;
          console.log(program);
        })
      $scope.selectProgram = function() {
        console.log($scope.model.program.id);
        Model.find({
            "filter": {
              "where": {
                "programId": $scope.model.program.id,
              },
              include: ['program', 'member']
            }
          }).$promise
          .then(function(comment) {
            $scope.models = [];
            for (var i = 0; i < comment.length; i++) {
              var currentComment = comment[i];
              console.log(currentComment);
              if (!currentComment.parent) {
                $scope.models.push(currentComment);
              }
            }
            for (var i = 0; i < $scope.models.length; i++) {
              var checkComment = $scope.models[i];
              console.log(checkComment);
              for (var j = 0; j < comment.length; j++) {
                var currentComment = comment[j];
                console.log(currentComment);
                if (currentComment.parent === checkComment.id) {
                  if (checkComment.replies && checkComment.replies.length > 0) {
                    checkComment.replies.push(currentComment);
                  } else {
                    //checkComment.replies = [currentComment];
                    checkComment.replies = [];
                    checkComment.replies.push(currentComment);
                  }
                  //checkComment.replies = (checkComment.replies && checkComment.replies.length > 0) ? checkComment.push(currentComment) : checkComment.replies[currentComment];
                }
              }
            }

          })
      }
    }
    $scope.delete = function(model) {
      // Use common service
      $rootScope.delete(model, $scope.models, Model);
    };


  }])
  .controller('CommentDetailController', ['$q', '$location', '$rootScope', '$routeParams', '$scope', 'Comment', 'Channel', 'ChannelComment', 'Program', function($q, $location, $rootScope, $routeParams, $scope, Model, Channel, ChannelComment, Program) {
    //    componentHandler.upgradeAllRegistered();

    $scope.model = {};
    if ($routeParams.modelId) {
      $scope.model = Model.findById({
        id: $routeParams.modelId
      });
    };
    $scope.channels = Channel.find();
    $scope.selectChannel = function() {
      Program.find({
          "filter": {
            "where": {
              "channelId": $scope.channel.id,
            }
          }
        }).$promise
        .then(function(program) {
          $scope.programs = program;
          console.log(program);
        });
    }
    $scope.selectProgram = function() {
      Model.find({
          "filter": {
            "where": {
              "programId": $scope.model.programId,
            }
          }
        }).$promise
        .then(function(comment) {
          console.log(comment);
          if (!comment.parent) {
            $scope.parents = comment;
          }
        })
    }


    $scope.save = function() {
      // Use common service

      $scope.model.create_at = new Date();
      $scope.model.memberId = $rootScope.currentUser.id;
      console.log($scope.model.memberId);
      $rootScope.save($scope.model, 'comment', Model);
    }
  }])
  .controller('LikeController', ['$rootScope', '$scope', 'Like', function($rootScope, $scope, Model) {
    console.log($rootScope.currentUser);
    $rootScope.title = 'Like';
    Model.find({
      "filter": {
        "include": ['program', 'member']
      }
    }, function(model) {
      console.log(model);
      $scope.models = model;
    });

    $scope.delete = function(model) {
      // Use common service
      console.log(model);
      $rootScope.delete(model, $scope.models, Model);
    };
  }])
  .controller('FollowController', ['$rootScope', '$scope', 'Follow', function($rootScope, $scope, Model) {
    console.log($rootScope.currentUser);
    $rootScope.title = 'Follow';
    Model.find({
      "filter": {
        "include": ['programfollow', 'memberfollow']
      }
    }, function(model) {
      console.log(model);
      $scope.models = model;
    });

    $scope.delete = function(model) {
      // Use common service
      $rootScope.delete(model, $scope.models, Model);
    };
  }])
  .controller('SignupController', ['$rootScope', '$scope', function($rootScope, $scope) {
    console.log('yoyo');
  }])
  .controller('UploadController', ['$rootScope', '$scope', function($rootScope, $scope) {
    console.log('yoyo');
  }])
  .controller('ThemeController', ['$rootScope', '$scope', 'Theme', function($rootScope, $scope, Model) {
    $rootScope.title = 'Theme';
    $scope.models = Model.find({
      filter: {
        include: 'previewImage'
      },
      access_token: $rootScope.currentUser.tokenId
    });
    $scope.delete = function (model) {
      // Use common service
      $rootScope.delete(model, $scope.models, Model);
    };

  }])
  .controller('ThemeDetailController', ['$rootScope', '$routeParams', '$scope', 'Theme', 'FileUploader',
  function($rootScope, $routeParams, $scope, Model, FileUploader) {
    $rootScope.title = 'Theme';
    $scope.model = {};
    if ($routeParams.modelId) {
      Model.findById({
        id: $routeParams.modelId,
        access_token: $rootScope.currentUser.tokenId
      }, {
        filter:{
          include: 'previewImage'
        }

      }, function (model) {
        $scope.model = model;
      });
    };

    $scope.save = function () {
      $rootScope.save($scope.model, 'theme', Model);
    }


    // Upload saga
    // create a uploader with options
    var uploader = $scope.uploader = new FileUploader({
      scope: $scope, // to automatically update the html. Default: $rootScope
      url: '/uploadapi?access_token=' + $rootScope.currentUser.tokenId,
      formData: [
        {
          type: 'theme',
          themeId: $routeParams.modelId
        }
      ]
    });

    // REGISTER HANDLERS
    // --------------------
    uploader.onAfterAddingFile = function (item) {
      console.info('After adding a file', item);
      item.upload();
    };

    // --------------------
    uploader.onSuccessItem = function (item, response, status, headers) {
      $scope.model.image = response.url;

    };
    // --------------------
    uploader.onErrorItem = function (item, response, status, headers) {
      console.info('Error', response, status, headers);
    };
    // --------------------
  }])
  .controller('TemplateController', ['$rootScope', '$scope', 'Template', function($rootScope, $scope, Model) {
    $rootScope.title = 'Template';
    $scope.models = Model.find({
      filter: {
        include: ['previewImage', 'theme']
      },
      access_token: $rootScope.currentUser.tokenId
    });
    $scope.delete = function (model) {
      // Use common service
      $rootScope.delete(model, $scope.models, Model);
    };
  }])
  .controller('TemplateDetailController', ['$rootScope', '$routeParams', '$scope', 'FileUploader', 'Template', 'Theme',
  function($rootScope, $routeParams, $scope, FileUploader, Model, Theme) {
    $rootScope.title = 'Template';
    $scope.themes = Theme.query();
    $scope.model = {
      element: []
    };
    if ($routeParams.modelId) {
      Model.findById({
        id: $routeParams.modelId,
        access_token: $rootScope.currentUser.tokenId
      }, {
        filter:{
          include: 'previewImage'
        }

      }, function (model) {
        $scope.model = model;
      });
    };

    $scope.save = function () {
      $rootScope.save($scope.model, 'template', Model);
    }

    $scope.addNewElement = function () {
      $scope.model.element.push({
        type: 'text',
        data: 'Text',
      });
    }


    // Upload saga
    // create a uploader with options
    var uploader = $scope.uploader = new FileUploader({
      scope: $scope, // to automatically update the html. Default: $rootScope
      url: '/uploadapi?access_token=' + $rootScope.currentUser.tokenId,
      formData: [
        {
          type: 'template',
          templateId: $routeParams.modelId
        }
      ]
    });

    // REGISTER HANDLERS
    // --------------------
    uploader.onAfterAddingFile = function (item) {
      console.info('After adding a file', item);
      item.upload();
    };

    // --------------------
    uploader.onSuccessItem = function (item, response, status, headers) {
      $scope.model.image = response.url;

    };
    // --------------------
    uploader.onErrorItem = function (item, response, status, headers) {
      console.info('Error', response, status, headers);
    };
    // --------------------
  }])

;
