import keyMirror from 'keymirror';

module.exports = {

  ActionTypes: keyMirror({
    MEMBER_INIT: null,
    CLICK_LOGIN: null,
    RECEIVE_FB_RESPONSE: null,
    RECEIVE_LOGIN_RESPONSE: null,
    RECEIVE_LOGOUT_RESPONSE: null,
    RECEIVE_UPDATE_PROFILE_RESPONSE: null,
    RECEIVE_INTEREST_RESPONSE: null,
    INTEREST_INIT: null,
    RECEIVE_THEME_RESPONSE: null,
    RECEIVE_TEMPLATE_RESPONSE: null,
    SELECT_TEMPLATE: null,
    RECEIVE_SUGGESTION_POSTS_RESPONSE: null,
    RECEIVE_SUGGESTION_MEMBERS_RESPONSE: null,
    RECEIVE_FEED_RESPONSE: null,
    RECEIVE_CREATE_POST_RESPONSE: null,
    RECEIVE_POST_RESPONSE: null,

  }),


  APIUrl: 'http://localhost:3000'
  // APIUrl: 'http://122.155.201.110' // Production IP

};
