
import VingDispatcher from '../dispatcher/VingDispatcher';
import VingConstants from '../constants/VingConstants';
import {EventEmitter} from 'events';

const ActionTypes = VingConstants.ActionTypes;
const CHANGE_EVENT = 'change';

let _member = {};
let _currentPage = 'HomeSignup';

const MemberStore = Object.assign({}, EventEmitter.prototype, {

  init: function(member) {
    _member = member;
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  getMember: function() {
    return _member;
  },

  getAccessToken: function() {
    if (_member) {
      return _member.token;
    } else {
      return null;
    }

  },

  isLogin: function() {
    if (_member && _member.token) {
      return true;
    } else {
      return false;
    }
  },

  getCurrentPage: function() {
    return _currentPage;
  }
});


MemberStore.dispatchToken = VingDispatcher.register(function(action) {

  switch(action.type) {

    case ActionTypes.RECEIVE_FB_RESPONSE:
      // VingDispatcher.waitFor([SomeStore.dispatchToken]);
      console.log(action.member);
      _member = action.member;
      MemberStore.emitChange();
      break;

    case ActionTypes.RECEIVE_LOGIN_RESPONSE:
      console.log(action.member);
      _member = action.member;
      MemberStore.emitChange();
      break;

    case ActionTypes.RECEIVE_SIGNUP_RESPONSE:
      console.log(action.member);
      _member = action.member;
      _currentPage = action.currentPage
      MemberStore.emitChange();
      break;
    case ActionTypes.MEMBER_INIT:
      MemberStore.init(action.member);
      _currentPage = action.currentPage;
      MemberStore.emitChange();
      break;
    case ActionTypes.RECEIVE_LOGOUT_RESPONSE:
      _member = {};
      MemberStore.emitChange();
      break;

    default:
      // do nothing
  }

});

module.exports = MemberStore;
