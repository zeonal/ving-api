
import VingDispatcher from '../dispatcher/VingDispatcher';
import VingConstants from '../constants/VingConstants';
import MemberStore from './MemberStore';
import {EventEmitter} from 'events';

const ActionTypes = VingConstants.ActionTypes;
const CHANGE_EVENT = 'change';

let _interests = [];

const InterestStore = Object.assign({}, EventEmitter.prototype, {

  init: function(interests) {
    _interests = interests;
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  getInterests: function() {
    return _interests;
  },
});


InterestStore.dispatchToken = VingDispatcher.register(function(action) {

  switch(action.type) {

    case ActionTypes.RECEIVE_INTEREST_RESPONSE:
      VingDispatcher.waitFor([MemberStore.dispatchToken]);
      _interests = action.interests;
      InterestStore.emitChange();
      break;



    default:
      // do nothing
  }

});

module.exports = InterestStore;
