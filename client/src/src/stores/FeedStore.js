
import VingDispatcher from '../dispatcher/VingDispatcher';
import VingConstants from '../constants/VingConstants';
import MemberStore from './MemberStore';
import {EventEmitter} from 'events';

const ActionTypes = VingConstants.ActionTypes;
const CHANGE_EVENT = 'change';

let _feed = [];

const FeedStore = Object.assign({}, EventEmitter.prototype, {

  /**
   * @param {array} feed Feed from API
   */
  init: function(feed) {
    _feed = feed;
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  getFeed: function() {
    return _feed;
  },


});


FeedStore.dispatchToken = VingDispatcher.register(function(action) {

  switch(action.type) {

    case ActionTypes.RECEIVE_FEED_RESPONSE:
      VingDispatcher.waitFor([MemberStore.dispatchToken]);
      _feed = action.feed;
      FeedStore.emitChange();
      break;




    default:
      // do nothing
  }

});

module.exports = FeedStore;
