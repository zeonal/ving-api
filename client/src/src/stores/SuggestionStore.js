
import VingDispatcher from '../dispatcher/VingDispatcher';
import VingConstants from '../constants/VingConstants';
import MemberStore from './MemberStore';
import {EventEmitter} from 'events';

const ActionTypes = VingConstants.ActionTypes;
const CHANGE_EVENT = 'change';

let _members = [];
let _posts = [];

const SuggestionStore = Object.assign({}, EventEmitter.prototype, {

  /**
   * @param {array} members Suggestion Members from API
   * @param {array} posts Suggestion Posts from API
   */
  init: function(members, posts) {
    _members = members;
    _posts = posts;
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  getMembers: function() {
    return _members;
  },

  getPosts: function () {
    return _posts;
  }
});


SuggestionStore.dispatchToken = VingDispatcher.register(function(action) {

  switch(action.type) {

    case ActionTypes.RECEIVE_SUGGESTION_POSTS_RESPONSE:
      VingDispatcher.waitFor([MemberStore.dispatchToken]);
      _posts = action.posts;
      SuggestionStore.emitChange();
      break;
    case ActionTypes.RECEIVE_SUGGESTION_MEMBERS_RESPONSE:
      VingDispatcher.waitFor([MemberStore.dispatchToken]);
      _members = action.members;
      SuggestionStore.emitChange();
      break;



    default:
      // do nothing
  }

});

module.exports = SuggestionStore;
