
import VingDispatcher from '../dispatcher/VingDispatcher';
import VingConstants from '../constants/VingConstants';
import MemberStore from './MemberStore';
import {EventEmitter} from 'events';

const ActionTypes = VingConstants.ActionTypes;
const CHANGE_EVENT = 'change';

let _themes = [];
let _templates = [];
let _currentThemeId = null;
let _currentTemplateId = null;
let _currentTemplate = null;

const ThemeStore = Object.assign({}, EventEmitter.prototype, {

  init: function(themes) {
    _themes = themes;
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  getThemes: function() {
    return _themes;
  },

  getTemplates: function() {
    return _templates;
  },

  getCurrentThemeId: function() {
    return _currentThemeId;
  },

  getcurrentTemplateId: function() {
    return _currentTemplateId;
  },

  getcurrentTemplate: function() {
    return _currentTemplate;
  },
});


ThemeStore.dispatchToken = VingDispatcher.register(function(action) {

  switch(action.type) {

    case ActionTypes.RECEIVE_THEME_RESPONSE:
      VingDispatcher.waitFor([MemberStore.dispatchToken]);
      _themes = action.themes;
      ThemeStore.emitChange();
      break;

    case ActionTypes.RECEIVE_TEMPLATE_RESPONSE:
      VingDispatcher.waitFor([MemberStore.dispatchToken]);
      _templates = action.templates;
      _currentThemeId = action.themeId;
      ThemeStore.emitChange();
      break;

    case ActionTypes.SELECT_TEMPLATE:
      VingDispatcher.waitFor([MemberStore.dispatchToken]);
      _currentTemplateId = action.templateId;
      for (var i = 0; i < _templates.length; i++) {
        if (_templates[i].id === action.templateId) {
          _currentTemplate = _templates[i];
          break;
        }
      }
      ThemeStore.emitChange();
      break;



    default:
      // do nothing
  }

});

module.exports = ThemeStore;
