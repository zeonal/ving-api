
import VingDispatcher from '../dispatcher/VingDispatcher';
import VingConstants from '../constants/VingConstants';
import MemberStore from './MemberStore';
import {EventEmitter} from 'events';

const ActionTypes = VingConstants.ActionTypes;
const CHANGE_EVENT = 'change';

let _posts = [];
let _currentPost = {};

const PostStore = Object.assign({}, EventEmitter.prototype, {

  /**
   * @param {array} feed Feed from API
   */
  init: function(post) {
    _currentPost = post;
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  getCurrentPost: function() {
    return _currentPost;
  },


});


PostStore.dispatchToken = VingDispatcher.register(function(action) {

  switch(action.type) {

    case ActionTypes.RECEIVE_CREATE_POST_RESPONSE:
      VingDispatcher.waitFor([MemberStore.dispatchToken]);
      _currentPost = action.post;
      PostStore.emitChange();
      break;
    case ActionTypes.RECEIVE_POST_RESPONSE:
      VingDispatcher.waitFor([MemberStore.dispatchToken]);
      _currentPost = action.post;
      PostStore.emitChange();
      break;
    default:
      // do nothing
  }

});

module.exports = PostStore;
