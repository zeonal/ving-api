import React, {Component} from 'react';
import PostCard from './PostCard';
import GroupCard from './GroupCard';

import VingAPIActionCreators from '../actions/VingAPIActionCreators';
import MemberStore from '../stores/MemberStore';
import FeedStore from '../stores/FeedStore';
import {browserHistory} from 'react-router';

function getStateFromStores() {
  return {member: MemberStore.getMember(), isLogin: MemberStore.isLogin(), feed: FeedStore.getFeed()};
}

//a#################################################### data2 ###########################################################################
/*
let data = [
  {
    "id": "5818533971b8580ece3f38dc",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818533971b8580ece3f38db",
    "member": {
      "displayName": "Ton narenrit",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [
        {
          "url": "http://192.168.120.157/images/p1.png",
          "id": "58116f84227b2615c09ca42b",
          "profileId": "58116bfc227b2615c09ca42a"
        }
      ]
    },
    "group": {
      "name": "Test group1",
      "description": "lorem ipsum1",
      "post": [
        {
          "name": "Title test1",
          "description": "description test1",
          "price": 0,
          "defaultImageUrl": "http://192.168.120.157/images/1.png",
          "id": "5818533971b8580ece3f38db",
          "memberId": "58116bfc227b2615c09ca42a",
          "countLike": 1,
          "isLiked": true,
          "countComment": 1,
          "countShare": 0
        }, {
          "name": "Title test2",
          "description": "description test2",
          "price": 10,
          "defaultImageUrl": "http://192.168.120.157/images/2.png",
          "id": "5818540d463f470edf7c1313",
          "memberId": "58116bfc227b2615c09ca42a",
          "countLike": 0,
          "isLiked": false,
          "countComment": 0,
          "countShare": 0
        }, {
          "name": "Title test3",
          "description": "description test3",
          "price": 10,
          "defaultImageUrl": "http://192.168.120.157/images/3.png",
          "id": "5818540d463f470edf7c1313",
          "memberId": "58116bfc227b2615c09ca42a",
          "countLike": 0,
          "isLiked": false,
          "countComment": 0,
          "countShare": 0
        }, {
          "name": "Title test4",
          "description": "description test4",
          "price": 10,
          "defaultImageUrl": "http://192.168.120.157/images/4.png",
          "id": "5818540d463f470edf7c1313",
          "memberId": "58116bfc227b2615c09ca42a",
          "countLike": 0,
          "isLiked": false,
          "countComment": 0,
          "countShare": 0
        }, {
          "name": "Title test1",
          "description": "description test1",
          "price": 0,
          "defaultImageUrl": "http://192.168.120.157/images/5.png",
          "id": "5818533971b8580ece3f38db",
          "memberId": "58116bfc227b2615c09ca42a",
          "countLike": 1,
          "isLiked": true,
          "countComment": 1,
          "countShare": 0
        }
      ]
    }
  }, {
    "id": "5818533971b8580ece3f38dc",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818533971b8580ece3f38db",
    "member": {
      "displayName": "Ton narenrit",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [
        {
          "url": "http://192.168.120.157/images/p2.png",
          "id": "58116f84227b2615c09ca42b",
          "profileId": "58116bfc227b2615c09ca42a"
        }
      ]
    },
    "post": {
      "name": "Title test1",
      "description": "description test1",
      "price": 0,
      "defaultImageUrl": "http://192.168.120.157/images/2.png",
      "id": "5818533971b8580ece3f38db",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 1,
      "isLiked": true,
      "countComment": 1,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [
        {
          "url": "http://192.168.120.157/images/p3.png",
          "id": "58116f84227b2615c09ca42b",
          "profileId": "58116bfc227b2615c09ca42a"
        }
      ]
    },
    "post": {
      "name": "Title test2",
      "description": "description test2",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/3.png",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [
        {
          "url": "http://192.168.120.157/images/p4.png",
          "id": "58116f84227b2615c09ca42b",
          "profileId": "58116bfc227b2615c09ca42a"
        }
      ]
    },
    "post": {
      "name": "Title test3",
      "description": "description test3",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/4.png",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  }, {
    "id": "5818533971b8580ece3f38dc",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818533971b8580ece3f38db",
    "member": {
      "displayName": "Ton narenrit",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [
        {
          "url": "http://192.168.120.157/images/p5.png",
          "id": "58116f84227b2615c09ca42b",
          "profileId": "58116bfc227b2615c09ca42a"
        }
      ]
    },
    "group": {
      "name": "Test group2",
      "description": "lorem ipsum1",
      "post": [
        {
          "name": "Title test1",
          "description": "description test1",
          "price": 0,
          "defaultImageUrl": "http://192.168.120.157/images/5.png",
          "id": "5818533971b8580ece3f38db",
          "memberId": "58116bfc227b2615c09ca42a",
          "countLike": 1,
          "isLiked": true,
          "countComment": 1,
          "countShare": 0
        }, {
          "name": "Title test2",
          "description": "description test2",
          "price": 10,
          "defaultImageUrl": "http://192.168.120.157/images/6.png",
          "id": "5818540d463f470edf7c1313",
          "memberId": "58116bfc227b2615c09ca42a",
          "countLike": 0,
          "isLiked": false,
          "countComment": 0,
          "countShare": 0
        }, {
          "name": "Title test3",
          "description": "description test3",
          "price": 10,
          "defaultImageUrl": "http://192.168.120.157/images/7.png",
          "id": "5818540d463f470edf7c1313",
          "memberId": "58116bfc227b2615c09ca42a",
          "countLike": 0,
          "isLiked": false,
          "countComment": 0,
          "countShare": 0
        }, {
          "name": "Title test4",
          "description": "description test4",
          "price": 10,
          "defaultImageUrl": "http://192.168.120.157/images/8.png",
          "id": "5818540d463f470edf7c1313",
          "memberId": "58116bfc227b2615c09ca42a",
          "countLike": 0,
          "isLiked": false,
          "countComment": 0,
          "countShare": 0
        }, {
          "name": "Title test1",
          "description": "description test1",
          "price": 0,
          "defaultImageUrl": "http://192.168.120.157/images/9.png",
          "id": "5818533971b8580ece3f38db",
          "memberId": "58116bfc227b2615c09ca42a",
          "countLike": 1,
          "isLiked": true,
          "countComment": 1,
          "countShare": 0
        }
      ]
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [
        {
          "url": "http://192.168.120.157/images/p6.png",
          "id": "58116f84227b2615c09ca42b",
          "profileId": "58116bfc227b2615c09ca42a"
        }
      ]
    },
    "post": {
      "name": "Title test4",
      "description": "description test4",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/6.png",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  }, {
    "id": "5818533971b8580ece3f38dc",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818533971b8580ece3f38db",
    "member": {
      "displayName": "Ton narenrit",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [
        {
          "url": "http://192.168.120.157/images/p7.png",
          "id": "58116f84227b2615c09ca42b",
          "profileId": "58116bfc227b2615c09ca42a"
        }
      ]
    },
    "post": {
      "name": "Title test1",
      "description": "description test1",
      "price": 0,
      "defaultImageUrl": "http://192.168.120.157/images/7.png",
      "id": "5818533971b8580ece3f38db",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 1,
      "isLiked": true,
      "countComment": 1,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [
        {
          "url": "http://192.168.120.157/images/p8.png",
          "id": "58116f84227b2615c09ca42b",
          "profileId": "58116bfc227b2615c09ca42a"
        }
      ]
    },
    "post": {
      "name": "Title test2",
      "description": "description test2",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/8.png",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [
        {
          "url": "http://192.168.120.157/images/p1.png",
          "id": "58116f84227b2615c09ca42b",
          "profileId": "58116bfc227b2615c09ca42a"
        }
      ]
    },
    "post": {
      "name": "Title test3",
      "description": "description test3",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/9.png",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [
        {
          "url": "http://192.168.120.157/images/p2.png",
          "id": "58116f84227b2615c09ca42b",
          "profileId": "58116bfc227b2615c09ca42a"
        }
      ]
    },
    "post": {
      "name": "Title test4",
      "description": "description test4",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/10.png",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  }, {
    "id": "5818533971b8580ece3f38dc",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818533971b8580ece3f38db",
    "member": {
      "displayName": "Ton narenrit",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [
        {
          "url": "http://192.168.120.157/images/p3.png",
          "id": "58116f84227b2615c09ca42b",

          "profileId": "58116bfc227b2615c09ca42a"
        }
      ]
    },
    "post": {
      "name": "Title test1",
      "description": "description test1",
      "price": 0,
      "defaultImageUrl": "http://192.168.120.157/images/11.png",
      "id": "5818533971b8580ece3f38db",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 1,
      "isLiked": true,
      "countComment": 1,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [
        {
          "url": "http://192.168.120.157/images/p4.png",
          "id": "58116f84227b2615c09ca42b",
          "profileId": "58116bfc227b2615c09ca42a"
        }
      ]
    },
    "post": {
      "name": "Title test2",
      "description": "description test2",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/12.png",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [
        {
          "url": "http://192.168.120.157/images/p5.png",
          "id": "58116f84227b2615c09ca42b",
          "profileId": "58116bfc227b2615c09ca42a"
        }
      ]
    },
    "post": {
      "name": "Title test3",
      "description": "description test3",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/5.png",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [
        {
          "url": "http://192.168.120.157/images/p6.png",
          "id": "58116f84227b2615c09ca42b",
          "profileId": "58116bfc227b2615c09ca42a"
        }
      ]
    },
    "post": {
      "name": "Title test4",
      "description": "description test4",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/7.png",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  }, {
    "id": "5818533971b8580ece3f38dc",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818533971b8580ece3f38db",
    "member": {
      "displayName": "Ton narenrit",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [
        {
          "url": "http://192.168.120.157/images/p7.png",
          "id": "58116f84227b2615c09ca42b",
          "profileId": "58116bfc227b2615c09ca42a"
        }
      ]
    },
    "post": {
      "name": "Title test1",
      "description": "description test1",
      "price": 0,
      "defaultImageUrl": "http://192.168.120.157/images/2.png",
      "id": "5818533971b8580ece3f38db",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 1,
      "isLiked": true,
      "countComment": 1,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [
        {
          "url": "http://192.168.120.157/images/p8.png",
          "id": "58116f84227b2615c09ca42b",
          "profileId": "58116bfc227b2615c09ca42a"
        }
      ]
    },
    "post": {
      "name": "Title test2",
      "description": "description test2",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/12.png",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [
        {
          "url": "http://192.168.120.157/images/p4.png",
          "id": "58116f84227b2615c09ca42b",
          "profileId": "58116bfc227b2615c09ca42a"
        }
      ]
    },
    "post": {
      "name": "Title test3",
      "description": "description test3",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/3.png",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [
        {
          "url": "http://192.168.120.157/images/p7.png",
          "id": "58116f84227b2615c09ca42b",
          "profileId": "58116bfc227b2615c09ca42a"
        }
      ]
    },
    "post": {
      "name": "Title test4",
      "description": "description test4",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/9.png",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  }
];
*/

class Feed extends Component {

  constructor(props) {
    super(props);
    this.state = getStateFromStores();
    this._onChange = this._onChange.bind(this);
  }

  componentDidMount() {
    MemberStore.addChangeListener(this._onChange);
    FeedStore.addChangeListener(this._onChange);
    if (!this.state.isLogin) {
      browserHistory.push('/test');
    } else {
      VingAPIActionCreators.initFeed(this.state.member.token.id, this.state.member.id);
    }
  }

  componentWillUnmount() {
    MemberStore.removeChangeListener(this._onChange);
    FeedStore.removeChangeListener(this._onChange);
  }

  render() {
    return (
      <div className="row" id="myDiv" style={{
        "marginRight": "3px",
        "marginLeft": "3px",
        "textShadow": "rgb(3, 3, 3) 4px 3px 5px"
      }}>

        {this.state.feed.map(function(item, i) {

          if (item.group) {
            if ((i % 8) === 1) {
              return (<GroupCard key={item.id} data={item} pstion={i + 1}/>);
            } else if ((i % 8) === 0) {
              return (<GroupCard key={item.id} data={item} pstion={i + 1}/>);
            } else {
              return (<GroupCard key={item.id} data={item} pstion={i + 1}/>);
            }
          } else if (item.post) {
            if ((i % 8) === 1) {
              return (<PostCard key={item.id} data={item} pstion={i + 1}/>);
            } else if ((i % 8) === 0) {
              return (<PostCard key={item.id} data={item} pstion={i + 1}/>);
            } else {
              return (<PostCard key={item.id} data={item} pstion={i + 1}/>);
            }
          } else {
            return <div>null</div>;
          }

        }, this)}
      </div>
    );
  }

  _onChange() {
    this.setState(getStateFromStores());
  }
}

export default Feed;
