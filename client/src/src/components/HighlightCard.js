import React, { Component } from 'react';
import classNames from 'classnames';

let cardpost = classNames('card', 'card-inverse');
class HighlightCard extends Component {

    render() {
    	if(this.props.pstion=="NULL")
    	{
    		cardpost = classNames('card', 'card-inverse', 'col-sm-12', 'carousel-item');
    	}
    	else if((this.props.pstion%4)<2)
    	{
    		cardpost = classNames('card', 'card-inverse', 'col-sm-6');
    	}
    	else
    	{
    		cardpost = classNames('card', 'card-inverse', 'col-sm-3');
    	}
        return (
        	
				<div className={cardpost} style={{  "min-height": "235px","mix-height": "auto", "background": "url('"+this.props.data.post.defaultImageUrl+"') no-repeat" , "background-size": "cover", "background-repeat": "no-repeat", "background-position": "center top"}}>
				  
				  <div className="card-img-overlay">
				    <h4 className="card-title" style={{"position": "absolute", "bottom": "85px", "width": "90%", "height": "30px", "overflow": "hidden", "margin":"0px"}}>{ this.props.data.post.name }</h4>
				    <p className="card-text " style={{"position": "absolute", "bottom": "55px", "width": "90%", "height": "22px", "overflow": "hidden"}}>{this.props.data.post.description}</p>
				    <p className="card-text" style={{"position": "absolute", "bottom": "10px", "width": "90%"}}>
				    	<img className="img-responsive img-circle" src={this.props.data.member.profileImage[0].url} style={{width: '60px',height: '60px', "border-radius":"30px","vertical-align": "bottom"}} />
				    	<span className="card-text" style={{"top": "7px", left: "65px", "position": "absolute","font-size":"23px"}}>{ this.props.data.member.displayName }</span>
				    	<span className="text-muted" style={{"top": "36px", left: "65px", "position": "absolute","font-size":"11px"}}>Last updated 2hrs ago</span>
				    </p>
				  </div>
				</div>
         <div className="carousel-item active">
            <img src="http://192.168.120.157/images/postcard.jpg" alt="First slide" />
          </div>
			
           
        );
    }
}

export default HighlightCard;              