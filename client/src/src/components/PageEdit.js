import React, {Component} from 'react';

// import Cropper from 'react-cropper';
import ContentEditable from 'react-contenteditable';
// import clamp from 'lodash/clamp';
// import shuffle from 'lodash/shuffle';
// import SkyLight from 'react-skylight';

import '../ving.css';

class PageEdit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      stations: this.props.stations,
      pageId: this.props.pageId,
      css: this.props.css,
      index: 0,
      img: "",
      position: 0
    };
  }


    render() {
        return (

            <div className="page flex-box flex-box-big" style={{"width": "100vw", 'height': '100vh','overflow': 'hidden', "position": "relative","margin": "0px 0px 0px 0px", "padding": "0px 0px 0px 0px"}}>
            <style>
              {this.props.css}
            </style>
            <div class="hiddenfile" style={{"width":"0px","height":"0px"}}>
              <input ref="fileinput" name="upload" type="file" id="fileinput"/>
            </div>
              <meta id="meta-description" name="description" content={typeof this.props.stations[2]=== 'undefined'?"":this.props.stations[2].data} />
              <meta id="og-title" property="og:title" content={typeof this.props.stations[1]!== 'undefined'?this.props.stations[1].data:""} />
              <meta id="og-image" property="og:image" content={typeof this.props.stations[0]!== 'undefined'?this.props.stations[0].data:""} />
             {this.props.stations.map(function(item, i) {
                return (
                    <ElementEditable type={item.type} data={item.data} stations={this.props.stations} prefix={this.props.prefix} e1={i} action={(data) => this.props.action(data)} modals={this.props.modals} position={(data) => this.props.setPosition(data)} />
                    );
            }, this)}
          </div>
        );
    }
}

//Edit element in Page
class ElementEditable extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: '',
      type: '',
      e1: '',
      update: true
    };
  }

  componentDidUpdate(data) {
    // console.log(this.props.data);
  }
  _handleChange(fieldName, event) {
    console.log('onChange=>', event.target.value);
    let newstations = this.props.stations;
    newstations[this.props.e1].data = event.target.value;
    this.props.action({stations: newstations});
    // console.log('onChange=>',newstations);
  }

  _openModal(event) {
    this.props.position(this.props.e1);
    this.props.modals.show();
    if (typeof event.target.pause() === 'undefined') {
      event.target.pause();
    }

  }

  // play video toggle
  _videoPlay(event) {
    if (event.target.paused) {
      event.target.play();
    } else {
      event.target.pause();
    }
  }


     render() {
        return (
            <div className={this.props.prefix+this.props.e1 + ((this.props.data==="")?"":" edit")}  >

            {(() => {
                switch (this.props.type) {
                case "img":
                    return (<a className="btn-edit" ref={'mcrp'+this.props.e1} > <img src={this.props.data} alt="img" onClick={() => this._openModal(this.props.e1)}/></a>);
                case "video":
                    return (<video className="" controls onClick={(event) => this._openModal(event)}>
                              <source src={this.props.data} type="video/mp4"/>
                            </video>);
                case "text":
                    return (<p><ContentEditable className="" html={this.props.data} action={(data) => this.props.action(data)} onChange={(event) => this._handleChange('data', event)}/></p>);
                default:
                    return <p></p>;
                }
            })()}

        </div>
            );
    }
}

export default PageEdit;
