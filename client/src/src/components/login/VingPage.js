import React, {Component} from 'react';

import Nav from './Nav';
import LoginForm from './LoginForm';
import Footer from './Footer';

class VingPage extends Component {
    render() {
        return (
            <div>
                <Nav />
                <LoginForm />
                <Footer />
            </div>
        );
    }
}

export default VingPage;