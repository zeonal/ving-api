import React, {Component} from 'react';
// import {
//     Navbar, Nav, NavItem, NavDropdown, MenuItem
// } from 'react-bootstrap';
import '../../css/nav.css';

class VingNav extends Component {
    render() {
        return (

            <nav className="navbar navbar-static-top navbar-dark bg-inverse">
                <div className="container">
                    <a className="navbar-brand" href="#">VING</a>

                    <button
                        className="float-xs-right btn btn-info"
                        style={{marginLeft:'8px'}}
                        type="button">Create</button>
                    <span className="float-xs-right" style={{paddingLeft:'2px',paddingRight:'2px'}}>
                        <img alt="Member's avatar" className="img-responsive rounded-circle" src="http://placehold.it/40x40" />
                    </span>

                    <button
                        className="navbar-toggler float-xs-right"
                        style={{marginLeft:'3px'}}
                        type="button" data-toggle="collapse"
                        data-target="#navbar-header"
                        aria-controls="navbar-header"
                        aria-expanded="false"
                        aria-label="Toggle navigation"></button>

                    <span className="form-inline float-xs-right">
                        <input
                            className="form-control navbar-search float-xs-right text-center"
                            type="text"
                            placeholder="Search now" />
                    </span>
                </div>
            </nav>
        );
    }
}

export default VingNav;
