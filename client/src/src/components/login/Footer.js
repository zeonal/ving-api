import React, {Component} from 'react';
import '../../css/footer.css';

class Footer extends Component {
    render() {
        return (
            <footer className="footer bg-inverse">
                <div className="container">
                    <p className="text-muted">
                        <div className="col-md-4">
                            <span className="text-white"> @Ving 2016 </span>
                        </div>
                        <div className="col-md-8">
                        <span className="float-xs-right">
                            <ul className="nav navbar-nav text-white">
                                <li className="nav-item">
                                    <a href="/feedback">Feedback</a>
                                </li>
                                <li className="nav-item">
                                    <a href="/termofuse">Term of use</a>
                                </li>
                                <li className="nav-item">
                                    <a href="/privicy">Privacy Policy</a>
                                </li>
                            </ul>
                        </span>
                        </div>
                    </p>
                </div>
            </footer>
        );
    }
}

export default Footer;