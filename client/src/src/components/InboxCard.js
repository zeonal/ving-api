import React, {Component} from 'react';
import '../css/InboxCard.css';

class InboxCard extends Component {
    render() {
        const inboxCards = this.props.cards.map(function(card) {
            return (
                <div key={card.id} className="container-fluid inbox-card">
                    <img className="left-most" src={card.toMember.picPath} alt="profile" />
                    <div className="inbox-content">
                        <h6 className="inbox-name">{card.toMember.displayName}</h6>
                        <h6 className="inbox-comment">{card.message[card.message.length-1].message}</h6>
                    </div>
                    <img className="right-most" src="./testPics/letter.png" alt="letter pic" />
                </div>
            );
        });
        return <div>{inboxCards}</div>;
    }
}

export default InboxCard;