import React, { Component } from 'react';

class Nav extends Component {

    render() {
        return (
            <div className="navbar navbar-dark bg-inverse bg-faded">
               <div className="container">
               <button className="navbar-toggler hidden-lg-up" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"></button>
                  <div className="collapse navbar-toggleable-md" id="navbarResponsive">
                     <a className="navbar-brand navehomebrand"  href="#" >Ving</a>
                     <div className="form-inline float-lg-right " role="form">
                        <div className="form-group has-success has-feedback">
                           <input type="text" className="form-control form-control-sm form-control-success" id="inputSuccess1" />
                           <span className="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>
                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                        <button className="btn navbar-icon-menu hidden-xs btn-sm" type="button" ></button>
                        <img alt="home" className="rounded-circle imgHeadhome" src={"data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22100%22%20height%3D%22100%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20100%20100%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_1584320dc49%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_1584320dc49%22%3E%3Crect%20width%3D%22100%22%20height%3D%22100%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2224.435001373291015%22%20y%3D%2254.598999977111816%22%3E100x100%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"} />
                        <button type="button" className="btn btn-primary btn-sm">+ Create</button>
                     </div>

                  </div>
               </div>
            </div>
            );
    }
}

export default Nav;
