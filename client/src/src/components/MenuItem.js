import React, {Component} from 'react';
import '../css/MenuItem.css';

class MenuItem extends Component {
    render() {
        return (
            <div className="menu">
                <ul>
                    <li>
                        <a href="#">
                            <img className="custom-icon" src="./testPics/list.jpeg" alt="My Profile" />
                            <p className="menu-text">My Profile</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img className="custom-icon" src="./testPics/list.jpeg" alt="My Subscribe" />
                            <p className="menu-text">My Subscribe</p>
                        </a>
                    </li>
                    <li className="bottom-underline">
                        <a href="#">
                            <img className="custom-icon" src="./testPics/list.jpeg" alt="My Interested" />
                            <p className="menu-text">My Interested</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img className="custom-icon" src="./testPics/list.jpeg" alt="Setting" />
                            <p className="menu-text">Setting</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img className="custom-icon" src="./testPics/list.jpeg" alt="Help" />
                            <p className="menu-text">Help</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img className="custom-icon" src="./testPics/list.jpeg" alt="Log out" />
                            <p className="menu-text">Log out</p>
                        </a>
                    </li>
                </ul>
            </div>
        );
    }
}

export default MenuItem;