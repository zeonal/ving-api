import React, {Component} from 'react';

import '../css/memberCover.css';

// import VingAPIActionCreators from '../actions/VingAPIActionCreators';
import MemberStore from '../stores/MemberStore';
import { browserHistory } from 'react-router';

function getStateFromStores() {
  return {
    member: MemberStore.getMember(),
    isLogin: MemberStore.isLogin(),
  };
}

class MemberCover extends Component {
  constructor(props) {
    super(props);
    this.state = getStateFromStores();
    this._onChange = this._onChange.bind(this);
    console.log(this.state.member);
  }

  componentDidMount() {
    MemberStore.addChangeListener(this._onChange);
    if (!this.state.isLogin) {
      browserHistory.push('/test');
    }

  }

  componentWillUnmount() {
    MemberStore.removeChangeListener(this._onChange);
  }

  render() {
    return (
      <div className="jumbotron" style={{
          backgroundColor: '#666'
      }}>
        <div className="container">
          <div className="row">
            <div className="col-md"></div>
            <div className="col-xs align-center" style={{
              textAlign: 'center'
            }}>
              <div style={{
                backgroundImage: 'url(http://placehold.it/150x150)',
                backgroundSize: 'cover',
                width: '20vh',
                height: '20vh',
                paddingTop: '8vh',
                margin: 'auto',
                textAlign: 'center'
              }} className="rounded-circle">
                <i className="fa fa-camera-retro fa-lg align-middle"></i>
              </div>
              <h2 style={{
                paddingTop: '25px',
                textAlign: 'center',
                color: 'white'
              }}>Name Lorem Ipsum
                &nbsp;<i className="fa fa-pencil"></i>
              </h2>
              <p style={{
                textAlign: 'center',
                color: 'white'
              }}>Lorem ipsum dolor sit amet,
              </p>
              {/* Overlay */}


            </div>
            <div className="col-md"></div>

          </div>
          <div className="row">
            <div className="col-md"></div>
            <div className="col-xs">
              <div className="stats">
                <ul style={{margin: 'auto'}}>
                  <li>98<span>Posts</span>
                  </li>
                  <li>298<span>Followings</span>
                  </li>
                  <li>923<span>Followers</span>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-md"></div>
          </div>

        </div>

      </div>

    );
  }

  _onChange() {
    this.setState(getStateFromStores());
  }
}

export default MemberCover;
