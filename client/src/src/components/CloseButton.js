import React, { Component } from "react";

class CloseButton extends Component {
  render() {
    return (
      <div>
        <input type="button" defaultValue='close' onClick={() => { window.location = '/close';}} />
      </div>
    );
  }
}
export default CloseButton;
