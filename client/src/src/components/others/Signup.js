import React, {Component} from 'react';
import './../ving.css';

import MemberStore from '../stores/MemberStore';
import VingLoginActionCreators from '../actions/VingLoginActionCreators';

function getStateFromStores() {
  return {member: MemberStore.getMember(), isLogin: MemberStore.isLogin()};
}

class Signup extends Component {

  constructor(props) {
    super(props);
    this.state = getStateFromStores();
    this._onChange = this._onChange.bind(this);
    // this._onClick = this._onClick.bind(this);
  }

  componentDidMount() {
    MemberStore.addChangeListener(this._onChange);
  }

  componentWillUnmount() {
    MemberStore.removeChangeListener(this._onChange);
  }

  render() {
    return (
      <div className="signup-container">
        <div className="right-container">
        <form>
          <h2>Signup</h2>

          <p><label>Display Name:</label>
            <input type="text" onChange={(event) => this._handleField('displayName', event)}  /></p>
            <p><label>Email:</label>
              <input type="text" onChange={(event) => this._handleField('email', event)} /></p>
          <p><label >First Name:</label>
            <input type="text" onChange={(event) => this._handleField('firstname', event)} />
          </p>
          <p><label >Last Name:</label>
            <input type="text" onChange={(event) => this._handleField('lastname', event)} />
          </p>

          <p><input type="submit" value="Submit" onSubmit={(e) => {

              e.preventDefault();

            }}/></p>
        </form>
        </div>
      </div>
    );
  }

  _handleField(fieldName, event) {
    let newState = {};
    newState[fieldName] = event.target.value;
    this.setState(newState);
    this._onChange = this._onChange.bind(this);
  }

  _onChange() {
    this.setState(getStateFromStores());
  }
}

export default Signup;
