import React, {Component} from 'react';
import bg from '../../images/templates/01.png';
import bg2 from '../../images/templates/02.png';
import bg3 from '../../images/templates/03.png';
import bg4 from '../../images/templates/04.png';
import bg5 from '../../images/templates/05.png';
import '../../ving.css';

import {Motion, spring} from 'react-motion';
import clamp from 'lodash/clamp';
import TemplateBox from '../TemplateBox';
import {Swipeable, CustomGesture, moves} from 'react-touch';
import ContentEditable from 'react-contenteditable';

import './../../css/font.css';
import './../../ving.css';

const CIRCLE = [
  moves.RIGHT,
  moves.DOWNRIGHT,
  moves.DOWN,
  moves.DOWNLEFT,
  moves.LEFT,
  moves.UPLEFT,
  moves.UP,
  moves.UPRIGHT,
  moves.RIGHT
];

class Page extends React.Component {
  state = {
    iconsDraggable: false,
    currentlyDragged: null
  };

  _onSwipe(increment) {
    if (this.state.iconsDraggable) {
      return;
    }
    this.props.onSwipe(increment);
  }

  _handleChange(event) {
    console.log(event);
  }

  render() {
    return (
      <CustomGesture config={CIRCLE} >
        <Swipeable onSwipeLeft={() => this._onSwipe(1)} onSwipeRight={() => this._onSwipe(-1)}>
          <div className="page">
            <div className="viewer" style={this.props.content.style}>
              {this.props.content.elements.map((element, index) => {

                if (element.type === 'text') {
                  return (
                    <div key={index} style={element.style}>
                      <ContentEditable html={element.data} onChange={this._handleChange}/>
                    </div>
                  );
                }
                else {
                  return (
                    <div key={index} style={element.style}>
                      {element.data}
                    </div>
                  );
                }

              })}

            </div>
          </div>
        </Swipeable>
      </CustomGesture>
    );
  }
}

let pages = [
  {
    elements: [
      {
        style: {
          width: '100%',
          height: '100%',
          backgroundSize: 'cover',
          backgroundImage: 'url(' + bg + ')'
        },
        data: '',
        type: 'background'
      }, {
        style: {
          position: 'relative',
          left: '50px',
          bottom: '250px',
          fontFamily: 'Pattaya, sans-serif',
          fontSize: '4em',
          color: 'white',
          width: '100%',
          transform: 'rotate(-10deg)'
        },
        data: 'Good Goodlife.',
        type: 'text'
      },
      {
        style: {
          position: 'relative',
          left: '50px',
          top: '50px',
          fontFamily: 'Pattaya, sans-serif',
          fontSize: '4em',
          color: 'white',
          width: '100%',
          transform: 'rotate(-10deg)'
        },
        data: 'testtttt',
        type: 'text'
      }
    ]
  },
  {
    elements: [
      {
        style: {
          width: '100%',
          height: '100%',
          backgroundSize: 'cover',
          backgroundImage: 'url(' + bg + ')'
        },
        data: '',
        type: 'background'
      }, {
        style: {
          position: 'relative',
          left: '50px',
          bottom: '250px',
          fontFamily: 'Pattaya, sans-serif',
          fontSize: '4em',
          color: 'white',
          width: '100%',
          transform: 'rotate(-10deg)'
        },
        data: 'Good Goodlife.',
        type: 'text'
      },
      {
        style: {
          position: 'relative',
          left: '50px',
          top: '50px',
          fontFamily: 'Pattaya, sans-serif',
          fontSize: '4em',
          color: 'white',
          width: '100%',
          transform: 'rotate(-10deg)'
        },
        data: 'testtttt',
        type: 'text'
      }
    ]
  },

  {
    elements: [
      {
        style: {
          width: '100%',
          height: '100%',
          backgroundSize: 'cover',
          backgroundImage: 'url(' + bg + ')'
        },
        data: '',
        type: 'background'
      }, {
        style: {
          position: 'relative',
          left: '50px',
          bottom: '170px',
          fontFamily: 'Pattaya, sans-serif',
          fontSize: '4em',
          color: 'white',
          width: '100%',
          transform: 'rotate(-10deg)'
        },
        data: 'Good music Goodlife.',
        type: 'text'
      }
    ]
  }, {
    elements: [
      {
        style: {
          width: '100%',
          height: '100%',
          backgroundSize: 'cover',
          backgroundImage: 'url(' + bg2 + ')'
        },
        data: '',
        type: 'background'
      }, {
        style: {
          position: 'absolute',
          left: '60px',
          bottom: '100px',
          fontFamily: 'Pattaya, sans-serif',
          fontSize: '5em',
          color: 'white',
          width: '100%',
          transform: 'rotate(-10deg)'
        },
        data: 'Chapter 1',
        type: 'text'
      }
    ]
  },
  {
    "elements": [{
        style: {
          "width": "20%",
          "backgroundColor": "#000",
          "height": "100%",
          "float": "left"
        },
        "type": "text",
        "data": " "
      },
      {
        style : {
          "display": "block",
          "width": "80%",
          "float": "right",
          "height": "31vh",
          "opacity": "1",
          "background": "no-repeat",
          "backgroundImage": "url(" + bg4 + ")",
          "backgroundSize": "contain"
        },
        "type": "img",
        "data": ""
      },
      {
        style: {
          "fontFamily": "'CSChatThaiUI', Sans-Serif",
          "width": "79%",
          "float": "left",
          "paddingLeft": "5%",
          "paddingRight": "5px",
          "paddingTop": "5%",
          "textAlign": "left",
          "fontSize": "0.8em"
        },
        "type": "text",
        "data": "ดนตรีเป็นศิลปะที่อาศัยเสียงเพื่อเป็นสื่อในการถ่าย ทอดอารมณ์ความรู้สึกต่าง ๆไปสู่ผู้ฟังเป็นศิลปะที่ง่ายต่อ การสัมผัส ก่อให้เกิดความสุขความปลื้มปิติพึงพอใจให้ แก่มนุษย์ได้ นอกจากนี้ได้มีนักปราชญ์ท่านหนึ่งได้กล่าว ไว้ว่า “ดนตรีเป็นภาษาสากลของมนุษยชาติเกิดขึ้นจาก ธรรมชาติและมนุษย์ได้นำมาดัดแปลงแก้ไขให้ประณีต งดงามไพเราะเมื่อฟังดนตรีแล้วทำให้เกิดความรู้สึกนึกคิดต่างๆ” นั้นก็เป็นเหตุผลหนึ่งที่ทำให้เราได้ทราบว่ามนุษย์ ไม่ว่าจะเป็นชนชาติใดภาษาใดก็สามารถรับรู้อรรถรสของดนตรีได้โดยใช้เสียงเป็นสื่อได้เหมือนกัน"
      },
      {
        style: {
          "fontFamily": "'CSChatThaiUI', Sans-Serif",
          "width": "79%",
          "float": "left",
          "paddingLeft": "5%",
          "paddingRight": "5px",
          "paddingTop": "5%",
          "textAlign": "left",
          "fontSize": "0.8em"
        },
        "type": "text",
        "data": " มีบุคคลจำนวนไม่น้อยที่ตั้งคำถามว่า “ดนตรีคืออะไร” แล้ว “ทำไมต้องมีดนตรี” คำว่า “ดนตรี” ในพจนานุกรม ฉบับราชบัณฑิตยสถาน พ.ศ. 2525 ได้ให้ความหมายไว้ ว่า “เสียงที่ประกอบกันเป็นทำนองเพลง เครื่องบรรเลง ซึ่งมีเสียงดังทำให้รู้สึกเพลิดเพลิน หรือเกิดอารมณ์รัก โศก หรือรื่นเริง”จากความหมายข้างต้นจึงทำให้เราได้ทราบ คำตอบที่ว่าทำไมต้องมีดนตรีก็เพราะว่าดนตรีช่วยทำให้ มนุษย์เรารู้สึกเพลิดเพลินได้"
      }
    ]
  },
  {
    style: {
      backgroundColor: 'white'
    },
    elements: [
      {
        style: {
          fontFamily: 'CSChatThaiUI, Sans-Serif',
          backgroundColor: '#000',
          color: '#FFF',
          textAlign: 'left',
          fontSize: '30px',
          paddingTop: '30px',
          paddingLeft: '5%',
          paddingRight: '50%',
          lineHeight: 1
        },
        "type": "text",
        "data": "Good music,\n Good life."
      }, {
        style: {
          "fontFamily": "CSChatThaiUI, sans-serif",
          "backgroundColor": "rgb(0, 0, 0)",
          "color": "#666",
          "textAlign": "left",
          "fontSize": "17px",
          "paddingLeft": "5%",
          "paddingRight": "5%",
          "paddingTop": "15px",
          "paddingBottom": "15px",
          "marginTop": "-3px"
        },
        "type": "text",
        "data": "ดนตรีเป็นสิ่งที่ธรรมชาติให้มาพร้อม ๆ กับชีวิตมนุษย์โดยที่มนุษย์เองไม่รู้ตัว..."
      }, {
        style: {
          display: 'block',
          width: '100%',
          height: '40vh',
          margin: '0 auto 1em',
          opacity: 1,
          marginTop: '0px',
          backgroundSize: 'contain',
          backgroundColor: 'black',
          backgroundImage: 'url(' + bg3 + ')'
        },
        "type": "img",
        "data": ""
      }, {
        style: {
          "fontFamily": "'CSChatThaiUI', Sans-Serif",
          "width": "45%",
          "height": "50px",
          "fontSize": "18px",
          "paddingLeft": "10px",
          "lineHeight": "1",
        },
        "type": "text",
        "data": "Good music Good life."
      }, {
        style: {
          "fontFamily": "'CSChatThaiUI', Sans-Serif",
          "width": "45%",
          "height": "50px",
          "float": "left",
          "paddingLeft": "10px",
        },
        "type": "text",
        "data": "มีบุคคลจำนวนไม่น้อยที่ตั้งคำถามว่า “ดนตรีคืออะไร” แล้ว “ทำไมต้องมีดนตรี” คำว่า “ดนตรี” ในพจนานุกรม ฉบับราชบัณฑิตยสถาน พ.ศ. 2525 ได้ให้ความหมายไว้ ว่า “เสียงที่ประกอบกันเป็นทำนองเพลง เครื่องบรรเลง ซึ่งมีเสียงดังทำให้รู้สึกเพลิดเพลิน หรือเกิดอารมณ์รัก โศก หรือรื่นเริง”จากความหมายข้างต้นจึงทำให้เราได้ทราบ คำตอบที่ว่าทำไมต้องมีดนตรีก็เพราะว่าดนตรีช่วยทำให้ มนุษย์เรารู้สึกเพลิดเพลินได้"
      }, {
        style: {
          "fontFamily": "'CSChatThaiUI', Sans-Serif",
          "width": "45%",
          "height": "50px",
          "float": "right",
          "top": "-50px",
          "position": "relative",
          "paddingLeft": "0px",
        },
        "type": "text",
        "data": "ดนตรีเป็นศิลปะที่อาศัยเสียงเพื่อเป็นสื่อในการถ่าย ทอดอารมณ์ความรู้สึกต่าง ๆไปสู่ผู้ฟังเป็นศิลปะที่ง่ายต่อ การสัมผัส ก่อให้เกิดความสุขความปลื้มปิติพึงพอใจให้ แก่มนุษย์ได้ นอกจากนี้ได้มีนักปราชญ์ท่านหนึ่งได้กล่าว ไว้ว่า “ดนตรีเป็นภาษาสากลของมนุษยชาติเกิดขึ้นจาก ธรรมชาติและมนุษย์ได้นำมาดัดแปลงแก้ไขให้ประณีต งดงามไพเราะเมื่อฟังดนตรีแล้วทำให้เกิดความรู้สึกนึกคิดต่างๆ” นั้นก็เป็นเหตุผลหนึ่งที่ทำให้เราได้ทราบว่ามนุษย์ ไม่ว่าจะเป็นชนชาติใดภาษาใดก็สามารถรับรู้อรรถรสของดนตรีได้โดยใช้เสียงเป็นสื่อได้เหมือนกัน"
      }
    ]
  },
  {
      style: {
        backgroundColor: 'black'
      },
      "elements": [{
          style: {
            "display":"block",
            "width":"30%",
            "height": "100%",
            "opacity":"1",
            "float": "right",
            "backgroundImage": "url(" + bg5 + ")",
            "backgroundSize": "cover"
          },
          "type": "img",
          "data": ""
        },
        {
          style: {
            "fontFamily": "'CSChatThaiUI', Sans-Serif",
            "width": "70%",
            "backgroundColor": "#000",
            "color": "#FFF",
            "position": "relative",
            "float":"left",
            "fontSize":"30px",
            "verticalAlign":"80%",
            "textAlign":"right",
            "paddingTop": "25vh",
            "paddingRight":"20px",
            "lineHeight": "1",
          },
          "type": "text",
          "data": "a"
        },
        {
          style: {
            "fontFamily": "'CSChatThaiUI', Sans-Serif",
            "width": "70%",
            "backgroundColor": "#000",
            "color": "#FFF",
            "position": "relative",
            "float":"left",
            "fontSize":"30px",
            "verticalAlign":"80%",
            "textAlign":"right",
            "paddingTop": "0px",
            "paddingRight":"20px",
            "lineHeight": "1",
          },
          "type": "text",
          "data": "melody"
        },
        {
          style: {
            "fontFamily": "'CSChatThaiUI', Sans-Serif",
            "width": "70%",
            "backgroundColor": "#000",
            "color": "#FFF",
            "position": "relative",
            "float":"left",
            "fontSize":"30px",
            "verticalAlign":"80%",
            "textAlign":"right",
            "paddingTop": "0px",
            "paddingBottom": "8vh",
            "paddingRight":"20px",
            "lineHeight": "1",
          },
          "type": "text",
          "data": "also tune."
        },
        {
          style: {
            "fontFamily": "'CSChatThaiUI', Sans-Serif",
            "width": "70%",
            "backgroundColor": "#000",
            "color": "#999",
            "position": "relative",
            "float":"left",
            "fontSize":"0.9em",
            "verticalAlign":"80%",
            "textAlign":"right",
            "paddingTop": "0px",
            "paddingBottom": "5vh",
            "paddingRight":"20px",
            "paddingLeft":"25px",
            "lineHeight": "1.2",
          },
          "type": "text",
          "data": "“ดนตรี” มีความหมาย ที่กว้างและหลากหลายมาก การนำดนตรี ไปใช้ประกอบต่างๆที่เราคุ้นเคยเช่น การใช้ประกอบในภาพยนต์ เนื่องจาก ดนตรีนั้นสามารถนำไปเป็นพื้นฐานใน การสร้างอารมณ์ลักษณะต่างๆของ แต่ละฉากได้ พิธีกรรมทางศาสนา ก็มีการนำดนตรีเข้าไปมี ส่วนร่วมด้วย จึงทำให้มีความขลัง ความน่าเชื่อถือ ความศรัทธามีประสิทธิภาพมากขึ้น "
        },
        {
          style: {
            "fontFamily": "'CSChatThaiUI', Sans-Serif",
            "width": "70%",
            "backgroundColor": "#000",
            "color": "#999",
            "position": "relative",
            "float":"left",
            "fontSize":"0.9em",
            "verticalAlign":"80%",
            "textAlign":"right",
            "paddingTop": "0px",
            "paddingBottom": "8vh",
            "paddingRight":"20px",
            "paddingLeft":"25px",
            "lineHeight": "1.2",
          },
          "type": "text",
          "data": "  นอกจากนี้ดนตรีบางประเภทถูกนำ ไปใช้ในการเผยแพร่ความเป็นอันหนึ่ง อันเดียวกันของกลุ่มคนหรือเชื้อชาติ"
        }
      ],
    },
];

class Viewer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      animate: '',
      currentPage: 0
    };
  }

  _onSwipe(increment) {
    const currentPage = clamp(this.state.currentPage + increment, 0, pages.length - 1);
    this.setState({currentPage});
  }

  render() {

    const w = window,
      d = document,
      e = d.documentElement,
      g = d.getElementsByTagName('body')[0],
      width = w.innerWidth || e.clientWidth || g.clientWidth;
      // height = w.innerHeight || e.clientHeight || g.clientHeight;

    return (
      <div className="viewer-container">
        {pages.map((page, idx) => {
          const left = (idx - this.state.currentPage) * width;
          return (
            <Motion key={idx} style={{
              left: spring(left, {stiffness: 170, damping: 26})
            }}>
              {(style) => <div style={style} className="page-wrapper">
                <Page key={idx} onSwipe={increment => this._onSwipe(increment)} content={page}/>
              </div>}
            </Motion>
          );
        })}
        <TemplateBox />
      </div>
    );
  }
}

export default Viewer;
