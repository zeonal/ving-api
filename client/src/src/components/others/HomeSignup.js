import React, {Component} from 'react';
// import logo from './logo.svg';
import './../ving.css';
import homeImage from './../images/login-left-container.jpeg';

import VingLoginActionCreators from '../actions/VingLoginActionCreators';

class HomeSignup extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      confirmPassword: '',
    };
  }

  render() {
    return (
      <div className="login-container">
        <div className="left-container" style={{
          backgroundImage: 'url(' + homeImage + ')'
          }}>
        </div>
        <div className="right-container">
        <form onSubmit={(e) => {
          e.preventDefault();
          if (this.state.password !== ''
          && this.state.password === this.state.confirmPassword) {
            //this.props.action('signup', {});
            VingLoginActionCreators.signup(this.state.email, this.state.password);
          } else {
            alert('Password not matched!');
          }
        }}>
          <h2>Signup</h2>

          <p>We Strive to Empower Content Creator. Join Us!</p>

            <p><label>Email:</label>
              <input type="text" onChange={(event) =>
                  this._handleField('email', event)}/></p>
          <p><label >Password:</label>
            <input type="password" className="password" name="password"
              onChange={(event) => this._handleField('password', event)} />
          </p>
          <p><label >Confirm Password:</label>
            <input type="password" className="password" name="ConfirmPssword"
              onChange={(event) => this._handleField('confirmPassword', event)}
              />
          </p>

          <p><input type="submit" value="Signup"/></p>
        </form>
        </div>
      </div>
    );
  }

  _handleField(fieldName, event) {
    let newState = {};
    newState[fieldName] = event.target.value;
    this.setState(newState);
  }

}

export default HomeSignup;
