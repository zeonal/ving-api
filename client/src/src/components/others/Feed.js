import React, {Component} from 'react';
import bg from '../images/templates/01.png';
import Pattaya from '../fonts/Pattaya-Regular.woff';


class Feed extends Component {

  constructor(props) {
    super(props);
    this.state = {
      animate: ''
    };
  }

  componentDidMount() {

  }

  render() {
    console.log(Pattaya);
    return (
      <div className="feed">
        <div className="viewer-container">
          <div className="viewer">
            <div style={{
                flex: 1,
                width: '100%',
                height: '100%',
                backgroundSize: 'cover',
                backgroundImage: 'url(' + bg + ')'
              }}>

            </div>
            <div style={{
                position: 'relative',
                bottom: '200px',
                fontFamily: 'Pattaya, sans-serif',
                fontSize: '3em',
                color: 'white',
                width: '100%',
                transform: 'rotate(-14deg)'
              }}>Good music Goodlife.</div>
          </div>
        </div>

        <a href="#create" onClick={()=>{
            this.setState({
              animate: 'animated fadeIn'
            });
            }}>Open</a>
          <div className="modal" id="create" aria-hidden="true">
              <div className="modal-dialog">
                <div className="modal-header">
                  <h2>
                    Create
                  </h2>
                  <a href="#close" className="btn-close" aria-hidden="true">×</a>
                </div>
                <div className="modal-body">
                  Create
                </div>
                <div className="modal-footer">
                </div>
              </div>
            </div>
      </div>
    );
  }
}

export default Feed;
