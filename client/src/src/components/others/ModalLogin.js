import React, {Component} from 'react';
import './../ving.css';

import VingLoginActionCreators from '../actions/VingLoginActionCreators';
import MemberStore from '../stores/MemberStore';

function getStateFromStores() {
  return {
    member: MemberStore.getMember(),
    isLogin: MemberStore.isLogin()
  };
}


class ModalLogin extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    };

    this._onClickLoginEmail = this._onClickLoginEmail.bind(this);
    this._onChange = this._onChange.bind(this);
  }

  componentDidMount() {
    MemberStore.addChangeListener(this._onChange);
  }

  componentWillUnmount() {
    MemberStore.removeChangeListener(this._onChange);
  }



  render() {
    return (
      <div className="modal" id={this.props.modalName} aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-header">
            <h2>
              Login
            </h2>
            <a href="#close" className="btn-close" aria-hidden="true">×</a>
          </div>
          <div className="modal-body">
            <p><label>Email:</label>
                <input type="text" onChange={(event) =>
                    this._handleField('email', event)}/></p>
            <p><label >Password:</label>
              <input type="password" className="password" name="password"
                onChange={(event) => this._handleField('password', event)} />
            </p>
            <a href="#close" className="btn" onClick={this._onClickLoginEmail}>
              Login</a>

            <p> or </p>
            <a href="#close" onClick={this._onClickLoginFB}>
              <button>Login with Facebook</button>
            </a>
          </div>
          <div className="modal-footer">
            <a href="#close" onClick={this._onClickForgetPassword}>
              Forget your password?</a>
          </div>
        </div>
      </div>
    );
  }

  _handleField(fieldName, event) {
    let newState = {};
    newState[fieldName] = event.target.value;
    this.setState(newState);
    this._onChange = this._onChange.bind(this);
  }

  _onChange() {
    this.setState(getStateFromStores());
  }

  _onClickLoginEmail() {
    VingLoginActionCreators.loginWithEmail(
      this.state.email, this.state.password);
  }

  _onClickLoginFB() {
    VingLoginActionCreators.loginWithFB();
  }

  _onClickForgetPassword() {

  }
}

export default ModalLogin;
