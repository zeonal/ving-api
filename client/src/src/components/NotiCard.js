import React, {Component} from 'react';
import '../css/NotiCard.css';

class NotiCard extends Component {
    render() {
        const notiCards = this.props.cards.map(function(card) {
            return (
                <div key={card.id} className="container-fluid noti-card">
                    <img className="left-most" src={card.picPath} alt="profile" />
                    <div className="noti-content">
                        <h6 className="noti-name">{card.name}</h6>
                        <h6 className="noti-comment">{card.comment}</h6>
                        <h6 className="noti-dt">{card.time}</h6>
                    </div>
                    <img className="right-most" src="./testPics/defPic.png" alt="post pic" />
                </div>
            );
        });
        return <div>{notiCards}</div>;
    }
}

export default NotiCard;