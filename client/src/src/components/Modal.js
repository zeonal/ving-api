import React, { Component } from 'react';
import Modals from 'react-skylight';
import CommentBox from './CommentBox';

class Modal extends Component {

  render() {
    let myBigGreenDialog = {
           backgroundColor: '#E8E8E8',
          // color: '#ffffff',
          width: '90%',
          top : '5%',
          left : '30%',
          height: '90%',
          marginTop:'0'
        };
    return (
      <div className="container ModalComment">
          <button onClick={() => this.refs.customDialogzxc1.show()}>Open Modal</button>
        <Modals dialogStyles={myBigGreenDialog} hideOnOverlayClicked ref="customDialogzxc1" >
          <div className="row">
            <div className="col-md-7 col-lg-7 col-xl-8" style={{}} >
              <div className="Modalcontent" style={{}} >
                Content
              </div>
            </div>
            <div className="col-md-5 col-lg-5 col-xl-4" >
              <CommentBox />
            </div>
          </div>
        </Modals>
      </div>
    )
  }
}

export default Modal;
