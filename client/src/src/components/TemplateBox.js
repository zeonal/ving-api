import React, { Component } from 'react';
import Modals from 'react-skylight';

// Connect API
import VingAPIActionCreators from '../actions/VingAPIActionCreators';
import VingConstants from '../constants/VingConstants'
import MemberStore from '../stores/MemberStore';
import ThemeStore from '../stores/ThemeStore';
import { browserHistory } from 'react-router';
import classNames from 'classnames';


function getStateFromStores() {
  return {
    member: MemberStore.getMember(),
    themes: ThemeStore.getThemes(),
    templates: ThemeStore.getTemplates(),
    currentThemeId: ThemeStore.getCurrentThemeId(),
    currentTemplateId: ThemeStore.getcurrentTemplateId(),
    currentTemplate: ThemeStore.getcurrentTemplate(),
    isLogin: MemberStore.isLogin()
  };
}

class TemplateBox extends Component {

  constructor(props) {
    super(props);
    this.state = getStateFromStores();
    this._onChange = this._onChange.bind(this);
    this._addPage = this._addPage.bind(this);
  }

  componentDidMount() {
    MemberStore.addChangeListener(this._onChange);
    ThemeStore.addChangeListener(this._onChange);
    if (!this.state.isLogin) {
      browserHistory.push('/test');
    } else {
      // console.log(this.state.member);
      // Should get theme and template from here
      VingAPIActionCreators.initThemes(this.state.member.token.id);
    }
  }

  componentWillUnmount() {
    MemberStore.removeChangeListener(this._onChange);
    ThemeStore.removeChangeListener(this._onChange);
  }

  render() {
    let templateDialog = {
           backgroundColor: '#fff',
          // color: '#ffffff',
          width: '100%',
          top : '5%',
          left : '25%',
          height: '90%',
          marginTop:'0'
        };
    return (
      <div>
        <button onClick={() => this.refs.templateDialog.show()} style={{
            position: 'absolute',
            bottom: '50px',
            right: '50px',
            width: '50px',
            height: '50px',
            borderRadius: '25px',
            backgroundColor: 'lightBlue',
            color: 'white'
          }}><i className="fa fa-plus" aria-hidden="true"></i></button>
        <Modals dialogStyles={templateDialog} hideOnOverlayClicked ref="templateDialog" >
          <div className="row">
            <div className="col-md-7 col-lg-7 col-xl-12" style={{}} >
              {
                this.state.themes.map((theme, index) => {
                  return (
                    <img key={theme.id}
                    src={VingConstants.APIUrl + theme.previewImage[0].url}
                    alt={theme.name}
                    className={classNames({
                      'template-active': theme.id === this.state.currentThemeId
                    })}
                    style={{width: '20%', margin: '5px'}}
                    onClick={()=>{ this._selectTheme(theme.id) }}
                    />);
                })
              }
            </div>
          </div>
          <div className="row">
            <div className="col-md-7 col-lg-7 col-xl-12" style={{}} >
              {
                this.state.templates.map((template, index) => {
                  return (
                    <img key={template.id}
                    src={VingConstants.APIUrl + template.previewImage[0].url}
                    alt={template.name}
                    className={classNames({
                      'template-active': template.id === this.state.currentTemplateId
                    })}
                    style={{width: '20%', margin: '5px'}}
                    onClick={()=>{ this._selectTemplate(template.id) }}
                    />);
                })
              }
            </div>
          </div>
          <div className="row">
            <div className="col-md-7 col-lg-7 col-xl-12" style={{}} >
              <button onClick={() => { this._addPage() }} style={{
                  width: '100%',
                  height: '50px',
                  borderRadius: '25px',
                  backgroundColor: 'lightBlue',
                  color: 'white'
                }}>Add Page</button>
            </div>
          </div>
        </Modals>
      </div>
    )
  }

  _onChange() {
    this.setState(getStateFromStores());
  }

  _selectTheme(themeId) {
    VingAPIActionCreators.initTemplates(this.state.member.token.id, themeId);
  }

  _selectTemplate(templateId) {
    VingAPIActionCreators.selectTemplate(templateId);
  }

  _addPage() {
    if (this.state.currentTemplateId) {
      // Create page logic here
      console.log(this.state.currentTemplate);
      VingAPIActionCreators.createPage(this.state.member.token.id, this.state.member.id, this.state.currentTemplate, this.props.postId);
      VingAPIActionCreators.getPostDetail(this.state.member.token.id, this.props.postId);

    } else {
      alert('Please select template');
    }
  }
}

export default TemplateBox;
