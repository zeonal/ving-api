import React, {Component} from "react";

class SubscribeCard extends Component{
	render(){
		return (
			<div className="row subscribe-card" style={{'margin-bottom':'0px','padding-top': '35px','border-radius': '2px'}}>
        <div className="col-sm-10 offset-md-1">
       		<a className="subscribe-img" href="#">
            <img alt="Content" className="media-object subscribe-imgcard" src={this.props.item.url}/>
          </a>
          <div className="media subscribe-width">
            <div className="media-body" style={{'padding-left': '10px'}}>
              <h4 className="media-heading">
                {this.props.item.name}
              </h4>
                {this.props.item.content}
								<br />
								<div style={{margin: '20px'}}>
									<img alt="Content's owner" className="rounded-circle subscribe-imguser" src={this.props.item.member.url}/>
		              <label className="text-middle">{this.props.item.member.displayName}</label>
		              <label className="float-xs-right subscribe-fontdate">{this.props.item.create_at}</label>
								</div>

            </div>
          </div>
       	</div>
      </div>
		);
	}
}
export default SubscribeCard;
