import React, { Component } from 'react';
import PageEdit from './PageEdit';
import CropImage from './CropImage';
import clamp from 'lodash/clamp';
// import {Motion, spring} from 'react-motion';
// import {Draggable, Holdable, Swipeable, CustomGesture, moves} from 'react-touch';
import {Swipeable} from 'react-touch';
import SkyLight from 'react-skylight';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';


import '../page.css';

let accessToken = '';
const hostserver = '0.0.0.0'



const BigDialog = {
      width: '100vw',
      height: '100vh',
      marginTop: '0px',
      marginLeft: '0px',
      top: '0px',
      left: '0px',
      overflow: 'scroll'
    };


class PostEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            postId:this.props.postId,
            index: 0,
            stations:[],
            css: "",
            prefix: "",
            pageId:[],
            isLoaded: false
        }
        accessToken  = this.props.access_token;
    }

    action(data) {
      console.log('action=>',data.stations);

        if (typeof data.stations !== 'undefined') {
            this.setState({
              stations: data.stations
            });
            console.log('pageId=>',this.state.pageId[this.state.index].id);
            fetch(`http://${hostserver}:3000/api/Pages/updatePage?access_token=${accessToken}`, {
                method: "POST",
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                  pageId: this.state.pageId[this.state.index].id,
                  element: this.state.stations
                })
              })
              .then((response) => {
                return response.json()
              })
              .then((json) => {
                // console.log(json.element)
                console.log("this reload")
                this.forceUpdate();
              }).catch(function(ex) {
                console.log('parsing failed', ex)
              });

        } else if (typeof data.img !== 'undefined') {
            console.log('SetImg', data.img)
            this.setState({
              img: data.img
            });
        }
      // console.log(data.stations);
    }

    //set data images
    setimg(url){
        if (typeof url === 'undefined') {
          return;
        }
        let newstations = this.state.stations;
        // let current = newstations[this.state.position].data;
        newstations[this.state.position].data=url;
        newstations[this.state.position].type="img";
        console.log('newstations', newstations)
        this.action({
            index: this.state.index,
            stations: newstations
        });
    }

    //set data Video
    setvideo(url){
        if (typeof url === 'undefined') {
          return;
        }
        let newstations = this.state.stations;
        // let current = newstations[this.state.position].data;
        newstations[this.state.position].data=url;
        newstations[this.state.position].type="video";
        console.log('newstations', newstations)
        this.action({
            index: this.state.index,
            stations: newstations
        });
    }


    // set position data for images
    setPosition(int){
         if (typeof int === 'undefined') {
          return;
        }
        // console.log('_setPosition', int)
        this.setState({
            position: int
        });
    }

    componentDidMount(){
    //get API Post on load
      fetch(`http://${hostserver}:3000/api/Posts/detail?access_token=${accessToken}`,
            {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    postId: this.state.postId
                })
            })
            .then((response) => {
                return response.json()
            })
            .then((json) => {
                console.log('on load Posts',json.page);
                // pageId = json.page;
                 this.setState({
                      pageId: json.page,
                      isLoaded: true
                    });
                    //get API page on load
                    fetch(`http://${hostserver}:3000/api/Pages/detail?access_token=${accessToken}`,
                        {
                            method: "POST",
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({
                                pageId: this.state.pageId[this.state.index].id
                            })
                        })
                        .then((response) => {
                            return response.json()
                        })
                        .then((json) => {
                            console.log('on load page',json.element, json.css)
                            this.setState({
                                stations: json.element,
                                css: json.css,
                                prefix:json.prefix
                            });
                            this.render();
                        }).catch(function(ex) {
                        console.log('parsing failed', ex)
                    });

            }).catch(function(ex) {
            console.log('parsing failed', ex)
        });


    }

    _onSwipe(increment){
        const currentPage = clamp(this.state.index + increment, 0, this.state.pageId.length - 1);
        this.setState({index:currentPage});
       //get API page on Swipe
        fetch(`http://${hostserver}:3000/api/Pages/detail?access_token=${accessToken}`,
            {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    pageId: this.state.pageId[this.state.index].id
                })
            })
            .then((response) => {
                return response.json()
            })
            .then((json) => {
                // console.log(json.element, json.css)
                this.setState({
                    stations: json.element,
                    css: json.css,
                    prefix:json.prefix
                });
            }).catch(function(ex) {
            console.log('parsing failed', ex);
        });
    }




    render() {
        if (!this.state.isLoaded) {
            return (<div>Loading</div>);

        }
        // var PageEdit = <PageEdit pageId={this.state.pageId[this.state.index].id} stations={this.state.stations} css={this.state.css} action={(data) => this.action(data)} modals={this.refs.simpleDialog} setPosition={(data) => this.setPosition(data)} />;

        return (
            <div>
                <Swipeable onSwipeLeft={() => this._onSwipe(1)} onSwipeRight={() => this._onSwipe(-1)}>

                    <div className="flex-boxes" id="myDiv" style={{}}>
                        <ReactCSSTransitionGroup
                            transitionName="sample">
                        <PageEdit className="sample" key={this.state.pageId[this.state.index].id} pageId={this.state.pageId[this.state.index].id} stations={this.state.stations} css={this.state.css} prefix={this.state.prefix} action={(data) => this.action(data)} modals={this.refs.simpleDialog} setPosition={(data) => this.setPosition(data)} />
                        </ReactCSSTransitionGroup>
                        <div className="page-number"><p className="page-number-p"><i className="page-number-bg">{`${this.state.index+1}/${this.state.pageId.length}`}</i></p></div>
                    </div>

                </Swipeable>
                 <SkyLight dialogStyles={BigDialog} hideOnOverlayClicked ref="simpleDialog" title="Edit Image">
                    <CropImage data={this.state} img={(data) => this.setimg(data)} video={(data) => this.setvideo(data)} modals={this.refs.simpleDialog} guides={false} access_token={accessToken} hostserver={hostserver}/>
                 </SkyLight>
            </div>
            );
    }
}
export default PostEdit;
