import React, {Component} from 'react';
import '../css/interest.css';
import VingConstants from '../constants/VingConstants';


class InterestCard extends Component {
  render() {
    return (
        <div className="card col-lg-4 col-md-6 col-xs-12 interest">
          <img className="card-img-responsive" src={this.props.item.image ? VingConstants.APIUrl + this.props.item.image[0].url: ''} alt="Card" style={{maxWidth: "100%"}}/>
        </div>
    );
  }
}

export default InterestCard;
