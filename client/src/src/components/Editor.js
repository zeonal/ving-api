import React, {Component} from 'react';
// import PostCard from './PostCard';
// import GroupCard from './GroupCard';

import VingAPIActionCreators from '../actions/VingAPIActionCreators';
import MemberStore from '../stores/MemberStore';
import PostStore from '../stores/PostStore';
import {browserHistory} from 'react-router';

// Components
import TemplateBox from './TemplateBox';

function getStateFromStores() {
  return {
    member: MemberStore.getMember(),
    isLogin: MemberStore.isLogin(),
    currentPost: PostStore.getCurrentPost(),
  };
}

class Editor extends Component {

  constructor(props) {
    super(props);
    this.state = getStateFromStores();
    this._onChange = this._onChange.bind(this);
    this._createPost = this._createPost.bind(this);


  }

  componentDidMount() {
    MemberStore.addChangeListener(this._onChange);
    PostStore.addChangeListener(this._onChange);
    if (!this.state.isLogin) {
      browserHistory.push('/test');
    }

    // console.log(this.props.location.query.postId);
    if (this.props.location.query.postId) {
      VingAPIActionCreators.getPostDetail(this.state.member.token.id, this.props.location.query.postId);
    }
  }

  componentWillUnmount() {
    MemberStore.removeChangeListener(this._onChange);
    PostStore.removeChangeListener(this._onChange);
  }

  render() {

    if (!this.state.currentPost.page) {
      return (<div>Loading</div>);
    }
    return (
      <div className="container" style={{
        "marginRight": "3px",
        "marginLeft": "3px"
      }}> Test Editor
      {this.state.currentPost.page.map((page) => {
        console.log(page);
        return <div className="row" key={page.id}>{page.id}</div>;
      })}
      <TemplateBox postId={this.state.currentPost.id}/>
      </div>
    );
  }

  _onChange() {

    this.setState(getStateFromStores());
    console.log(this.state.currentPost);
  }

  _createPost() {
    VingAPIActionCreators.createPost(this.state.member.token.id, this.state.member.id);
  }
}

export default Editor;
