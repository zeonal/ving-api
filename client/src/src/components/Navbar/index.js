import React, {Component} from 'react';
import '../../ving.css';

import MemberStore from '../../stores/MemberStore';
import VingLoginActionCreators from '../../actions/VingLoginActionCreators';

function getStateFromStores() {
  return {member: MemberStore.getMember(), isLogin: MemberStore.isLogin()};
}

class Navbar extends Component {

  constructor(props) {
    super(props);
    this.state = getStateFromStores();
    this._onChange = this._onChange.bind(this);
    this._onClick = this._onClick.bind(this);
  }

  componentDidMount() {
    MemberStore.addChangeListener(this._onChange);
  }

  componentWillUnmount() {
    MemberStore.removeChangeListener(this._onChange);
  }

  render() {
    let loginComponent = null;
    let logoutComponent = null;
    if (this.state.isLogin) {
      loginComponent = (
        <li className="nav-link">
          <a href="#">{'Hello '}{this.state.member.displayName}!</a>
        </li>
      );
      logoutComponent = (
        <li className="nav-link">
          <a href="#" onClick={this._onClick}>Logout</a>
        </li>
      );
    } else {
      loginComponent = (
        <li className="nav-link">
          <a href="#login" onClick={this._onClick}>
            <button>Login</button>
          </a>
        </li>
      );
    }
    /*
    <div>
      <header className="navigation" role="banner">
        <div className="navigation-wrapper">
          <a href="#" className="navigation-menu-button" id="js-mobile-menu">MENU</a>
          <nav role="navigation">
            <ul id="js-navigation-menu" className="navigation-menu show">
              <li className="nav-link" style={{
                'paddingLeft': '20px'
              }}>
                <a href="/">Ving</a>
              </li>
            </ul>
          </nav>
          <div className="navigation-tools">
            <ul className="navigation-menu show">

              {loginComponent}
              {logoutComponent}
            </ul>
          </div>
        </div>
      </header>
    </div>
    */
    return (
        <nav className="navbar bg-faded" style={{
          backgroundColor: 'rgba(21,31,40,0.5)'
        }}>
          <a className="navbar-brand" href="#" style={{
            color: 'white'
          }}>Ving</a>
          <ul className="nav navbar-nav float-xs-right">
            <li className="nav-item">{this.state.member ? this.state.member.email: ''}</li>
            <li className="nav-item">
              <a className="nav-link" href="#" onClick={this._logout} style={{
                color: 'white',
                textDecoration: 'underline'
              }}>Logout</a>
            </li>
          </ul>
        </nav>
    );
  }

  /**
   * Event handler for 'change' events coming from the stores
   */
  _onChange() {
    this.setState(getStateFromStores());
  }

  _onClick() {
    VingLoginActionCreators.logout(MemberStore.getAccessToken());
  }
}

export default Navbar;
