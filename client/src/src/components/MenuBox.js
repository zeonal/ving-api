import React, {Component} from 'react';
import '../css/MenuBox.css';

import NotiCard from './NotiCard';
//import InboxCard from './InboxCard';
//import MenuItem from './MenuItem';

class MenuBox extends Component {
    render() {
        const notiCards = [
        {
            "id": "5818002bd109db7113b89793",
            "memberId": "57a194e83f5dfd5263894e12",
            "name": "jack",
            "picPath": "./testPics/jack.jpeg",
            "comment": "hehe, long time no see!",
            "time": "4:53:20 PM"
        },
        {
            "id": "58116ec918b9889d168c3c34",
            "memberId": "57a194e83f5dfd5263894e13",
            "name": "john",
            "picPath": "./testPics/john.jpeg",
            "comment": "Hey, buy me some food",
            "time": "3:42:10 PM"
        },
        {
            "id": "58116e9501f484651618007c",
            "memberId": "57a194e83f5dfd5263894e14",
            "name": "arcane",
            "picPath": "./testPics/arcane.jpeg",
            "comment": "I bought a cartoon for you dude.",
            "time": "3:41:00 PM"
        },
        {
            "id": "58116e850e232e521693bfbc",
            "memberId": "57a194e83f5dfd5263894e15",
            "name": "yugi",
            "picPath": "./testPics/yugi.jpeg",
            "comment": "meet you tommorow bro!",
            "time": "1:05:45 PM"
        }];

        const inboxCards = 

            {
                "status": "success",
                "message": "",
                "data": 
                [
                    {
                        "create_at": "2016-10-27T02:19:06.417Z",
                        "update_at": "2016-10-28T07:49:44.516Z",
                        "status": "active",
                        "id": "5811641ad256e48f11e2cec3",
                        "fromMemberId": "57a194e83f5dfd5263894e12",
                        "toMemberId": "57a194e83f5dfd5263894e13",
                        "count": 6,
                        "fromMember": {
                            "displayName": "jack",
                            "id": "56f369c4956732710bfca858"
                    },
                    "toMember": {
                        "displayName": "john",
                        "id": "57a194e83f5dfd5263894e13",
                        "picPath": "./testPics/john.jpeg",
                    },
                    "message": [
                        {
                            "message": "Hello, John!",
                            "status": "unread",
                            "create_at": "2016-10-28T07:49:44.546Z",
                            "id": "5813031819788fd326942f15",
                            "chatId": "5811641ad256e48f11e2cec3",
                            "owner": "57a194e83f5dfd5263894e13"
                        }
                    ]
                    },
                    {
                        "create_at": "2016-10-27T02:19:06.417Z",
                        "update_at": "2016-10-28T07:49:44.516Z",
                        "status": "active",
                        "id": "5721641ck256e45f21e3cqc1",
                        "fromMemberId": "57a194e83f5dfd5263894e12",
                        "toMemberId": "57a194e83f5dfd5263894e13",
                        "count": 6,
                        "fromMember": {
                            "displayName": "jack",
                            "id": "56f369c4956732710bfca858"
                    },
                    "toMember": {
                        "displayName": "john",
                        "id": "57a194e83f5dfd5263894e13",
                        "picPath": "./testPics/john.jpeg",
                    },
                    "message": [
                        {
                            "message": "How r u doin?",
                            "status": "unread",
                            "create_at": "2016-10-28T07:49:44.546Z",
                            "id": "5813031819788fd326942f15",
                            "chatId": "5811641ad256e48f11e2cec3",
                            "owner": "57a194e83f5dfd5263894e13"
                        }
                    ]
                    },
                    {
                        "create_at": "2016-10-27T02:19:06.417Z",
                        "update_at": "2016-10-28T07:49:44.516Z",
                        "status": "active",
                        "id": "5009123ad256e48w65e2opo2",
                        "fromMemberId": "57a194e83f5dfd5263894e12",
                        "toMemberId": "57a194e83f5dfd5263894e14",
                        "count": 6,
                        "fromMember": {
                            "displayName": "jack",
                            "id": "56f369c4956732710bfca858"
                    },
                    "toMember": {
                        "displayName": "arcane",
                        "id": "57a194e83f5dfd5263894e14",
                        "picPath": "./testPics/arcane.jpeg",
                    },
                    "message": [
                        {
                            "message": "Happy birthday arcane!",
                            "status": "unread",
                            "create_at": "2016-10-28T07:49:44.546Z",
                            "id": "5813031819788fd326942f15",
                            "chatId": "5811641ad256e48f11e2cec3",
                            "owner": "57a194e83f5dfd5263894e13"
                        }
                    ]
                    },
                    {
                        "create_at": "2016-10-27T02:19:06.417Z",
                        "update_at": "2016-10-28T07:49:44.516Z",
                        "status": "active",
                        "id": "5322120ad256e48y60e2xzx1",
                        "fromMemberId": "57a194e83f5dfd5263894e12",
                        "toMemberId": "57a194e83f5dfd5263894e13",
                        "count": 6,
                        "fromMember": {
                            "displayName": "jack",
                            "id": "56f369c4956732710bfca858"
                    },
                    "toMember": {
                        "displayName": "yugi",
                        "id": "57a194e83f5dfd5263894e15",
                        "picPath": "./testPics/yugi.jpeg",
                    },
                    "message": [
                        {
                            "message": "Tomorrow morning mate.",
                            "status": "unread",
                            "create_at": "2016-10-28T07:49:44.546Z",
                            "id": "5813031819788fd326942f15",
                            "chatId": "5811641ad256e48f11e2cec3",
                            "owner": "57a194e83f5dfd5263894e13"
                        }
                    ]
                    }
                ]
            }

        return (
            <div className="popup-box">
                <div className="box-head">
                    <p className="box-name">Carl</p>
                    <div className="box-menu">
                        <ul>
                            <li>
                                <a href="#">
                                    <img className="icon" src="./testPics/letter.png" alt="noti" width="30px" height="30px" />
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img className="icon" src="./testPics/letter.png" alt="letter" width="30px" height="30px" />
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img className="icon" src="./testPics/letter.png" alt="network" width="30px" height="30px" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="box-body">
                    {/*<MenuItem />*/}
                    <NotiCard cards={notiCards} />
                    {/*<InboxCard cards={inboxCards.data} />*/}
                </div>
            </div>
        );
    }
}

export default MenuBox;