import React, {Component} from 'react';
import '../css/interest.css';
import InterestCard from './InterestCard';

import VingAPIActionCreators from '../actions/VingAPIActionCreators';
import MemberStore from '../stores/MemberStore';
import InterestStore from '../stores/InterestStore';
import { browserHistory } from 'react-router';

function getStateFromStores() {
  return {
    member: MemberStore.getMember(),
    isLogin: MemberStore.isLogin(),
    interests: InterestStore.getInterests()
  };
}

class InterestBox extends Component {
  constructor(props) {
    super(props);
    this.state = getStateFromStores();
    this._onChange = this._onChange.bind(this);
  }

  componentDidMount() {
    MemberStore.addChangeListener(this._onChange);
    InterestStore.addChangeListener(this._onChange);
    if (!this.state.isLogin) {
      browserHistory.push('/test');
    } else {
      VingAPIActionCreators.initInterests(this.state.member.token.id);
    }
  }

  componentWillUnmount() {
    MemberStore.removeChangeListener(this._onChange);
    InterestStore.removeChangeListener(this._onChange);
  }

  render() {
    return (
      <div className="container text-xs-center interest">
        <section className="jumbotron jumbotron-fluid interest">
          <div className="container" style={{width: "80%"}}>
            <h1 className="display-4">Interested</h1>
            <p className="lead">Choose some topics you are Interested in</p>
          </div>
        </section>
        <div className="album text-muted text-xs-center interest">
          <div className="container" style={{width: "80%"}}>
            <div className="row interest">
              {
            	  this.state.interests.map((item, index) => {
            		return(
            			<InterestCard key={item.id} item={item}/>
            			)
            		},this)
              }
            </div> {/* end row */}
          </div> {/* end container */}
          <button type="button" onClick={this._handleNext} className="btn btn-primary" style={{backgroundColor: "#424242", borderColor: "#424242"}}>Next</button>
        </div> {/* end album text-muted */}
      </div>
    );
  }

  _onChange() {
    this.setState(getStateFromStores());
  }

  _handleNext() {
    browserHistory.push('/memberSuggestion');
  }
}


export default InterestBox;
