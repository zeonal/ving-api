import React, { Component } from 'react';

import {ReactBoostrapCarousel} from 'react-boostrap-carousel';
import '../css/bootstrap.min.css';
import '../css/react-boostrap-carousel.css';


import PostCard from './PostCard';
import GroupCard from './GroupCard';

//a#################################################### data2 ###########################################################################
let data = [{
  "id": "5818533971b8580ece3f38dc",
  "memberId": "58116bfc227b2615c09ca42a",
  "postId": "5818533971b8580ece3f38db",
  "member": {
    "displayName": "Ton narenrit",
    "id": "58116bfc227b2615c09ca42a",
    "profileImage": [{
      "url": "http://192.168.120.157/images/profile.jpg",
      "id": "58116f84227b2615c09ca42b",
      "profileId": "58116bfc227b2615c09ca42a"
    }]
  },
   "group": {
      "name": "Test group1",
      "description": "lorem ipsum1",
      "post": [{
            "name": "Title test1",
            "description": "description test1",
            "price": 0,
            "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
            "id": "5818533971b8580ece3f38db",
            "memberId": "58116bfc227b2615c09ca42a",
            "countLike": 1,
            "isLiked": true,
            "countComment": 1,
            "countShare": 0
          }, {
            "name": "Title test2",
            "description": "description test2",
            "price": 10,
            "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
            "id": "5818540d463f470edf7c1313",
            "memberId": "58116bfc227b2615c09ca42a",
            "countLike": 0,
            "isLiked": false,
            "countComment": 0,
            "countShare": 0
          },{
            "name": "Title test3",
            "description": "description test3",
            "price": 10,
            "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
            "id": "5818540d463f470edf7c1313",
            "memberId": "58116bfc227b2615c09ca42a",
            "countLike": 0,
            "isLiked": false,
            "countComment": 0,
            "countShare": 0
          },{
            "name": "Title test4",
            "description": "description test4",
            "price": 10,
            "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
            "id": "5818540d463f470edf7c1313",
            "memberId": "58116bfc227b2615c09ca42a",
            "countLike": 0,
            "isLiked": false,
            "countComment": 0,
            "countShare": 0
          },{
            "name": "Title test1",
            "description": "description test1",
            "price": 0,
            "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
            "id": "5818533971b8580ece3f38db",
            "memberId": "58116bfc227b2615c09ca42a",
            "countLike": 1,
            "isLiked": true,
            "countComment": 1,
            "countShare": 0
          }
      ]
    }
  }, {
    "id": "5818533971b8580ece3f38dc",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818533971b8580ece3f38db",
    "member": {
      "displayName": "Ton narenrit",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [{
        "url": "http://192.168.120.157/images/profile.jpg",
        "id": "58116f84227b2615c09ca42b",
        "profileId": "58116bfc227b2615c09ca42a"
      }]
    },
    "post": {
      "name": "Title test1",
      "description": "description test1",
      "price": 0,
      "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
      "id": "5818533971b8580ece3f38db",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 1,
      "isLiked": true,
      "countComment": 1,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [{
        "url": "http://192.168.120.157/images/profile.jpg",
        "id": "58116f84227b2615c09ca42b",
        "profileId": "58116bfc227b2615c09ca42a"
      }]
    },
    "post": {
      "name": "Title test2",
      "description": "description test2",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [{
        "url": "http://192.168.120.157/images/profile.jpg",
        "id": "58116f84227b2615c09ca42b",
        "profileId": "58116bfc227b2615c09ca42a"
      }]
    },
    "post": {
      "name": "Title test3",
      "description": "description test3",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  },{
  "id": "5818533971b8580ece3f38dc",
  "memberId": "58116bfc227b2615c09ca42a",
  "postId": "5818533971b8580ece3f38db",
  "member": {
    "displayName": "Ton narenrit",
    "id": "58116bfc227b2615c09ca42a",
    "profileImage": [{
      "url": "http://192.168.120.157/images/profile.jpg",
      "id": "58116f84227b2615c09ca42b",
      "profileId": "58116bfc227b2615c09ca42a"
    }]
  },
   "group": {
      "name": "Test group2",
      "description": "lorem ipsum1",
      "post": [{
            "name": "Title test1",
            "description": "description test1",
            "price": 0,
            "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
            "id": "5818533971b8580ece3f38db",
            "memberId": "58116bfc227b2615c09ca42a",
            "countLike": 1,
            "isLiked": true,
            "countComment": 1,
            "countShare": 0
          }, {
            "name": "Title test2",
            "description": "description test2",
            "price": 10,
            "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
            "id": "5818540d463f470edf7c1313",
            "memberId": "58116bfc227b2615c09ca42a",
            "countLike": 0,
            "isLiked": false,
            "countComment": 0,
            "countShare": 0
          },{
            "name": "Title test3",
            "description": "description test3",
            "price": 10,
            "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
            "id": "5818540d463f470edf7c1313",
            "memberId": "58116bfc227b2615c09ca42a",
            "countLike": 0,
            "isLiked": false,
            "countComment": 0,
            "countShare": 0
          },{
            "name": "Title test4",
            "description": "description test4",
            "price": 10,
            "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
            "id": "5818540d463f470edf7c1313",
            "memberId": "58116bfc227b2615c09ca42a",
            "countLike": 0,
            "isLiked": false,
            "countComment": 0,
            "countShare": 0
          },{
            "name": "Title test1",
            "description": "description test1",
            "price": 0,
            "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
            "id": "5818533971b8580ece3f38db",
            "memberId": "58116bfc227b2615c09ca42a",
            "countLike": 1,
            "isLiked": true,
            "countComment": 1,
            "countShare": 0
          }
      ]
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [{
        "url": "http://192.168.120.157/images/profile.jpg",
        "id": "58116f84227b2615c09ca42b",
        "profileId": "58116bfc227b2615c09ca42a"
      }]
    },
    "post": {
      "name": "Title test4",
      "description": "description test4",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  },{
    "id": "5818533971b8580ece3f38dc",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818533971b8580ece3f38db",
    "member": {
      "displayName": "Ton narenrit",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [{
        "url": "http://192.168.120.157/images/profile.jpg",
        "id": "58116f84227b2615c09ca42b",
        "profileId": "58116bfc227b2615c09ca42a"
      }]
    },
    "post": {
      "name": "Title test1",
      "description": "description test1",
      "price": 0,
      "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
      "id": "5818533971b8580ece3f38db",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 1,
      "isLiked": true,
      "countComment": 1,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [{
        "url": "http://192.168.120.157/images/profile.jpg",
        "id": "58116f84227b2615c09ca42b",
        "profileId": "58116bfc227b2615c09ca42a"
      }]
    },
    "post": {
      "name": "Title test2",
      "description": "description test2",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [{
        "url": "http://192.168.120.157/images/profile.jpg",
        "id": "58116f84227b2615c09ca42b",
        "profileId": "58116bfc227b2615c09ca42a"
      }]
    },
    "post": {
      "name": "Title test3",
      "description": "description test3",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [{
        "url": "http://192.168.120.157/images/profile.jpg",
        "id": "58116f84227b2615c09ca42b",
        "profileId": "58116bfc227b2615c09ca42a"
      }]
    },
    "post": {
      "name": "Title test4",
      "description": "description test4",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  },{
    "id": "5818533971b8580ece3f38dc",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818533971b8580ece3f38db",
    "member": {
      "displayName": "Ton narenrit",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [{
        "url": "http://192.168.120.157/images/profile.jpg",
        "id": "58116f84227b2615c09ca42b",


        "profileId": "58116bfc227b2615c09ca42a"
      }]
    },
    "post": {
      "name": "Title test1",
      "description": "description test1",
      "price": 0,
      "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
      "id": "5818533971b8580ece3f38db",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 1,
      "isLiked": true,
      "countComment": 1,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [{
        "url": "http://192.168.120.157/images/profile.jpg",
        "id": "58116f84227b2615c09ca42b",
        "profileId": "58116bfc227b2615c09ca42a"
      }]
    },
    "post": {
      "name": "Title test2",
      "description": "description test2",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [{
        "url": "http://192.168.120.157/images/profile.jpg",
        "id": "58116f84227b2615c09ca42b",
        "profileId": "58116bfc227b2615c09ca42a"
      }]
    },
    "post": {
      "name": "Title test3",
      "description": "description test3",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [{
        "url": "http://192.168.120.157/images/profile.jpg",
        "id": "58116f84227b2615c09ca42b",
        "profileId": "58116bfc227b2615c09ca42a"
      }]
    },
    "post": {
      "name": "Title test4",
      "description": "description test4",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  },{
    "id": "5818533971b8580ece3f38dc",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818533971b8580ece3f38db",
    "member": {
      "displayName": "Ton narenrit",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [{
        "url": "http://192.168.120.157/images/profile.jpg",
        "id": "58116f84227b2615c09ca42b",
        "profileId": "58116bfc227b2615c09ca42a"
      }]
    },
    "post": {
      "name": "Title test1",
      "description": "description test1",
      "price": 0,
      "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
      "id": "5818533971b8580ece3f38db",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 1,
      "isLiked": true,
      "countComment": 1,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [{
        "url": "http://192.168.120.157/images/profile.jpg",
        "id": "58116f84227b2615c09ca42b",
        "profileId": "58116bfc227b2615c09ca42a"
      }]
    },
    "post": {
      "name": "Title test2",
      "description": "description test2",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [{
        "url": "http://192.168.120.157/images/profile.jpg",
        "id": "58116f84227b2615c09ca42b",
        "profileId": "58116bfc227b2615c09ca42a"
      }]
    },
    "post": {
      "name": "Title test3",
      "description": "description test3",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  }, {
    "id": "5818540d463f470edf7c1314",
    "memberId": "58116bfc227b2615c09ca42a",
    "postId": "5818540d463f470edf7c1313",
    "member": {
      "displayName": "member",
      "id": "58116bfc227b2615c09ca42a",
      "profileImage": [{
        "url": "http://192.168.120.157/images/profile.jpg",
        "id": "58116f84227b2615c09ca42b",
        "profileId": "58116bfc227b2615c09ca42a"
      }]
    },
    "post": {
      "name": "Title test4",
      "description": "description test4",
      "price": 10,
      "defaultImageUrl": "http://192.168.120.157/images/postcard.jpg",
      "id": "5818540d463f470edf7c1313",
      "memberId": "58116bfc227b2615c09ca42a",
      "countLike": 0,
      "isLiked": false,
      "countComment": 0,
      "countShare": 0
    }
  }
];

class PostHighlight extends Component {

    render() {
        return (


      <div style={{height:300,margin:20}}>
          <ReactBoostrapCarousel animation={true} className="carousel-fade">

            {data.map(function(item, i) {

                        if(item.group){
                          return <div style={{height:350,width:"100%"}}><GroupCard data={item} pstion={"NULL"}/></div>;
                        }else if (item.post) {
                           return <div style={{height:350,width:"100%"}}><PostCard data={item} pstion={"NULL"}/></div>;
                        }
                        else {
                            return <div style={{height:350,width:"100%"}}><div>null</div></div>;
                        }

               }, this)}
          </ReactBoostrapCarousel>
        </div>


            );
    }
}

export default PostHighlight;
