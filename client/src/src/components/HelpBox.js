import React, {Component} from "react";
import '../css/helpbox.css';
// import {Panel,Accordion} from "react-bootstrap";
import HelpCard from "./HelpCard";

var elements = [{
  topic: ' Lorem ipsum',
  data: [{
    title: 'Lorem ipsum dolor sit amet?',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu hendrerit sem. Vivamus euismod hendrerit purus'
  }, {
    title: 'Lorem ipsum dolor sit amet?',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu hendrerit sem. Vivamus euismod hendrerit purus'
  }, {
    title: 'Lorem ipsum dolor sit amet?',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu hendrerit sem. Vivamus euismod hendrerit purus'
  }, {
    title: 'Lorem ipsum dolor sit amet?',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu hendrerit sem. Vivamus euismod hendrerit purus'
  }]
}, {
  topic: 'Lorem ipsum',
  data: [{
    title: 'Lorem ipsum dolor sit amet?',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu hendrerit sem. Vivamus euismod hendrerit purus'
  }, {
    title: 'Lorem ipsum dolor sit amet?',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu hendrerit sem. Vivamus euismod hendrerit purus'
  }, {
    title: 'Lorem ipsum dolor sit amet?',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu hendrerit sem. Vivamus euismod hendrerit purus'
  }, {
    title: 'Lorem ipsum dolor sit amet?',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu hendrerit sem. Vivamus euismod hendrerit purus'
  }]
}];




class HelpBox extends Component{
  render(){
    return (
    <div className="container helpbox-card">
    <div className="col-sm-10 offset-md-1">
     <h5>Help | <small>Lorem ipsum dolor sit amet</small></h5>
    </div>
     <div className='helpcard-card'>
      {elements.map(function(val,i){
         return(
          <HelpCard topic={val.topic} data={val.data} a1={i}/>);
          },this)}
    </div>
    </div>
  );
 }
}
export default HelpBox;
