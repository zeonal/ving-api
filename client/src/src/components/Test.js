import React, {Component} from "react";

import VingLoginActionCreators from '../actions/VingLoginActionCreators';
import MemberStore from '../stores/MemberStore';
import {browserHistory} from 'react-router';


function getStateFromStores() {
  return {
    member: MemberStore.getMember(),
    isLogin: MemberStore.isLogin(),
    username: '',
    password: ''
  };
}

VingLoginActionCreators.initMember();


class Test extends Component{
  constructor(props) {
    super(props);
    this.state = getStateFromStores();

    this._onClickLoginEmail = this._onClickLoginEmail.bind(this);
    this._onChange = this._onChange.bind(this);
  }

  componentDidMount() {
    MemberStore.addChangeListener(this._onChange);

    if (this.state.isLogin) {
      browserHistory.push('/home');
    }
  }

  componentWillUnmount() {
    MemberStore.removeChangeListener(this._onChange);
  }

	render(){
    const inputStyle = {
      backgroundColor: 'rgba(255,255,255,0.5)',
      border: '0',
      color: 'white'
    };

		return (
      <div style={{
          width: '100vw',
          height: '100vh',
          backgroundSize: 'cover',
          backgroundImage: 'url(https://images.unsplash.com/photo-1437419764061-2473afe69fc2?dpr=1&auto=format&fit=crop&w=1500&h=844&q=80&cs=tinysrgb&crop=)',
        }}>
        <div className="container">
          <div className="row">
            <div className="col-md-6 offset-md-6">
              <form onSubmit={this._onClickLoginEmail}>
                <p><label>Email:</label>
                    <input type="text" onChange={(event) =>
                        this._handleField('email', event)} style={inputStyle}/></p>
                <p><label >Password:</label>
                  <input type="password" className="password" name="password"
                    onChange={(event) => this._handleField('password', event)}
                    style={inputStyle}/>
                </p>
                <button className="btn" onClick={this._onClickLoginEmail}>
                  Login</button>
              </form>

            </div>
          </div>
        </div>
      </div>
		);
	}

  _handleField(fieldName, event) {
    let newState = {};
    newState[fieldName] = event.target.value;
    this.setState(newState);
    this._onChange = this._onChange.bind(this);
  }

  _onChange() {
    console.log('yoyo');
    this.setState(getStateFromStores());
    if (this.state.isLogin) {
      console.log(this.state);
      browserHistory.push('/home');
    }
  }

  _onClickLoginEmail(e) {
    e.preventDefault();
    VingLoginActionCreators.loginWithEmail(
      this.state.email, this.state.password);
  }
}

export default Test;
