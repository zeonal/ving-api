import React, {Component} from 'react';

class SettingBox extends Component {
  render() {
    return (
      <div className="container" style={{
        backgroundColor: '#F2F4F5'
      }}>
        <div className="row">
          <div className="col-lg-12 col-md-12 col-xs-12">
            <div className="col-lg-3 col-md-3 col-xs-5" style={{
              marginTop: '20px',
              marginBottom: '10px',
              paddingTop: '10px',
              paddingBottom: '10px',
              paddingLeft: '30px',
              borderRight: '1px solid #666',
              float: 'left'
            }}>
              :) Settings
            </div>
            <div style={{
              marginTop: '20px',
              marginBottom: '10px',
              marginLeft: '20px',
              paddingTop: '10px',
              paddingBottom: '10px',
              float: 'left'
            }}>
              Update Yourself
            </div>

          </div>
        </div> {/* row */}
        <div className="row">
          <div className="col-lg-12 col-md-12 col-xs-12" style={{
            backgroundColor: 'white',
            padding: '20px'
          }}>

            <h4 style={{color: '#ccc'}}>Account</h4>
            <div>
              <div className="form-group row">
                <label htmlFor="example-text-input" className="offset-xs-1 col-xs-2 col-form-label">
                  Email
                </label>
                <div className="offset-xs-2 col-xs-6">
                  <input className="form-control" type="text" defaultValue="Artisanal kale" id="example-text-input"/>
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="example-search-input" className="offset-xs-1 col-xs-2 col-form-label">
                  Password
                </label>
                <div className="offset-xs-2 col-xs-6">
                  <input className="form-control" type="password" defaultValue="How do I shoot web" id="example-search-input"/>
                </div>

              </div>
              <div className="float-xs-right" style={{marginRight: '10vw'}}>
                <a href="#">Change Password</a>
              </div>
            </div>

            <h4 style={{color: '#ccc', marginTop:'50px'}}>Connect Social Account</h4>
            <div>
              <div className="form-group row">
                <label htmlFor="example-text-input" className="offset-xs-1 col-xs-2 col-form-label">
                  Facebook
                </label>
                <div className="offset-xs-2 col-xs-6">
                  <input className="form-control" type="button" defaultValue="Facebook" id="example-text-input"/>
                </div>
              </div>
            </div>

          </div>
        </div> {/* row */}
        <div className="row">
          <div className="col-lg-12 col-md-12 col-xs-12" style={{
            backgroundColor: 'white',
            padding: '20px'
          }}>

            <h4 style={{color: '#ccc'}}>Notification</h4>
            <div>
              <div className="form-group row">
                <label htmlFor="example-text-input" className="offset-xs-1 col-xs-2 col-form-label">
                  Follow Notification
                </label>
                <div className="offset-xs-2 col-xs-6">
                  <input class="form-check-input" type="checkbox" id="blankCheckbox" value="option1" aria-label="..." />
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="example-text-input" className="offset-xs-1 col-xs-2 col-form-label">
                  Post Notification
                </label>
                <div className="offset-xs-2 col-xs-6">
                  <input class="form-check-input" type="checkbox" id="blankCheckbox" value="option1" aria-label="..." />
                </div>
              </div>
            </div>
          </div>
        </div> {/* row */}
        <div className="row">
          <div className="col-lg-12 col-md-12 col-xs-12" style={{
            backgroundColor: 'white',
            padding: '20px'
          }}>

            <h4 style={{color: '#ccc'}}>Block List</h4>
            <div className="col-xs-12">
              {[0, 1, 2, 3, 4, 5, 6].map((item, index) => {
                  return (
              <div className="form-group row">
                <div className="offset-xs-1 col-xs-2 col-form-label">
                  <img alt="Block Member" className="rounded-circle" style={{marginRight: '10px'}} src={'https://placeholdit.imgix.net/~text?txtsize=33&txt=Profile&w=50&h=50'} />
                    Name
                </div>
                <div className="offset-xs-5 ">
                  <button className="btn btn-primary" style={{marginTop: '10px'}}>Unblock</button>
                </div>
              </div>
            );
        })
        }
            </div>
          </div>
        </div> {/* row */}
        <div className="row" style={{paddingBottom: '50px'}}>
          <div  className="offset-sm-3 col-lg-6 col-md-6 col-xs-12">
                <button className="btn btn-primary col-sm-4">Save Changes</button> <button className="btn btn-primary col-sm-4" style={{marginLeft: '20px'}}>Discard</button>
              </div>


        </div> {/* row */}
      </div>
    );
  }
}

export default SettingBox;
