import React, {Component} from 'react';
// import PostCard from './PostCard';
// import GroupCard from './GroupCard';

import VingLoginActionCreators from '../actions/VingLoginActionCreators';
import VingAPIActionCreators from '../actions/VingAPIActionCreators';
import MemberStore from '../stores/MemberStore';
import PostStore from '../stores/PostStore';
import {browserHistory} from 'react-router';

function getStateFromStores() {
  return {
    member: MemberStore.getMember(),
    isLogin: MemberStore.isLogin(),
    currentPost: PostStore.getCurrentPost(),
  };
}

class Home extends Component {

  constructor(props) {
    super(props);
    this.state = getStateFromStores();
    this._onChange = this._onChange.bind(this);
    this._createPost = this._createPost.bind(this);
    this._logout = this._logout.bind(this);
  }

  componentDidMount() {
    MemberStore.addChangeListener(this._onChange);
    PostStore.addChangeListener(this._onChange);
    if (!this.state.isLogin) {
      browserHistory.push('/test');
    }
  }

  componentWillUnmount() {
    MemberStore.removeChangeListener(this._onChange);
    PostStore.removeChangeListener(this._onChange);
  }

  render() {
    return (
      <div style={{
          width: '100vw',
          height: '100vh',
          backgroundSize: 'cover',
          backgroundImage: 'url(https://images.unsplash.com/photo-1437419764061-2473afe69fc2?dpr=1&auto=format&fit=crop&w=1500&h=844&q=80&cs=tinysrgb&crop=)',
        }}>
        <nav className="navbar bg-faded" style={{
            backgroundColor: 'rgba(21,31,40,0.5)',
          }}>
          <a className="navbar-brand" href="#" style={{color: 'white',}}>Ving</a>
          <ul className="nav navbar-nav float-xs-right">
            <li className="nav-item">{this.state.member.email}</li>
            <li className="nav-item">
              <a className="nav-link" href="#" onClick={this._logout} style={{
                  color: 'white',
                  textDecoration: 'underline',
                }}>Logout</a>
            </li>
          </ul>


        </nav>
        <div className="row" style={{
          "marginRight": "3px",
          "marginLeft": "3px",
          color: 'white',
        }}>
          <div className="col-sm-6 offset-sm-6 col-xs-12">

            <button className="btn" onClick={this._createPost}>Create Post</button>

          </div>
        </div>
      </div>
    );
  }

  _onChange() {
    this.setState(getStateFromStores());
    if (this.state.currentPost) {

    }
    if (!this.state.isLogin) {
      browserHistory.push('/test');
    }
  }

  _createPost() {
    VingAPIActionCreators.createPost(this.state.member.token.id, this.state.member.id);
  }

  _logout() {
    console.log('yoyo');
    VingLoginActionCreators.logout(this.state.member.token.id);
  }
}

export default Home;
