import React, {Component} from 'react';
import '../css/memberSuggestion.css';

class MemberSuggestionCard extends Component {
  render() {
    console.log(this.props.item.member);

    return (
      <div className="flex-item mycard" style={{
        backgroundImage: this.props.item.member ? 'url(http://localhost:3000/' + this.props.item.member.profileImage[0].url + ')': ''
      }}>
        <div className="flex-container inner-card">
          <div className="flex-item inner-card" style={{
            paddingBottom: "5px"
          }}>
            <p style={{
              paddingBottom: "5px",
              margin: "0px"
            }}>
              <span style={{
                fontSize: "1.1rem"
              }}>{this.props.item.member ? this.props.item.member.displayName: 'Name'}</span>
              <span style={{
                fontSize: "0.8rem"
              }}>Lorem ipsum</span>
            </p>
            <button type="button" className="btn btn-primary btn-sm" style={{
              backgroundColor: "#9C9C9C",
              borderColor: "#9C9C9C",
              maxWidth: "100%"
            }}>Follow</button>
          </div>
        </div>
      </div>
    );
  }
}

export default MemberSuggestionCard;
