import React, { Component } from 'react';
import PageView from './PageView';
import clamp from 'lodash/clamp';
// import {Motion, spring} from 'react-motion';
// import {Draggable, Holdable, Swipeable, CustomGesture, moves} from 'react-touch';
import {Swipeable} from 'react-touch';



import '../page.css';

let accessToken = '';
const hostserver = '192.168.120.157'

class PostView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            postId:this.props.location.query.postId,
            index: 0,
            left: 0,
            top: 0,
            stations: [],
            css: "",
            prefix: "",
            pageId:[],
            isLoaded: false
        }
        accessToken  = this.props.location.query.access_token;
    }


    componentDidMount(){
    //get API Post on load
      fetch(`http://${hostserver}:3000/api/Posts/detail?access_token=${accessToken}`,
            {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    postId: this.state.postId
                })
            })
            .then((response) => {
                return response.json()
            })
            .then((json) => {
                console.log('on load Posts',json.page);
                // pageId = json.page;
                 this.setState({
                      pageId: json.page,
                      isLoaded: true
                    });
                    //get API page on load
                    fetch(`http://${hostserver}:3000/api/Pages/detail?access_token=${accessToken}`,
                        {
                            method: "POST",
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({
                                pageId: this.state.pageId[this.state.index].id
                            })
                        })
                        .then((response) => {
                            return response.json()
                        })
                        .then((json) => {
                            console.log('on load page',json.element, json.css)
                            this.setState({
                                stations: json.element,
                                css: json.css,
                                prefix:json.prefix
                            });
                            this.render();
                        }).catch(function(ex) {
                        console.log('parsing failed', ex)
                    });

            }).catch(function(ex) {
            console.log('parsing failed', ex)
        });
    }

    _onSwipe(increment){
        const currentPage = clamp(this.state.index + increment, 0, this.state.pageId.length - 1);
        this.setState({index:currentPage});

        //get API page on Swipe
        fetch(`http://${hostserver}:3000/api/Pages/detail?access_token=${accessToken}`,
            {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    pageId: this.state.pageId[this.state.index].id
                })
            })
            .then((response) => {
                return response.json()
            })
            .then((json) => {
                // console.log(json.element, json.css)
                this.setState({
                    stations: json.element,
                    css: json.css,
                    prefix:json.prefix
                });
            }).catch(function(ex) {
            console.log('parsing failed', ex);
        });
    }




    render() {
          if (!this.state.isLoaded) {
            return (<div>Loading</div>);

        }

        return (
            <Swipeable onSwipeLeft={() => this._onSwipe(1)} onSwipeRight={() => this._onSwipe(-1)}>
                <div className="flex-boxes" id="myDiv">
                    <PageView ref="pageView" pageId={this.state.pageId[this.state.index].id} stations={this.state.stations} css={this.state.css} prefix={this.state.prefix}/> 
                </div>
            </Swipeable>
            );
    }
}
export default PostView;
