import React, {Component} from 'react';
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';

const src = 'http://fengyuanchen.github.io/cropper/img/picture.jpg';
// let accessToken = '';
const hostserver = '192.168.120.157'

function gcd(a, b) {
  return (b === 0)
    ? a
    : gcd(b, a % b);
}

var fileVideo;
class CropImage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      src,
      cropResult: null,
      video: "",
      showImg: false,
      showVideo: false,
      ratio: this.props.ratio
    };
    this.cropImage = this.cropImage.bind(this);
    this.onChange = this.onChange.bind(this);
    this.useDefaultImage = this.useDefaultImage.bind(this);
  }
  accessToken = this.props.access_token;
  hostserver = this.props.hostserver;

  _ratio_w(url) {
    let image = new Image();
    image.src = url;
    let w = image.width;
    let h = image.height;
    let g = gcd(w, h);
    return (w / g);
  }

  _ratio_h(url) {
    let image = new Image();
    image.src = url;
    let w = image.width;
    let h = image.height;
    let g = gcd(w, h);
    return (h / g);
  }

  _isVideo(filename) {
    switch (filename[0].type) {
      case 'video/m4v':
      case 'video/avi':
      case 'video/mpg':
      case "video/mp4":
        return true;
      default:
        return false;
    }
  }
  _isImage(filename) {
    switch (filename[0].type) {
      case 'image/png':
      case 'image/jpeg':
      case 'image/gif':
        return true;
      default:
        return false;

    }
  }

  onChange(e) {
    e.preventDefault();
    let files;
    if (e.dataTransfer) {
      files = e.dataTransfer.files;
    } else if (e.target) {
      files = e.target.files;
    }
    console.log(this._isVideo(files)
      ? 'isVideo'
      : 'isImg');
    if (this._isVideo(files)) {

      this.setState({showImg: false});
      this.setState({showVideo: true});
      // console.log('isVideo',files[0]);
      let objectURL = URL.createObjectURL(files[0]);
      console.log('objectURL', objectURL);
      fileVideo = files[0]
      this.refs.previewvideo.src = objectURL

    } else if (this._isImage(files)) {

      this.setState({showVideo: false});
      this.setState({showImg: true});

      const reader = new FileReader();
      reader.onload = () => {
        this.setState({src: reader.result});
      };
      reader.readAsDataURL(files[0]);
    } else {
      return;
    }

  }

  cropImage() {
    if (typeof this.cropper.getCroppedCanvas() === 'undefined') {
      return;
    }

    // Set this Global for Set props.img
    var _this = this;
    // Upload cropped image to server if the browser supports `HTMLCanvasElement.toBlob`
    this.cropper.getCroppedCanvas().toBlob(function(blob) {

      //create form uplad img
      let form = new FormData();
      form.append("type", "profile");
      form.append('file', blob);

      //Upload img Use `reactjs.fetch` method
      fetch(`http://${hostserver}:3000/uploadapi?access_token=${_this.props.access_token}`, {
        method: "POST",
        "mimeType": "multipart/form-data",
        body: form
      }).then((response) => {
        return response.json()
      }).then((json) => {
        console.log(json.url)
        //call function setimg in modal2 for setimg
        _this.props.img(`http://${hostserver}:3000${json.url}`);
        _this.props.modals.hide();
        return;

      }).catch(function(ex) {
        console.log('parsing failed', ex)
      });
    });

  }

  useDefaultImage() {
    this.setState({src: src});
  }

  // play video toggle
  _videoPlay(event) {
    if (event.target.paused) {
      event.target.play();
    } else {
      event.target.pause();
    }
  }

  upVideo() {
    if (typeof fileVideo === 'undefined') {
      return;
    }
    console.log('Upload Video', this.refs.fcrop.files[0])
    // if(this.state.showVideo){
    //  console.log('Upload Video',fileVideo,this.refs.fcrop.files[0])
    // }
    let form = new FormData();
    form.append("type", "page");
    form.append('file', this.refs.fcrop.files[0]);

    fetch(`http://${hostserver}:3000/uploadapi?access_token=${this.props.access_token}`, {
      method: "POST",
      "mimeType": "multipart/form-data",
      body: form
    }).then((response) => {
      return response.json()
    }).then((json) => {
      console.log(json.url)
      //call function setimg in modal2 for setimg
      this.props.video(`http://${hostserver}:3000${json.url}`);
      this.props.modals.hide();
      return;

    }).catch(function(ex) {
      console.log('parsing failed', ex)
    });

  }

  render() {
    return (
      <div style={{
        width: '100%',
        float: 'none',
        'text-align': 'center'
      }}>
        <input type="file" ref="fcrop" onChange={this.onChange}/>
        <div ref='divcrop' style={{
          width: '100%',
          display: this.state.showImg
            ? 'block'
            : 'none'
        }}>

          <button onClick={this.useDefaultImage} style={{
            display: 'none'
          }}>Use default img</button>
          <br/>
          <br/>
          <Cropper style={{
            height: 200,
            width: '100%'
          }} aspectRatio={(this._ratio_w(this.props.data.stations[this.props.data.position].data)) / (this._ratio_h(this.props.data.stations[this.props.data.position].data))} preview=".img-preview" guides={false} src={this.state.src} ref={cropper => {
            this.cropper = cropper;
          }}/>
          <button className="btn btn-success" onClick={this.cropImage} style={{
            float: 'none'
          }}>
            Crop Image
          </button>
        </div>
        <div style={{
          display: 'none'
        }}>
          <div className="box" style={{
            width: '100%',
            float: 'none',
            'text-align': 'center'
          }}>
            <h1>Preview</h1>

            <div className="img-preview" style={{
              width: '100%',
              float: 'none',
              'height': '300',
              'overflow': 'hidden'
            }}/>

          </div>
          <div className="box" style={{
            width: '100%',
            float: 'none',
            display: 'none'
          }}>
            <img style={{
              width: '100%',
              display: 'none'
            }} src={this.state.cropResult} alt="cropped preview" />
          </div>
        </div>
        <video controls ref="previewvideo" onClick={(event) => this._videoPlay(event)} style={{
          width: '100%',
          float: 'none',
          'text-align': 'center',
          display: this.state.showVideo
            ? 'block'
            : 'none'
        }}>
          <source src={this.state.video} type="video/mp4"/>
        </video>
        <button className="btn btn-success" onClick={(event) => this.upVideo(event)} style={{
          float: 'none',
          display: this.state.showVideo
            ? 'block'
            : 'none'
        }}>
          Upload Video
        </button>
        <br style={{
          clear: 'both'
        }}/>
      </div>
    );
  }
}
export default CropImage;
