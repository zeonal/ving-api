import React, {Component} from "react";
import '../css/helpbox.css';
import {Panel,Accordion} from "react-bootstrap";



class HelpCard extends Component{
  render(){
    return (
     <div className="col-sm-10 offset-md-1 helpcard-topic">
     <h5>{this.props.topic}</h5>
      <Accordion>
          {this.props.data.map(function(item,index){
              return(
                <Panel style={{'padding-top': '10px'}} header={item.title} eventKey={this.props.a1+'-'+index}>
                <label className='helpcard-content'>{item.content}</label>
                </Panel>
              );
          },this)}
       </Accordion>
      </div>
    );
  }
}
export default HelpCard;
