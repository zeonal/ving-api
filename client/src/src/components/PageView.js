import React, { Component } from 'react';

import '../ving.css';

class PageView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            stations: this.props.stations,
            pageId:this.props.pageId,
            css: this.props.css,
            index: 0
        };
    }
    
    render() {
        return (
               
            <div className="page flex-box flex-box-big slideInRigh" style={{"width": "100vw", 'height': '100vh','overflow': 'hidden', "position": "relative","margin": "0px 0px 0px 0px", "padding": "0px 0px 0px 0px"}}>
            <style>    
              {this.props.css}
            </style>           
            <div class="hiddenfile" style={{"width":"0px","height":"0px"}}>
              <input ref="fileinput" name="upload" type="file" id="fileinput"/>
            </div>
              <meta id="meta-description" name="description" content={typeof this.props.stations[2]=== 'undefined'?"":this.props.stations[2].data} />
              <meta id="og-title" property="og:title" content={typeof this.props.stations[1]!== 'undefined'?this.props.stations[1].data:""} />
              <meta id="og-image" property="og:image" content={typeof this.props.stations[0]!== 'undefined'?this.props.stations[0].data:""} />
             {this.props.stations.map(function(item, i) {
                return (
                    <Element type={item.type} data={item.data} stations={this.props.stations} prefix={this.props.prefix} e1={i} action={(data) => this.action(data)} />
                    );
            }, this)}
          </div>
        );
    }
}

//element PageView
class Element extends Component {
  
  // play video toggle 
  _videoPlay(event){ 
    if(event.target.paused){
      event.target.play();
    }else{
      event.target.pause();
    }
  }
    render() {
        return (
            <div className={this.props.prefix+this.props.e1 + " edit"}> 
            {(() => {
                //create element by type
                switch (this.props.type) {
                case "img":
                    return <img src={this.props.data} alt="img" />;
                case "video":
                    return (<video controls onClick={(event) => this._videoPlay(event)}>
                              <source src={this.props.data} type="video/mp4"/>
                            </video>);
                case "text":
                    return <p>{this.props.data}</p>;
                default:
                    return <p></p>;
                }
            })()}
        </div>
            );
    }
}

export default PageView; 