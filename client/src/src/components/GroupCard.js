import React, {Component} from 'react';
import classNames from 'classnames';

let cardpost = classNames('card', 'card-inverse');
let minheight = "235px";
function getpostheight(position) {
  if (position === 1 || position === 0) {
    return "500px";
  } else if (position === 2 || position === 3) {
    return "350px";
  } else {
    return "250px";
  }
}
function getposttop(position) {
  let ch = parseInt(position % 8, 10);  // Base 10
  let x = parseInt(position / 8, 10);   // Base 10
  let top = (x * (-1 * 250));
  if (ch === 4 || ch === 5) {
    top -= 150;
    return top + "px";
  } else if (ch === 0) {
    // top = top - 0;
    return top + "px";
  } else {
    // top = top - 0;
    return top + "px";
  }
}
class GroupCard extends Component {

  render() {
    if (this.props.pstion === "NULL") {
      cardpost = classNames('card', 'card-inverse', 'col-sm-12', 'carousel-item');
      minheight = "350px";
    } else if ((this.props.pstion % 8) < 4) {
      cardpost = classNames('card', 'card-inverse', 'col-sm-6');
    } else {
      cardpost = classNames('card', 'card-inverse', 'col-sm-3');
    }

    return (

      <div className={cardpost} style={{
        "height": getpostheight(this.props.pstion % 8),
        "top": getposttop(this.props.pstion % 8),
        "border": "3px solid #FFF",
        "border-radius": "15px",
        "margin": "0px",
        "overflow": "hidden",
        "min-height": minheight,
        "mix-height": "auto",
        "background": "url('" + this.props.data.group.post[0].defaultImageUrl + "') no-repeat",
        "background-size": "cover",
        "background-repeat": "no-repeat",
        "background-position": "center top"
      }}>
        {this.props.data.group.post.map(function(item, i) {
          if (i < 4) {
            return (
              <div style={{
                "position": "absolute",
                "height": "100%",
                "width": "100%",
                "left": i*25 + "%"
              }}><img alt="post's cover" style={{
                "position": "absolute",
                "height": "100%"
              }} src={item.defaultImageUrl}/></div>
            );
          } else {
            return null;
          }
        }, this)}

        <div className="card-img-overlay">
          <h4 className="card-title" style={{
            "position": "absolute",
            "bottom": "85px",
            "width": "90%",
            "height": "30px",
            "overflow": "hidden",
            "margin": "0px"
          }}>{this.props.data.group.name}</h4>
          <p className="card-text " style={{
            "position": "absolute",
            "bottom": "55px",
            "width": "90%",
            "height": "22px",
            "overflow": "hidden"
          }}>{this.props.data.group.description}</p>
          <p className="card-text" style={{
            "position": "absolute",
            "bottom": "10px",
            "width": "90%"
          }}>
            <img alt="avatar" className="img-responsive img-circle" src={this.props.data.member.profileImage[0].url} style={{
              width: '60px',
              height: '60px',
              "border-radius": "30px",
              "vertical-align": "bottom"
            }}/>
            <span className="card-text" style={{
              "top": "7px",
              left: "65px",
              "position": "absolute",
              "font-size": "23px"
            }}>{this.props.data.member.displayName}</span>
            <span className="text-muted" style={{
              "top": "36px",
              left: "65px",
              "position": "absolute",
              "font-size": "11px"
            }}>Last updated 2hrs ago</span>
          </p>
        </div>
      </div>

    );
  }
}

export default GroupCard;
