import React, {Component} from 'react';
import '../css/memberSuggestion.css';
import MemberSuggestionCard from './MemberSuggestionCard';

import VingAPIActionCreators from '../actions/VingAPIActionCreators';
import MemberStore from '../stores/MemberStore';
import SuggestionStore from '../stores/SuggestionStore';
import {browserHistory} from 'react-router';

function getStateFromStores() {
  return {member: MemberStore.getMember(), isLogin: MemberStore.isLogin(), suggestionMembers: SuggestionStore.getMembers()};
}

class MemberSuggestionBox extends Component {
  constructor(props) {
    super(props);
    this.state = getStateFromStores();
    this._onChange = this._onChange.bind(this);
  }

  componentDidMount() {
    MemberStore.addChangeListener(this._onChange);
    SuggestionStore.addChangeListener(this._onChange);
    if (!this.state.isLogin) {
      browserHistory.push('/test');
    } else {
      VingAPIActionCreators.initSuggestionMembers(this.state.member.token.id);
    }
  }

  componentWillUnmount() {
    MemberStore.removeChangeListener(this._onChange);
    SuggestionStore.removeChangeListener(this._onChange);
  }

  render() {
    return (
      <div className="flex-container text-xs-center one">
        <div className="flex-item one">
          <h1 style={{
            color: "#EEEEEE"
          }}>Follow Suggestion</h1>
        </div>
        <div className="flex-item one">
          <p style={{
            color: "#EEEEEE"
          }}>We suggest you to check them out. Choose the ones you would like to follow</p>
        </div>
        <div className="flex-item one">
          <div className="flex-container mycard">
            {this.state.suggestionMembers.map(function(item, index) {
              if (item.member) {
                return (<MemberSuggestionCard key={item.id} item={item}/>)
              } else {
                return null;
              }

            }, this)
}
          </div>
        </div>
        <div className="flex-item one">
          <button type="button" className="btn btn-primary" style={{
            backgroundColor: "#424242",
            borderColor: "#424242"
          }} onClick={this._onClickStart}>Get Started</button>
        </div>

      </div>
    );
  }

  _onChange() {
    this.setState(getStateFromStores());
  }

  _onClickStart() {
    browserHistory.push('/Feed');
  }
}

export default MemberSuggestionBox;
