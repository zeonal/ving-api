import React, {
    Component
} from 'react';
import SubscribeCard from './SubscribeCard';
import '../css/subscribe.css';
var data = [{
    id: 1,
    name: "topic1",
    url:"http://www.w3schools.com/css/img_fjords.jpg",
     "create_at": "2016-10-06T10:27:04.036Z",
    content: " Description learn more test : Curabitur blandit tempus porttitor. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulumDescription learn more test : Curabitur blandit tempus porttitor. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulumDescription learn more test : Curabitur blandit tempus porttitor. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum",
    member: {
    	"url":"http://www.jqueryscript.net/images/Simplest-Responsive-jQuery-Image-Lightbox-Plugin-simple-lightbox.jpg",
        "displayName": "Alex Patrix",
        "id": "57f626f868de7b086dd499c4"
    }
},{
    id: 2,
    url:"http://www.w3schools.com/css/img_fjords.jpg",
    name: "topic2",
    content: "Description learn more test : Curabitur blandit tempus porttitor. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.",
     "create_at": "2016-10-06T10:27:04.036Z",
    member: {
    	"url":" http://www.jqueryscript.net/images/Simplest-Responsive-jQuery-Image-Lightbox-Plugin-simple-lightbox.jpg",
        "displayName": "Alex Patrix",
        "create_at": "2016-10-06T10:27:04.036Z",
        "id": "57f626f868de7b086dd499c4"
    }
},{
    id: 3,
    name: "topic3",
    url:"http://www.w3schools.com/css/img_fjords.jpg",
    content: "Description learn more test : Curabitur blandit tempus porttitor. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.",
     "create_at": "2016-10-06T10:27:04.036Z",
    member: {
    	"url":"http://www.jqueryscript.net/images/Simplest-Responsive-jQuery-Image-Lightbox-Plugin-simple-lightbox.jpg",
        "displayName": "Alex Patrix",
        "create_at": "2016-10-06T10:27:04.036Z",
        "id": "57f626f868de7b086dd499c4"
    }
},{
    id: 4,
    name: "topic4",
    url:"http://www.w3schools.com/css/img_fjords.jpg",
    content: "Description learn more test : Curabitur blandit tempus porttitor. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.",
     "create_at": "2016-10-06T10:27:04.036Z",
    member: {
    	"url":"http://www.jqueryscript.net/images/Simplest-Responsive-jQuery-Image-Lightbox-Plugin-simple-lightbox.jpg",
        "displayName": "Alex Patrix",
        "create_at": "2016-10-06T10:27:04.036Z",
        "id": "57f626f868de7b086dd499c4"
    }
}];

class SubscribeBox extends Component {
    render() {
        return (
        
          <div className="container">
           <div className="row" style={{'margin':'100px'}}>
            <p>My Subscribe | <small>Lorem ipsum dolor sit amet</small></p>
           
        	{
        	  data.map(function(item,index){ 
        		return(
        			<SubscribeCard item={item}/>
        			)
        		},this)}
            </div>
          </div>
        );
    }
}
export default SubscribeBox;