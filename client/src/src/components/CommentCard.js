import React, { Component } from 'react';

class CommentCard extends Component {

    render() {
      return (
        <div className='CommentCard' >
          <div className="media mediaCommentCard" style={{}}>
            <div className="media-body">
          <div className="media mt-2">
            <a className="media-left" href="#">
              <img
                  alt="avatar"
                  className="media-object rounded-circle"
                  src={this.props.item.image.url}
                  data-holder-rendered="true"
                  style={{width: 40, height: 40}} />
            </a>
            <div className="media-body">
              <div className="CommentCardHeader">
                <h6 className="media-heading">
                  {this.props.item.name}
                </h6>
              {this.props.item.time}
              </div>

            </div>

        </div>
          {this.props.item.comment}
        </div>
      </div>
          <div className="icon-comment-center col-centered" style={{'text-align':'center',padding:'5px 5px',backgroundColor:"rgb(185, 183, 183)"}}>
              <div style={{fontSize:'0.7em'}}>
                <img
                  alt="avatar"
                  className=" rounded-circle"
                  src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2264%22%20height%3D%2264%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2064%2064%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_158518d07bc%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_158518d07bc%22%3E%3Crect%20width%3D%2264%22%20height%3D%2264%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2214%22%20y%3D%2236.5%22%3E64x64%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                  data-holder-rendered="true"
                  align="middle"
                  style={{width: 40, height: 40,margin:'0 auto',verticalAlign:'middle'}} />
                <span style={{marginLeft:'5px'}}>123</span>

              </div>
          </div>
        </div>
      );
    }
}
export default CommentCard;
