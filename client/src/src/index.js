import React from 'react';
import ReactDOM from 'react-dom';

// import './index.css';

import VingAPIUtils from './utils/VingAPIUtils';
import { Router, Route, browserHistory } from 'react-router';


// Add components here
import App from './App';
import InterestBox from './components/InterestBox';
import MemberSuggestionBox from './components/MemberSuggestionBox';
import Viewer from './components/others/Viewer';

import Feed from './components/Feed';
import SubscribeBox from './components/SubscribeBox';
import SettingBox from './components/SettingBox';
import VingPage from './components/login/VingPage';
import RegisterForm from './components/register/RegisterForm';
import HelpBox from './components/HelpBox';
import Nav from './components/Nav';
import Modal from './components/Modal';
import PostHighlight from './components/PostHighlight';
import MemberCover from './components/MemberCover';
import PostView from './components/PostView';
import Test from './components/Test';
import PostEdit from './components/PostEdit';
import CreatePost from './components/CreatePost';
import CloseButton from './components/CloseButton';
import Home from './components/Home';
import Editor from './components/Editor';



VingAPIUtils.initFB();

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/" component={App}/>
    <Route path="viewer" component={Viewer}/>
    <Route path="view" component={Viewer}/>
    <Route path="setting" component={SettingBox}/>
    <Route path="Feed" component={Feed}/>
    <Route path="PostHighlight" component={PostHighlight}/>
    <Route path="interest" component={InterestBox}/>
    <Route path="memberSuggestion" component={MemberSuggestionBox}/>
    <Route path="SubscribeBox" component={SubscribeBox}/>
    <Route path="login" component={VingPage} />
    <Route path="register" component={RegisterForm} />
    <Route path="HelpBox" component={HelpBox}/>
    <Route path="Nav" component={Nav}/>
    <Route path="Modal" component={Modal}/>
    <Route path="MemberCover" component={MemberCover}/>
    <Route path="PostView" component={PostView}/>
    <Route path="test" component={Test}/>
    <Route path="PostEdit" component={PostEdit}/>
    <Route path="CreatePost" component={CreatePost}/>
    <Route path="CloseButton" component={CloseButton}/>
    <Route path="home" component={Home}/>
    <Route path="editor" component={Editor}/>
  </Router>,
  document.getElementById('root')
);
