import React, {
    Component
} from 'react';

import './App.css';
import './css/bootstrap-flex.min.css';
import './css/font-awesome.min.css';
import { Link } from 'react-router';

class App extends Component {
  render() {
    return (
      <div>
        <Link to="/interest">Interest</Link><br />
        <Link to="/memberSuggestion">memberSuggestion</Link><br />
        <Link to="/viewer">Viewer</Link><br />
        <Link to="/SubscribeBox">SubscribeBox</Link><br />
        <Link to="/HelpBox">HelpBox</Link><br />
        <Link to="/setting">Setting</Link><br />
        <Link to="/viewer">Viewer</Link><br />
        <Link to="/Feed">Feed</Link><br />
        <Link to="/login">Login</Link><br/>
        <Link to="/register">Register</Link> <br/>
        <Link to="/Nav">Nav</Link><br />
        <Link to="/Modal">Modal</Link><br />
        <Link to="/PostHighlight">Highlight</Link><br />
        <Link to="/MemberCover">MemberCover</Link><br />
        <Link to="/PostView">PostView</Link><br />
        <Link to="/PostEdit">PostEdit</Link><br />
        <Link to="/CreatePost">CreatePost</Link><br />
        <Link to="/CloseButton">CloseButton</Link><br />
      </div>
    );

  }
}

export default App;
