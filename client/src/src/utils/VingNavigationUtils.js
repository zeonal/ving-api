// import VingConstants from '../constants/VingConstants';
import React from 'react';

import HomeSignup from '../components/HomeSignup';
import Signup from '../components/Signup';
import Feed from '../components/Feed';

module.exports = {

  getComponent: function(componentName) {
    if (componentName === 'HomeSignup') {
      return (<HomeSignup />);
    } else if (componentName === 'Signup') {
      return (<Signup />);
    } else if (componentName === 'Feed') {
      return (<Feed />);
    }
  },
};
