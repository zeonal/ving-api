import VingConstants from '../constants/VingConstants';

function _fetchAPI(url, data, callback) {
  // console.log(url);
  // console.log(data);
  fetch(url, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data)
  })
  .then((response) => response.json())
  .then((responseJson) => {
    // console.log(responseJson);
    if (responseJson.error) {
      return callback(responseJson.error);
    }
    return callback(null, responseJson);
  })
  .catch((error) => {
    console.log(error);
    return callback(error);
  });
};

module.exports = {

  initFB: function() {
    (function(d, s, id) {
      const element = d.getElementsByTagName(s)[0];
      const fjs = element;
      let js = element;
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = '//connect.facebook.net/en_US/sdk.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    window.fbAsyncInit = () => {
      window.FB.init({appId: '998117060253928', xfbml: true, cookie: true, version: 'v2.8'});
    };
  },

  loginWithFB: function(callback) {
    let loginWithFacebookAccessToken = this._loginWithFacebookAccessToken;
    window.FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        console.log(response);
        loginWithFacebookAccessToken(response.authResponse.accessToken, (err, member) => {
          return callback(null, member)
        });
        // window.FB.api('/me?fields=first_name,last_name,picture', (facebookObject) => {
        //   return callback(null, facebookObject);
        // });
      } else {
        window.FB.login((response) => {
          console.log(response);
          loginWithFacebookAccessToken(response.authResponse.accessToken, (err, member) => {
            return callback(null, member)
          });
          // window.FB.api('/me?fields=first_name,last_name,picture', (facebookObject) => {
          //   return callback(null, facebookObject);
          // });
        }, {
          scope: ['public_profile', 'user_friends']
        });
      }
    });
  },

  _loginWithFacebookAccessToken: function(accessToken, callback) {
    _fetchAPI(VingConstants.APIUrl + '/api/Members/loginWithFacebookAccessToken',
    {
      accessToken: accessToken
    }, (err, member) => {
      if (!err) {
        localStorage.setItem('member', JSON.stringify(member));
      }
      return callback(null, member);
    });
  },

  loginWithEmail: function (email, password, callback) {
    _fetchAPI(VingConstants.APIUrl + '/api/Members/login',
    {
      email: email,
      password: password,
    }, (err, member) => {
      if (!err) {
        localStorage.setItem('member', JSON.stringify(member));
      }
      return callback(null, member);
    });
  },

  signup: function (email, password, callback) {
    _fetchAPI(VingConstants.APIUrl + '/api/Members/signup',
    {
      email: email,
      password: password,
    }, (err, member) => {
      if (!err) {
        localStorage.setItem('member', JSON.stringify(member));
      }
      return callback(null, member);
    });
  },

  logout: function(accessToken, callback) {
    if (accessToken) {
      fetch(VingConstants.APIUrl + '/api/Members/logout?access_token=' + accessToken.id, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson);
        localStorage.clear();
        return callback(null);
      })
      .catch((error) => {
        // Logout has no return value.
        // console.log(error);
        localStorage.clear();
        return callback(null);
      });
    }

  },

  getInterest: function (accessToken, callback) {
    _fetchAPI(VingConstants.APIUrl + '/api/Interests/getInterest?access_token=' + accessToken,
    {
    }, (err, result) => {
      if (err) {
        console.log(err);
      }
      return callback(null, result.results);
    });
  },

  getThemes: function (accessToken, callback) {
    _fetchAPI(VingConstants.APIUrl + '/api/Themes/getTheme?access_token=' + accessToken,
    {
    }, (err, response) => {
      if (err) {
        console.log(err);
        return callback(null, []);
      }
      return callback(null, response);
    });
  },

  getTemplates: function (accessToken, themeId, callback) {
    _fetchAPI(VingConstants.APIUrl + '/api/Templates/getTemplate?access_token=' + accessToken,
    {
      themeId: themeId
    }, (err, response) => {
      if (err) {
        console.log(err);
        return callback(null, []);
      }
      return callback(null, response);
    });
  },

  getSuggestionMembers: function (accessToken, callback) {
    _fetchAPI(VingConstants.APIUrl + '/api/Suggestions/getMemberSuggestion?access_token=' + accessToken,
    {
    }, (err, response) => {
      if (err) {
        console.log(err);
      } else {
        console.log(response);
        return callback(null, response.results);
      }

    });
  },

  getFeedByMember: function (accessToken, memberId, callback) {
    _fetchAPI(VingConstants.APIUrl + '/api/Feeds/byMember?access_token=' + accessToken,
    {
      memberId: memberId
    }, (err, response) => {
      if (err) {
        console.log(err);
      } else {
        console.log(response);
        return callback(null, response.results);
      }

    });
  },

  createPost: function (accessToken, memberId, callback) {
    _fetchAPI(VingConstants.APIUrl + '/api/Posts/createPost?access_token=' + accessToken,
    {
      memberId: memberId
    }, (err, response) => {
      if (err) {
        console.log(err);
      } else {
        // console.log(response);
        return callback(null, response);
      }

    });
  },

  getPostDetail: function (accessToken, postId, callback) {
    _fetchAPI(VingConstants.APIUrl + '/api/Posts/detail?access_token=' + accessToken,
    {
      postId: postId
    }, (err, response) => {
      if (err) {
        console.log(err);
        return callback(null, {});
      } else {
        return callback(null, response);
      }

    });
  },

  createPage: function (accessToken, memberId, template, postId, callback) {
    _fetchAPI(VingConstants.APIUrl + '/api/Pages/createPage?access_token=' + accessToken,
    {
      memberId: memberId,
      element: template.element,
      templateId: template.id,
      postId: postId
    }, (err, response) => {
      if (err) {
        console.log(err);
        return callback(null, {});
      } else {
        return callback(null, response);
      }

    });
  },
};
