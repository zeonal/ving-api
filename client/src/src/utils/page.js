modules.export = {
  page: [{
    css: ".elem0 img {display: block;width: 100%;margin: 0 auto 1em;opacity: 1; }.elem1 { font-family: 'Conv_Pattaya-Regular', Sans-Serif; top: 82%;  position: absolute;  z-index: 1000;  color: #FFF;  width: 87%;  font-size: 72px;  transform: rotate(-10deg); }",
    element: [{
        type: 'img',
        data: "http://localhost/testsass/img/template1_01.png"
      },
      {
        type: 'text',
        data: "Good music Good life."
      }
    ]
  }, {
    "css": ".elem2 img {  display: block;  width: 100%;  margin: 0 auto 1em;  opacity: 1; }  .elem3 {  font-family: 'Conv_Pattaya-Regular', Sans-Serif;  top: 82%;  position: absolute;  z-index: 1000;  color: #FFF;  width: 87%;  font-size: 72px;  transform: rotate(-10deg); }",
    "element": [{
        "type": "img",
        "data": "http://localhost/testsass/img/template1_02.png"
      },
      {
        "type": "text",
        "data": "Chapter 1"
      }
    ],
    "postId": "581b11b0c67f4b5005bfcbcd",
    "hidden": "false"
  }, {
    "css": "",
    "element": [{
        "type": "text",
        "data": "Good music Good life."
      },
      {
        "type": "text",
        "data": "ดนตรีเป็นสิ่งที่ธรรมชาติให้มาพร้อม ๆ กับชีวิตมนุษย์โดยที่มนุษย์เองไม่รู้ตัว..."
      },
      {
        "type": "img",
        "data": "http://localhost/testsass/img/template1_03_02.png"
      },
      {
        "type": "text",
        "data": "Good music Good life."
      },
      {
        "type": "text",
        "data": "มีบุคคลจำนวนไม่น้อยที่ตั้งคำถามว่า “ดนตรีคืออะไร” แล้ว “ทำไมต้องมีดนตรี” คำว่า “ดนตรี” ในพจนานุกรม ฉบับราชบัณฑิตยสถาน พ.ศ. 2525 ได้ให้ความหมายไว้ ว่า “เสียงที่ประกอบกันเป็นทำนองเพลง เครื่องบรรเลง ซึ่งมีเสียงดังทำให้รู้สึกเพลิดเพลิน หรือเกิดอารมณ์รัก โศก หรือรื่นเริง”จากความหมายข้างต้นจึงทำให้เราได้ทราบ คำตอบที่ว่าทำไมต้องมีดนตรีก็เพราะว่าดนตรีช่วยทำให้ มนุษย์เรารู้สึกเพลิดเพลินได้"
      },
      {
        "type": "text",
        "data": "ดนตรีเป็นศิลปะที่อาศัยเสียงเพื่อเป็นสื่อในการถ่าย ทอดอารมณ์ความรู้สึกต่าง ๆไปสู่ผู้ฟังเป็นศิลปะที่ง่ายต่อ การสัมผัส ก่อให้เกิดความสุขความปลื้มปิติพึงพอใจให้ แก่มนุษย์ได้ นอกจากนี้ได้มีนักปราชญ์ท่านหนึ่งได้กล่าว ไว้ว่า “ดนตรีเป็นภาษาสากลของมนุษยชาติเกิดขึ้นจาก ธรรมชาติและมนุษย์ได้นำมาดัดแปลงแก้ไขให้ประณีต งดงามไพเราะเมื่อฟังดนตรีแล้วทำให้เกิดความรู้สึกนึกคิดต่างๆ” นั้นก็เป็นเหตุผลหนึ่งที่ทำให้เราได้ทราบว่ามนุษย์ ไม่ว่าจะเป็นชนชาติใดภาษาใดก็สามารถรับรู้อรรถรสของดนตรีได้โดยใช้เสียงเป็นสื่อได้เหมือนกัน"
      }
    ],
    "postId": "581b11b0c67f4b5005bfcbcd",
    "hidden": "false"
  }, {
    "css": " .elem10 {  width: 20%;  background-color: #000;  height: 100%;  float: left; }  .elem11 {  width: 79%;  float: left; }  .elem11 img {    display: block;    width: 100%;    margin: 0 auto 1em;    opacity: 1; }  .elem12 {  font-family: 'Conv_CSChatThaiUI', Sans-Serif;  width: 79%;  float: left;  padding-left: 35px;  text-align: left; }   ",
    "element": [{
        "type": "text",
        "data": " "
      },
      {
        "type": "img",
        "data": "http://localhost/testsass/img/04_02.png"
      },
      {
        "type": "text",
        "data": "ดนตรีเป็นศิลปะที่อาศัยเสียงเพื่อเป็นสื่อในการถ่าย ทอดอารมณ์ความรู้สึกต่าง ๆไปสู่ผู้ฟังเป็นศิลปะที่ง่ายต่อ การสัมผัส ก่อให้เกิดความสุขความปลื้มปิติพึงพอใจให้ แก่มนุษย์ได้ นอกจากนี้ได้มีนักปราชญ์ท่านหนึ่งได้กล่าว ไว้ว่า “ดนตรีเป็นภาษาสากลของมนุษยชาติเกิดขึ้นจาก ธรรมชาติและมนุษย์ได้นำมาดัดแปลงแก้ไขให้ประณีต งดงามไพเราะเมื่อฟังดนตรีแล้วทำให้เกิดความรู้สึกนึกคิดต่างๆ” นั้นก็เป็นเหตุผลหนึ่งที่ทำให้เราได้ทราบว่ามนุษย์ ไม่ว่าจะเป็นชนชาติใดภาษาใดก็สามารถรับรู้อรรถรสของดนตรีได้โดยใช้เสียงเป็นสื่อได้เหมือนกัน มีบุคคลจำนวนไม่น้อยที่ตั้งคำถามว่า “ดนตรีคืออะไร” แล้ว “ทำไมต้องมีดนตรี” คำว่า “ดนตรี” ในพจนานุกรม ฉบับราชบัณฑิตยสถาน พ.ศ. 2525 ได้ให้ความหมายไว้ ว่า “เสียงที่ประกอบกันเป็นทำนองเพลง เครื่องบรรเลง ซึ่งมีเสียงดังทำให้รู้สึกเพลิดเพลิน หรือเกิดอารมณ์รัก โศก หรือรื่นเริง”จากความหมายข้างต้นจึงทำให้เราได้ทราบ คำตอบที่ว่าทำไมต้องมีดนตรีก็เพราะว่าดนตรีช่วยทำให้ มนุษย์เรารู้สึกเพลิดเพลินได้"
      }
    ],
    "postId": "581b11b0c67f4b5005bfcbcd",
    "hidden": "false"
  }, {
    "css": ".elem13 {  width: 25%;  float: right; }  .elem13 img {    display: block;    width: 100%;    opacity: 1; }.elem13:parent {  background: #F00; } .elem14 {  font-family: 'Conv_CSChatThaiUI', Sans-Serif;  width: 73%;  height: 40%;  float: right;  background-color: #000;  color: #FFF;  position: relative; }  .elem14 p {    width: 50%;    /* height: 100%; */    float: right;    font-size: 30px;    vertical-align: 80%;    text-align: right;    padding-right: 20px;    position: absolute;    bottom: 0px;    right: 0px; } .elem15 {  font-family: 'Conv_CSChatThaiUI', Sans-Serif;  width: 73%;  float: right;  background-color: #000;  color: #FFF;  height: 60%; }  .elem15 p {    width: 80%;    height: 100%;    float: right;    text-align: right;    padding-right: 20px; }",
    "element": [{
        "type": "img",
        "data": "http://localhost/testsass/img/template1_05_02.png"
      },
      {
        "type": "text",
        "data": "a melody also tune."
      },
      {
        "type": "text",
        "data": "“ดนตรี” มีความหมาย ที่กว้างและหลากหลายมาก การนำดนตรี ไปใช้ประกอบต่างๆที่เราคุ้นเคยเช่น การใช้ประกอบในภาพยนต์ เนื่องจาก ดนตรีนั้นสามารถนำไปเป็นพื้นฐานใน การสร้างอารมณ์ลักษณะต่างๆของ แต่ละฉากได้ พิธีกรรมทางศาสนา ก็มีการนำดนตรีเข้าไปมี ส่วนร่วมด้วย จึงทำให้มีความขลัง ความน่าเชื่อถือ ความศรัทธามีประสิทธิภาพมากขึ้น นอกจากนี้ดนตรีบางประเภทถูกนำ ไปใช้ในการเผยแพร่ความเป็นอันหนึ่ง อันเดียวกันของกลุ่มคนหรือเชื้อชาติ"
      }
    ],
    "postId": "581b11b0c67f4b5005bfcbcd",
    "hidden": "false"
  }]
};
