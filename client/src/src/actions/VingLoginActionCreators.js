import VingDispatcher from '../dispatcher/VingDispatcher';
import VingConstants from '../constants/VingConstants';
import VingAPIUtils from '../utils/VingAPIUtils';

var ActionTypes = VingConstants.ActionTypes;

module.exports = {

  initMember: function (startPage) {
    let member = JSON.parse(localStorage.getItem('member'));
    VingDispatcher.dispatch({
      type: ActionTypes.MEMBER_INIT,
      member: member,
      currentPage: startPage ? startPage : 'HomeSignup'
    });
  },

  loginWithEmail: function(email, password) {
    VingAPIUtils.loginWithEmail(email, password, (err, response) => {
      VingDispatcher.dispatch({
        type: ActionTypes.RECEIVE_LOGIN_RESPONSE,
        member: response,
        error: err
      });
    });
  },

  loginWithFB: function() {
    VingAPIUtils.loginWithFB((err, response) => {
      VingDispatcher.dispatch({
        type: ActionTypes.RECEIVE_FB_RESPONSE,
        member: response,
        error: err
      });
    });
  },

  signup: function(email, password) {
    VingAPIUtils.signup(email, password, (err, response) => {
      VingDispatcher.dispatch({
        type: ActionTypes.RECEIVE_SIGNUP_RESPONSE,
        member: response,
        currentPage: 'Signup',
        error: err
      });
    });
  },

  logout: function(accessToken) {
    VingAPIUtils.logout(accessToken, (err, response) => {
      VingDispatcher.dispatch({
        type: ActionTypes.RECEIVE_LOGOUT_RESPONSE,
        member: response,
        currentPage: 'HomeSignup',
        error: err
      });
    });
  },

  updateProfile: function(data) {
    VingAPIUtils.logout(data, (err, response) => {
      VingDispatcher.dispatch({
        type: ActionTypes.RECEIVE_UPDATE_PROFILE_RESPONSE,
        member: response,
        currentPage: 'InterestSurvey',
        error: err
      });
    });
  },

};
