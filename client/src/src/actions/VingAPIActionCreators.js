import VingDispatcher from '../dispatcher/VingDispatcher';
import VingConstants from '../constants/VingConstants';
import VingAPIUtils from '../utils/VingAPIUtils';

var ActionTypes = VingConstants.ActionTypes;

module.exports = {
  initInterests: function (accessToken) {
    VingAPIUtils.getInterest(accessToken, (err, interests) => {
      VingDispatcher.dispatch({
        type: ActionTypes.RECEIVE_INTEREST_RESPONSE,
        interests: interests
      });
    });
  },

  initThemes: function (accessToken) {
    VingAPIUtils.getThemes(accessToken, (err, response) => {
      VingDispatcher.dispatch({
        type: ActionTypes.RECEIVE_THEME_RESPONSE,
        themes: response
      });
    });
  },

  initTemplates: function (accessToken, themeId) {
    VingAPIUtils.getTemplates(accessToken, themeId, (err, response) => {
      VingDispatcher.dispatch({
        type: ActionTypes.RECEIVE_TEMPLATE_RESPONSE,
        themeId: themeId,
        templates : response
      });
    });
  },

  selectTemplate: function (templateId) {
    VingDispatcher.dispatch({
      type: ActionTypes.SELECT_TEMPLATE,
      templateId: templateId
    });
  },

  initSuggestionMembers: function (accessToken) {
    VingAPIUtils.getSuggestionMembers(accessToken, (err, response) => {
      VingDispatcher.dispatch({
        type: ActionTypes.RECEIVE_SUGGESTION_MEMBERS_RESPONSE,
        members : response
      });
    });
  },

  initFeed: function (accessToken, memberId) {
    VingAPIUtils.getFeedByMember(accessToken, memberId, (err, response) => {
      VingDispatcher.dispatch({
        type: ActionTypes.RECEIVE_FEED_RESPONSE,
        feed : response
      });
    });
  },

  createPost: function (accessToken, memberId) {
    VingAPIUtils.createPost(accessToken, memberId, (err, response) => {
      VingDispatcher.dispatch({
        type: ActionTypes.RECEIVE_CREATE_POST_RESPONSE,
        post : response
      });
    });
  },

  getPostDetail: function (accessToken, postId) {
    VingAPIUtils.getPostDetail(accessToken, postId, (err, response) => {
      VingDispatcher.dispatch({
        type: ActionTypes.RECEIVE_POST_RESPONSE,
        post : response
      });
    });
  },

  createPage: function (accessToken, memberId, template, postId) {
    VingAPIUtils.createPage(accessToken, memberId, template, postId, (err, response) => {
      VingDispatcher.dispatch({
        type: ActionTypes.RECEIVE_CREATE_PAGE_RESPONSE,
        page : response
      });
    });
  },
};
