var async = require('async');
var moment = require('moment');

module.exports = function (app) {


  app.post('/payment', function (req, res) {
    console.log('app.post(/payment)');
    // console.log('req.hostname', req.hostname);
    // console.log('req.headers', req.headers);
    if (req.body) {
      var obj;
      try {
        obj = JSON.parse(req.body);
      } catch(err) {
        console.log(err);
        var error = new Error('Error: Invalid.');
        error.status = 204;
        console.log(error);
        res.send(error);
      }
      console.log('obj', obj);
      if (obj.TransID && obj.Channel && obj.Token && obj.Currency && obj.DateTime) {
        var CoinHistory = app.models.CoinHistory;
        CoinHistory.findOne({where: {token: obj.Token}}, function(err, returnObj){
          if (err) {
            console.log(err);
            return callback(err);
          } else if (!returnObj) {
            var error = new Error('Error: Token not found.');
            error.status = 204;
            console.log(error);
            res.send(error);
          }
          else {
            returnObj.transID = obj.TransID;
            returnObj.paymentChannel = obj.Channel;
            returnObj.coin = parseInt(obj.Money);
            returnObj.currency = obj.Currency;
            returnObj.paymentTime = moment(obj.DateTime).toDate();
            returnObj.save(function(){
              return res.send({Status: '200', StatusText: 'success'});
            });
          }
        });
      } else {
        var error = new Error('Error: Invalid.');
        error.status = 204;
        console.log(error);
        res.send(error);
      }
    }
  });

};