var async = require('async');
// var redis = require('redis'),
//     client = redis.createClient();
var redisHelper = require('./../../common/lib/redishelper');
var client = redisHelper.client;
module.exports = function(app) {

    var Program = app.models.Program;
    var Schedule = app.models.Schedule;
    var Tag = app.models.Tag;

    app.get('/cleansingProgramTag', function(req, res) {
        console.log('cleansingProgramTag');
        // console.log(req.body);
        Program.find(function(err, programs) {
            if (err) {
                console.log(err);
            };
            // return programs;
            for (var i = programs.length - 1; i >= 0; i--) {
                if (programs[i].tag && programs[i].tag.length > 0) {
                    for (var j = programs[i].tag.length - 1; j >= 0; j--) {
                        // console.log(i);
                        console.log(programs[i].tag[j]);
                        client.sadd("tag:program:" + programs[i].tag[j].toString(), programs[i].id.toString(), redis.print);
                    }
                }
            }
            res.send('cleansing Program Tag');
        });
    });


    app.get('/cleansingSchedule', function(req, res) {
        /* Cleansing Schedule those have non-exist Program */
        console.log('cleansingSchedule');

        Schedule.find({
            include: {
			    relation: 'program', // include the owner object
			    scope: { // further filter the owner object
			      fields: 'id'
			    }
			}
        }, function(err, schedules) {
            if (err || !schedules || schedules.length < 1) {
                console.log(err);
                return err;
            }
            var count = 0;
            async.forEach(schedules, function (schedule, callback) {
                if (!schedule.program()) {
                	Schedule.destroyById(schedule.id, function(err) {
                		if (err) {
                			console.log(err);
                		}
                		console.log('delete', count);
                		count++;
                		callback(err);
                	})
                }
                else {
                	return callback(null);
                }
	        },
	        function (err) {
                if (err) {
                  res.send(err);
                }
                res.send('Cleansing ' + count + ' Schedule those have non-exist Program');
            })
        });
    });

}
