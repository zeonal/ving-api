
var async = require('async');
// var redis = require('redis'),
//     client = redis.createClient();
var redisHelper = require('./../../common/lib/redishelper');
var client = redisHelper.client;

module.exports = function(app) {

	var Program = app.models.Program;
	var Schedule = app.models.Schedule;
	var Tag = app.models.Tag;

	app.post('/addProgramTag', function(req, res) {
		console.log('addProgramTag');
		console.log(req.body);
		if (req.body.tagName && req.body.programId) {
			client.sadd ("tag:program:"+req.body.tagName.toString(), req.body.programId.toString(), redis.print);
			console.log('add tag', 'tag:program:'+req.body.tagName.toString())
		}
	});

	app.post('/deleteProgramTag', function(req, res) {
		console.log('deleteProgramTag');
		console.log(req.body);
		if (req.body.tagName && req.body.programId) {
			client.srem ("tag:program:"+req.body.tagName.toString(), req.body.programId.toString(), redis.print);
			console.log('delete tag', 'tag:program:'+req.body.tagName.toString())
		}
	});


}
