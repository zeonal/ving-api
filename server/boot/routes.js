var request = require('request');
var cheerio = require('cheerio');
var moment = require('moment');
var async = require('async');
var path = require('path');

// CX and API_KEY for Google API
const CX = '010239418844030630648:py4ewdvjwl4';
const API_KEY = 'AIzaSyA8PguRW5CfaVBdySkIHlT9RBpfwD268DA';

// Development Environment for reset password
var env = process.env.NODE_ENV || 'production';
var resetUrl = 'http://192.168.120.204#/resetpassword';
var BASE_URL = 'http://192.168.120.204';
if ('dev' === env) {
  resetUrl = 'http://localhost:3000#/resetpassword';
  BASE_URL = 'http://localhost:3000';
}


// The following line installs the Node.js require extension
// for `.marko` files. Once installed, `*.marko` files can be
// required just like any other JavaScript modules.
require('marko/node-require').install();


// ...

// Load a Marko template by requiring a .marko file:
var template = require('./templates/test.marko');


module.exports = function (app) {

  app.get('/', function (req, res) {
    return res.sendFile(path.join(__dirname + '/../../client/src/build/index.html'));
  });

  // Use template engine
  app.get('/view/:postId', function (req, res) {
    const Post = app.models.Post;
    console.log(req.params.postId);
    Post.findById(req.params.postId, {
      include: {
        relation: 'member',
        scope: {
          include: 'profileImage'
        }
      }
    }, function (err, post) {
      if (err) {
        //render err
        console.log(err);
        return res.send(err);
      }
      console.log(post);
      if (post) {
        const member = post.member();
        const profileImage = member.profileImage();
        var displayImage = null;
        if (profileImage) {
          displayImage = profileImage[0].url;
        }
        console.log(displayImage);
        return template.render({
          post: post,
          member: post.member(),
          profileImage: displayImage
        }, res);
      } else {
        return res.send('null');
      }

    });

  });

  app.post('/hook', function (req, res) {
    var exec = require('child_process').exec;
    res.send('yoyo');
    exec('cd /home/ving/ving-api && git pull', function () {
      exec('cd /home/ving/ving-api && npm install', function (err) {
        if (err) {
          console.log(err);
        }
        exec('forever restartall', function (err) {
          if (err) {
            console.log(err);
          }
        });
      });
    });
  });


  var errorHandler = function (statusCode, message) {
    return {
      error: {
        name: 'Error',
        status: statusCode,
        message: message,
        statusCode: statusCode
      }
    };
  }

  // Revamped upload API to be more generic
  // to add more file type see also: server.js
  app.post('/uploadapi', function (req, res) {

    var Image = app.models.Image;
    var AccessToken = app.models.AccessToken;



    if (!req.query.access_token) {
      var err = new errorHandler(401, 'Not Authorized.');
      res.statusCode = 401;
      return res.send(err);
    }


    if (!req.body.type) {
      var err = new errorHandler(400, 'Paremeter `type` is undefined.');
      res.statusCode = 400;
      return res.send(err);
    }

    console.log('Upload of type: ' );
    console.log(req.body);

    // Get memberId from AccessToken
    AccessToken.findOne({
      where: {
        id: req.query.access_token
      }
    }, function (err, token) {
      if (err) {
        return res.send(err);
      }

      if (token) {
        var url = '/assets/' + req.body.type;
        url += '/' + req.file.filename;

        // var allowedType = ['profile', 'cover', 'group'];
        // **Set Allowed types in server.js**
        var query = {};
        query.url = url;
        query.profileId = req.body.type === 'profile' && token.userId ? token.userId : undefined;
        query.coverId = req.body.type === 'cover' && token.userId ? token.userId : undefined;
        query.groupId = req.body.type === 'group' && req.body.groupId ? req.body.groupId : undefined;
        query.interestId = req.body.type === 'interest' && req.body.interestId ? req.body.interestId : undefined;
        query.pageId = req.body.type === 'page' && req.body.pageId ? req.body.pageId : undefined;
        query.postId = req.body.type === 'post' && req.body.postId ? req.body.postId : undefined;
        // query.postId = req.body.type === 'post' && req.body.postId ? req.body.postId : undefined;
        query.themeId = req.body.type === 'theme' && req.body.themeId ? req.body.themeId : undefined;
        query.templateId = req.body.type === 'template' && req.body.templateId ? req.body.templateId : undefined;
        query.memberId = token.userId;
        // automatically set default to true.
        query.hidden = 0;

        console.log(query);

        // Default mayhem
        if (query.profileId) {
          Image.updateAll({profileId: query.profileId}, {hidden: 1});
        }
        if (query.coverId) {
          Image.updateAll({coverId: query.coverId}, {hidden: 1});
        }
        if (query.groupId) {
          Image.updateAll({groupId: query.groupId}, {hidden: 1});
        }
        if (query.interestId) {
          Image.updateAll({interestId: query.interestId}, {hidden: 1});
        }
        // if (query.pageId) {
        //   Image.updateAll({profileId: query.pageId}, {hidden: 'false'});
        // }

        query.filename = req.file.filename;

        Image.create(query, function (err, instance) {
          if (err) {
            console.log(err);
            return callback(err);
          }
          if (query.postId) {
            var Post = Image.app.models.Post;
            Post.findById(query.postId, function (err, returnPost) {
              if (err) {
                console.log(err);
                var errMsg = new Error(err);
                errMsg.status = 500; // HTTP status code
                return res.send(errMsg);
              }
              returnPost.defaultImageUrl = query.url;
              returnPost.save(function(){
                return res.send(instance);
              });
            });
          } else {
            return res.send(instance);
          }
        });
      } else {
        var err = new errorHandler(401, 'Not Authorized.');
        res.statusCode = 401;
        return res.send(err);
      }
    });
  });

  app.get('/crossdomain.xml', function (req, res) {
    var xml = '<?xml version="1.0" ?>';
    xml += '<!DOCTYPE cross-domain-policy SYSTEM ';
    xml += '"http://www.macromedia.com/xml/dtds/cross-domain-policy.dtd">';
    xml += '<cross-domain-policy>';
    xml += '<allow-access-from domain="*" secure="false"/>';
    xml += '</cross-domain-policy>';
    return res.send(xml);
  });

};
