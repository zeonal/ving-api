var async = require('async');
var moment = require('moment');
var path = require('path');
var request = require('request');
var fs = require('fs');
var gm = require('gm').subClass({imageMagick: true});

/**
  FIXME: This code should be removed in production.
*/
module.exports = function (app) {

  var download = function (uri, filename, callback) {
    request.head(uri, function (err, res, body) {
      console.log(uri);
      if (err) {
        console.log(err);
      }
      console.log('content-type:', res.headers['content-type']);
      console.log('content-length:', res.headers['content-length']);

      request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
  };

  app.get('/mockimage', function (req, res) {
    gm('app/public/menu/' + file)
    .resize(750, 750)
    .noProfile()
    .write('app/public/menu/' + filename + '_thumb' + ext, function (err) {
      if (!err) console.log(filename + ext + ' done');
    });
  });

  app.get('/mock', function (req, res) {

    var mockImage = 'https://images.unsplash.com/photo-1452421822248-d4c2b47f0c81?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&s=74553159742c7cf05c6eb04baabc408c&w=1199&h=799';
    var mockInterest = [
      {
        name: 'Comedy',
        url: 'https://images.unsplash.com/photo-1444005233317-7fb24f0da789?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&s=4aaa14b8ac9c6683c92cb1cf3beadb6c&w=1199&h=799',
      },
      {
        name: 'Movies',
        url: 'https://images.unsplash.com/photo-1461151304267-38535e780c79?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&s=6c6352ec73bb4dcb7b612aca32fd244f&w=1199&h=799',
      },
      {
        name: 'Music',
        url: 'https://images.unsplash.com/photo-1458063048042-fa854e05b745?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&s=dd86f025302ec1f75cd71f342585f8ec&w=1199&h=799'
      },

    ];

    var mockCoaches = [
      {
        name: 'Lisa',
        displayName: '@lisa',
        email: 'lisa@vingtv.com',
        password: '1234',
        url: 'https://images.unsplash.com/reserve/RONyPwknRQOO3ag4xf3R_Kinsey.jpg?dpr=1&auto=format&fit=crop&w=750&h=500&q=80&cs=tinysrgb&crop='
      },
      {
        name: 'John',
        displayName: '@john',
        email: 'john@vingtv.com',
        password: '1234',
        url: 'https://images.unsplash.com/photo-1469332183683-f2bce6607452?dpr=1&auto=format&fit=crop&w=750&h=500&q=80&cs=tinysrgb&crop='
      },
      {
        name: 'Anna',
        displayName: '@anna',
        email: 'anna@vingtv.com',
        password: '1234',
        url: 'https://images.unsplash.com/photo-1474880900115-a35744e35877?dpr=1&auto=format&fit=crop&w=750&h=500&q=80&cs=tinysrgb&crop='
      }
    ];

    var Theme = app.models.Theme;
    var Template = app.models.Template;
    var Post = app.models.Post;
    var Image = app.models.Image;
    var Group = app.models.Group;
    var Page = app.models.Page;
    var Member = app.models.Member;
    var Interest = app.models.Interest;
    var Suggestion = app.models.Suggestion;
    var Feed = app.models.Feed;

    async.waterfall([

      // Create interest
      function (callback) {
        async.forEach(mockInterest, function (interest, cb) {
          //console.log(interest);

          Interest.remove({
            name: interest.name
          }, function (err) {
            Interest.upsert({
              name: interest.name,
              hidden: 0
            }, function (err, obj) {
              if (err) {
                console.log(err);
                return cb(err);
              }
              var imageFilename = Math.floor(Math.random() * 1000) + '-' + Date.now() + '.jpg';
              var imagePath = path.join(__dirname, '../../client/dist/assets/interest/' + imageFilename);
              download(interest.url, imagePath, function () {
                console.log('done downloading');
                Image.upsert({
                  url: '/assets/interest/' + imageFilename,
                  interestId: obj.id,
                }, function (err, image) {
                  if (err) {
                    return callback(err);
                  }

                  return cb(null);
                });
              });
            });
          });


        }, function (err) {
          return callback();
        });

      },

      // Remove member Suggestion
      function (callback) {
        async.forEach(mockCoaches, function (coach, cb) {
          Member.remove({
            email: coach.email
          }, function (err, member) {
            if (err) {
              return cb(err);
            }

            return cb(null);
          });

        }, function (err) {
          return callback();
        });

      },

      // Add member suggestion.
      function (callback) {
        async.forEach(mockCoaches, function (coach, cb) {
          Member.upsert({
            email: coach.email,
            password: coach.password,
            displayName: coach.displayName,
            name: coach.name,
            coach: true,
          }, function (err, member) {
            if (err) {
              return cb(err);
            }

            Suggestion.upsert({
              memberId: member.id
            }, function (err, suggestion) {

              var imageFilename = Math.floor(Math.random() * 1000) + '-' + Date.now() + '.jpg';
              var imagePath = path.join(__dirname, '../../client/dist/assets/profile/' + imageFilename);
              download(coach.url, imagePath, function () {
                console.log('done downloading');
                Image.upsert({
                  url: '/assets/profile/' + imageFilename,
                  profileId: member.id,
                  hidden: 0
                }, function (err, image) {
                  if (err) {
                    return cb(err);
                  }
                  console.log(image);
                  return cb(null);
                });
              });
            });
          });
        }, function (err) {

          return callback(err);
        });

      },

      // // Remove Test Member
      // function (callback) {
      //   Member.remove({
      //     email: 'test@test.com'
      //   }, function (err, member) {
      //     if (err) {
      //       return callback(err);
      //     }
      //
      //     return callback(null);
      //   });
      //
      // },

      // Create a test Member.
      function (callback) {
        Member.upsert({
          email: 'test@test.com',
          password: 'test'
        }, function (err, member) {
          if (err) {
            return callback(err);
          }
          return callback(null, member);
        });

      },

      // Create a test Theme.
      function (member, callback) {
        Theme.upsert({
          name: 'Test Theme'
        }, function (err, theme) {
          if (err) {
            return callback(err);
          }

          callback(null, member, theme);
        });

      },
      // Create a preview image for theme.
      function (member, theme, callback) {
        var imageFilename = Math.floor(Math.random() * 1000) + '-' + Date.now() + '.jpg';
        var imagePath = path.join(__dirname, '../../client/dist/assets/theme/' + imageFilename);
        download(mockImage, imagePath, function () {
          console.log('done downloading');
          Image.upsert({
            url: '/assets/theme/' + imageFilename,
            themeId: theme.id,
          }, function (err, image) {
            if (err) {
              return callback(err);
            }
            //console.log(image);

            return callback(null, member, theme);
          });
        });


      },
      function (member, theme, callback) {
        Template.upsert({
          css: '.1{color:#ff0000;}.2{color:#000;}.3{color:#00ff00;}',
          html: '<div class="1"></div><div class="2"></div><div class="3"></div>',
          element: [{
            index: 0,
            class: '1',
            type: 'text',
            data: 'title'
          }, {
            index: 1,
            class: '2',
            type: 'text',
            data: 'content'
          }, {
            index: 2,
            class: '3',
            type: 'text',
            data: 'footer'
          }]
        }, function (err, template) {
          if (err) {
            return callback(err);
          }
          return callback(null, member, theme, template);
        });
      },
      function (member, theme, template, callback) {
        var imageFilename = Math.floor(Math.random() * 1000) + '-' + Date.now() + '.jpg';
        var imagePath = path.join(__dirname, '../../client/dist/assets/template/' + imageFilename);
        download(mockImage, imagePath, function () {
          Image.upsert({
            url: '/assets/template/' + imageFilename,
            templateId: template.id,
          }, function (err, image) {
            if (err) {
              return callback(err);
            }
            //console.log(image);

            return callback(null, member, theme, template);
          });
        });

      },
      function (member, theme, template, callback) {
        Group.upsert({
          name: 'Test Group',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          hidden: 0
        }, function (err, group) {
          if (err) {
            return callback(err);
          }
          return callback(null, member, theme, template, group);
        });
      },
      // Create a test post.
      function (member, theme, template, group, callback) {
        var imageFilename = Math.floor(Math.random() * 1000) + '-' + Date.now() + '.jpg';
        var imagePath = path.join(__dirname, '../../client/dist/assets/post/' + imageFilename);
        download(mockImage, imagePath, function () {
          Post.upsert({
            name: 'Test Post',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            hidden: 0,
            groupId: group.id,
            defaultImageUrl: '/assets/post/' + imageFilename,
          }, function (err, post) {
            if (err) {
              return callback(err);
            }
            return callback(null, member, theme, template, group, post);
          });

        });

      },

      // Create 3 pages.
      function (member, theme, template, group, post, callback) {
        var ary = [1, 2, 3];
        async.each(ary, function(i, cb) {
          Page.upsert({
            css: template.css,
            html: template.html,
            element: [{
              index: 0,
              class: '1',
              data: 'title page ' + i.toString()
            }, {
              index: 1,
              class: '2',
              data: 'content page ' + i.toString()
            }, {
              index: 2,
              class: '3',
              data: 'footer page ' + i.toString()
            }],
            postId: post.id,
            templateId: template.id,
            memberId: member.id,
          }, function (err, page) {
            if (err) {
              return cb(err);
            }
            return cb(null);
          });
        }, function (err) {
          return callback(null, member, theme, template, group, post)
        });

      },

      // Create Feed
      function (member, theme, template, group, post, callback) {
        console.log(member);
        console.log(post);
        Feed.upsert({
          memberId: member.id,
          postId: post.id
        }, function (err, feed) {
          if (err) {
            console.log(err);
          }
          console.log(feed);
          return callback(null);
        })

      },

    ], function (err, result) {
      // result now equals 'done'
      if (err) {
        console.log(err);
      }
      return res.send('ok');
    });

  });
};
