/**
 * Created by thoonly on 24/11/2559.
 */
var async = require('async');
var moment = require('moment');
var CronJob = require('cron').CronJob;


module.exports = function (app) {
  var PurchaseGroup = app.models.PurchaseGroup;
  var Group = app.models.Group;
  /*
   * Runs every day 12 AM
   */
  var job = new CronJob({
    cronTime: '0 0 0 * * *',
    onTick: function () {

      let startToday = moment().startOf('day');
      let endToday = moment().endOf('day');

      PurchaseGroup.find({
        where: {
           and:[
             {expire_date:{gte: startToday}},
             {expire_date:{lt: endToday}}
             ]
        }
      }, function (err, purGroups) {

        async.forEach(purGroups, function (purGroup, cb) {

          Group.subscribeGroup({memberId: purGroup.memberId, groupId: purGroup.groupId}, function (err) {

            if (err) {
              return cb({err: err, memberId: purGroup.memberId, groupId: purGroup.groupId});
            }

            cb();
          });


        }, function (err) {
          if (err) {
            console.log(err);
          }

        });

        if (err) {
          console.log(err);
        } else {

        }

      });

    },
    start: true,
    timeZone: 'Asia/Bangkok'
  });

  job.start();
};

