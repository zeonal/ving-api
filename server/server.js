// Modules
var loopback = require('loopback');
var boot = require('loopback-boot');
// var bodyParser = require('body-parser');
var multer = require('multer');
var path = require('path');
var morgan = require('morgan');
var fs = require('fs');
var app = module.exports = loopback();


// Filter out static files
app.use(morgan('dev', {
  skip: function (req) {
    if ((/^\/view/i).test(req.path)) {
      return true;
    } else if (!(/\.(html|css|png|jpg|gif|jpeg|js|fonts|map|woff|woff2)$/i).test(req.path)) {
      return false;
    } else {
      return true;
    }
  }
}));

// Signup with multipart profile picture
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    // Save original filename
    req.body.originalname = file.originalname;

    // Add new type in here.
    var allowedType = ['profile', 'cover', 'group', 'interest', 'page', 'post', 'theme', 'template'];

    // Check if type is not empty string and also in allowed types.
    // console.log('---- Reqbody Type:');
    // console.log(req.body + '-----');
    // console.log('---- Upload Type:' + req.body.type + '-----');
    if (req.body.type && req.body.type !== '' && allowedType.indexOf(req.body.type) > -1) {
      var filePath = path.resolve(__dirname, '../client/dist/assets/' + req.body.type);

      // console.log(filePath);
      // Make dir for the first time.
      // Should be removed in production
      if (!fs.existsSync(filePath)) {
        fs.mkdirSync(filePath);
      }
      return cb(null, filePath);

    } else {
      return cb(null, path.resolve(__dirname, '../client/dist/assets/'));
    }
  },
  filename: function (req, file, cb) {
    if (req.body.email) {
      cb(null, req.body.email.replace('@', '_').replace('.', '_') + '-' + Date.now() + path.extname(file.originalname));
    } else {
      cb(null, Math.floor(Math.random() * 1000) + '-' + Date.now() + path.extname(file.originalname));
    }
  }
});

app.use(multer({
  storage: storage
}).single('file'));


// -- Mount static files here--
// All static middleware should be registered at the end, as all requests
// passing the static middleware are hitting the file system
// Example:

app.use(loopback.static(path.resolve(__dirname, '../client/public')));

app.start = function () {
  // start the web server
  return app.listen(function () {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};





// app.remotes().before('*.*', function(ctx,next) {
//   console.log(ctx.req.connection.remoteAddress)
//   next();
// });

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function (err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});

module.exports = app;
